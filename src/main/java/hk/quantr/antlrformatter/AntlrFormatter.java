package hk.quantr.antlrformatter;

import java.io.IOException;
import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.parser.ANTLRv4Parser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class AntlrFormatter {

	public static boolean debug = true;

	public static String format(String inputFile) throws IOException {
		//		ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(new FileInputStream("/Users/peter/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerParser.g4")));

		ANTLRv4Lexer lexer = new ANTLRv4Lexer(CharStreams.fromString(inputFile));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
		ANTLRv4Parser.GrammarSpecContext context = parser.grammarSpec();

		ParseTreeWalker walker = new ParseTreeWalker();

		Phrase1Listener phrase1Listener = new Phrase1Listener();
		walker.walk(phrase1Listener, context);

		SourceFormatterListener listener = new SourceFormatterListener(tokenStream, phrase1Listener);
		walker.walk(listener, context);

		if (debug) {
			System.out.println("tokenStream.size()=" + tokenStream.size());
			for (int x = 0; x < tokenStream.size(); x++) {
				System.out.println("> " + String.format("%-40s %s", tokenStream.get(x), ANTLRv4Lexer.VOCABULARY.getSymbolicName(tokenStream.get(x).getType())));
			}
			System.out.println("----------------------------------------------------------------------");
		}

//
//		System.out.println(listener.result);
		return listener.result;
	}
}
