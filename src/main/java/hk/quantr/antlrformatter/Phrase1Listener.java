package hk.quantr.antlrformatter;

import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.parser.ANTLRv4ParserBaseListener;
import org.antlr.v4.runtime.tree.TerminalNode;
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Phrase1Listener extends ANTLRv4ParserBaseListener {

	public int max_RULE_REF = Integer.MIN_VALUE;
	public int max_TOKEN_REF = Integer.MIN_VALUE;

	@Override
	public void visitTerminal(TerminalNode node) {
		String tokenName = ANTLRv4Lexer.VOCABULARY.getSymbolicName(node.getSymbol().getType());
//		System.out.println(String.format("Phase 1 : %-20s %5d %d %d \t %s", node.getText(), node.getSymbol().getLine(), node.getSymbol().getStartIndex(), node.getSymbol().getStopIndex(), tokenName));
		if (tokenName.equals("RULE_REF")) {
			if (node.getText().length() > max_RULE_REF) {
				max_RULE_REF = node.getText().length();
			}
		} else if (tokenName.equals("TOKEN_REF")) {
			if (node.getText().length() > max_TOKEN_REF) {
				max_TOKEN_REF = node.getText().length();
			}
		}
	}

}
