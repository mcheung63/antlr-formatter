package hk.quantr.antlrformatter;

import hk.quantr.antlrformatter.datastructure.Option;
import java.util.ArrayList;
import java.util.List;
import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.parser.ANTLRv4Parser;
import org.antlr.parser.ANTLRv4ParserBaseListener;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class SourceFormatterListener extends ANTLRv4ParserBaseListener {

	BufferedTokenStream tokens;

	public String result = "";
	boolean newline = false;
	String lastTokenName = "";
	Phrase1Listener phrase1Listener;
	boolean ignore;
	ArrayList<Option> options = new ArrayList();
	Option tempOption;
	String identifer;
	String optionValue;

	ArrayList<String> noSpace = new ArrayList<String>() {
		{
			add("AT");
			add("SEMI");
			add("ACTION_CONTENT");
			add("ARGUMENT_CONTENT");
		}
	};

	SourceFormatterListener(BufferedTokenStream tokens, Phrase1Listener phrase1Listener) {
		this.tokens = tokens;
		this.phrase1Listener = phrase1Listener;
	}

//	@Override
//	public void enterEveryRule(ParserRuleContext ctx) {
//		int ruleIndex = ctx.getRuleIndex();
//		String ruleName;
//		if (ruleIndex >= 0 && ruleIndex < ruleNames.length) {
//			ruleName = ruleNames[ruleIndex];
//		} else {
//			ruleName = Integer.toString(ruleIndex);
//		}
//		System.out.println("enterEveryRule >> " + ruleName + "\t\t\t" + ctx.getText() + "\t\t\t" + ctx.getStart().getStartIndex() + ", " + ctx.getStop().getStopIndex());
////		if (ruleName.equals("rules")) {
////			ht.put(ctx.getStart().getStartIndex(), "\n");
////		}
//	}
	@Override
	public void visitTerminal(TerminalNode node) {
		String tokenName = ANTLRv4Lexer.VOCABULARY.getSymbolicName(node.getSymbol().getType());
		String leftToken = getLeftToken(node.getSymbol());
		String rightToken = getRightToken(node.getSymbol());

		if (AntlrFormatter.debug) {
			System.out.println(String.format("visitTerminal %-20s %d %d \t %-20s \t %s", node, node.getSymbol().getStartIndex(), node.getSymbol().getStopIndex(), tokenName, leftToken));
		}

//		if (ignore) {
//			return;
//		}
		if (tokenName.equals("EOF")) {
			return;
		}

		if (tokenName.equals("RULE_REF") && leftToken != null && leftToken.equals("SEMI")) {
			result += String.format("%-" + phrase1Listener.max_RULE_REF + "s", node.getText());
		} else if (tokenName.equals("TOKEN_REF") && leftToken != null && leftToken.equals("SEMI")) {
			result += String.format("%-" + phrase1Listener.max_TOKEN_REF + "s", node.getText());
		} else {
			result += node.getText();
			result += getRightHidden(node.getSymbol());//getRightNewLines(node.getSymbol(), true);
		}
//		if (!rightToken.equals("SEMI")) {
//			result += " ";
//		}

		if (tokenName.equals("SEMI")) {
			newline = true;
		} else {
			newline = false;
		}

		lastTokenName = tokenName;
		
	}

	public String getRightHidden(Token token) {
		int i = token.getTokenIndex();
		List<Token> cmtChannel;
		cmtChannel = tokens.getHiddenTokensToRight(i, ANTLRv4Lexer.OFF_CHANNEL);
		String s = "";
		if (cmtChannel != null) {
			for (Token t : cmtChannel) {
				s += t.getText();
			}
		}
		return s;
	}

	public String getRightNewLines(Token token, boolean right) {
		int i = token.getTokenIndex();
		List<Token> cmtChannel;
		if (right) {
			cmtChannel = tokens.getHiddenTokensToRight(i, ANTLRv4Lexer.OFF_CHANNEL);
		} else {
			cmtChannel = tokens.getHiddenTokensToLeft(i, ANTLRv4Lexer.OFF_CHANNEL);
		}
		String s = "";
		if (cmtChannel != null) {
			for (Token t : cmtChannel) {
				String tokenName = ANTLRv4Lexer.VOCABULARY.getSymbolicName(t.getType());
				if (tokenName.equals("WS")) {
					s += "\n".repeat(StringUtils.countMatches(t.getText(), "\n"));
				}
			}
		}
		return s;
	}

	public String getRightToken(Token token) {
		int i = token.getTokenIndex();
		List<Token> cmtChannel = tokens.getHiddenTokensToRight(i, token.getChannel());
		if (cmtChannel != null) {
			Token t = cmtChannel.get(0);
			String tokenName = ANTLRv4Lexer.VOCABULARY.getSymbolicName(t.getType());
			return tokenName;
		}
		return null;
	}

	public String getLeftToken(Token token) {
		int i = token.getTokenIndex();
		int x = i - 1;
		while (x >= 0) {
			List<Token> cmtChannel = tokens.getTokens(x, x);
//		List<Token> cmtChannel = tokens.getHiddenTokensToLeft(i, token.getChannel());
			if (cmtChannel != null) {
				Token t = cmtChannel.get(0);
				if (t.getChannel() == 0) {
					String tokenName = ANTLRv4Lexer.VOCABULARY.getSymbolicName(t.getType());
					return tokenName;
				}
			}
			x -= 1;
		}
		return null;
	}

	public ArrayList<String> getRightTokens(Token token, boolean right) {
		ArrayList<String> temp = new ArrayList();
		int i = token.getTokenIndex();
		List<Token> cmtChannel;// = tokens.getTokens();
//		if (right) {
		cmtChannel = tokens.getHiddenTokensToRight(i, token.getChannel());
//		} else {
//			cmtChannel = tokens.getHiddenTokensToLeft(i, ANTLRv4Lexer.OFF_CHANNEL);
//		}
		if (cmtChannel != null) {
			for (Token t : cmtChannel) {
				String tokenName = ANTLRv4Lexer.VOCABULARY.getSymbolicName(t.getType());
				temp.add(tokenName);
			}
		}
		return temp;
	}

	@Override
	public void enterOptionsSpec(ANTLRv4Parser.OptionsSpecContext ctx) {
		ignore = true;
	}

	@Override
	public void exitOptionsSpec(ANTLRv4Parser.OptionsSpecContext ctx) {
		System.out.println("exitOptionsSpec");
		if (options.size() == 0) {
			result += "options { }";
		} else if (options.size() == 1) {
			result += "options { " + options.get(0) + " }";
		}
		result += getRightNewLines(ctx.getStart(), false);
		ignore = false;
	}

	@Override
	public void exitOption(ANTLRv4Parser.OptionContext ctx) {
		options.add(new Option(identifer, optionValue));
	}

	@Override
	public void exitIdentifier(ANTLRv4Parser.IdentifierContext ctx) {
		identifer = ctx.getText();
	}

	@Override
	public void exitOptionValue(ANTLRv4Parser.OptionValueContext ctx) {
		optionValue = ctx.getText();
	}

}
