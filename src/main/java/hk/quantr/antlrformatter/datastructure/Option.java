/*
 * Copyright 2019 peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.antlrformatter.datastructure;

/**
 *
 * @author peter
 */
public class Option {

	public String identifier;
	public String optionValue;

	public Option() {

	}

	public Option(String identifier, String optionValue) {
		this.identifier = identifier;
		this.optionValue = optionValue;
	}

	@Override
	public String toString() {
		return identifier + " = " + optionValue + ";";
	}
}
