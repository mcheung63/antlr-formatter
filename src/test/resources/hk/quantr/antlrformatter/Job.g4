grammar Job;

eval returns [String value]
    :    output=jobExp {System.out.println($output.text); $value = $output.text;}
    ;


jobExp returns [String value]
    :    ind=indro whitespace var1=language ' developer from ' var2=place 
                            {
                            System.out.println($var1.text);
                            System.out.println($var2.text);
                            $value = $var1.text+$var2.text; }
    ;

indro
    :
    'i am looking for a'
    |
    'i am searching for a'
    ;

language :
    'java' | 'python' | 'cpp'
    ;

place :
    'india' | 'america' | 'africa'
    ;

whitespace :
    (' '|'\t')+
    ;