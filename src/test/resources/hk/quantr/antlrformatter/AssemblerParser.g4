parser grammar AssemblerParser;

options { tokenVocab=AssemblerLexer; }


@header {
	import com.peterswing.CommonLib;
	import hk.quantr.assembler.MAL;
	import hk.quantr.antlrcalculatorlibrary.CalculatorLibrary;
}

@parser::members{
	public MAL mal = new MAL();
        
}

/*
reg		:	RAX|EAX|AX|AH|AL
			|RBX|EBX|BX|BH|BL
			|RCX|ECX|CX|CH|CL
			|RDX|EDX|DX|DH|DL
			|RBP|EBP|BP
			|RSP|ESP|SP
			|RSI|ESI|SI
			|RDI|EDI|DI
			|R8|R8D|R8W|R8L
			|R9|R9D|R9W|R9L
			|R10|R10D|R10W|R10L
			|R11|R11D|R11W|R11L
			|R12|R12D|R12W|R12L
			|R13|R13D|R13W|R13L
			|R14|R14D|R14W|R14L
			|R15|R15D|R15W|R15L
			|RFLAGS|EFLAGS
			|RIP|EIP
			|CS|DS|ES|FS|GS
			;
*/
                        
r8  	returns[int operandSize,int col]:
                        AH{ mal.rex_rb = false;mal.modrm_row = 28; $operandSize=8; $col = 4;}|AL { mal.rex_rb = false;mal.modrm_row = 24; $operandSize=8; $col = 0;}
			|BH{mal.rex_rb = false; mal.modrm_row= 31; $operandSize=8; $col = 7;}|BL { mal.rex_rb = false;mal.modrm_row = 27; $operandSize=8; $col = 3;}
			|CH {mal.rex_rb = false; mal.modrm_row = 29;$operandSize=8; $col = 5;}|CL { mal.rex_rb = false;mal.modrm_row = 25;$operandSize=8; $col = 1;}
			|DH { mal.rex_rb = false;mal.modrm_row = 30;$operandSize=8; $col = 6;}|DL { mal.rex_rb = false;mal.modrm_row = 26;$operandSize=8; $col = 2;}
			|R8L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = 241;$col =0;}
			|R9L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = 251;$col =1;}
			|R10L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = 261;$col =2;}
			|R11L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = 271;$col =3;}
			|R12L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = 281;$col =4;}
			|R13L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = 291;$col =5;}
			|R14L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = 301;$col =6;}
			|R15L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = 311;$col =7;}
			|BPL {mal.rex_rb = false; $operandSize=8;mal.modrm_row = 291;$col = 5;}
			|SPL {mal.rex_rb = false; $operandSize=8;mal.modrm_row = 281;$col = 4;}
			|DIL {mal.rex_rb = false; $operandSize=8;mal.modrm_row = 311;$col = 7;}
			|SIL {mal.rex_rb = false; $operandSize=8;mal.modrm_row = 301;$col = 6;}
			;
                        
r16	returns[int operandSize, int col]	:	AX  {$operandSize=16; mal.rex_rb = false; mal.modrm_row = 24; $col = 0;}
			|BX {$operandSize=16; mal.rex_rb = false; mal.modrm_row = 27;$col = 3;}
			|CX {$operandSize=16; mal.rex_rb = false; mal.modrm_row = 25;$col = 1;}
			|DX {$operandSize=16; mal.rex_rb = false; mal.modrm_row = 26;$col = 2;}
			|BP {$operandSize=16; mal.rex_rb = false; mal.modrm_row = 29;$col = 5;}
			|SP {$operandSize=16; mal.rex_rb = false; mal.modrm_row = 28;$col = 4;}
			|SI {$operandSize=16; mal.rex_rb = false; mal.modrm_row = 30;$col = 6;}
			|DI {$operandSize=16; mal.rex_rb = false; mal.modrm_row = 31;$col = 7;}
			|R8W {$operandSize=16; mal.rex_rb = true; mal.modrm_row = 241; $col = 0;}
			|R9W {$operandSize=16; mal.rex_rb = true; mal.modrm_row = 251;$col = 1;}
			|R10W {$operandSize=16; mal.rex_rb = true; mal.modrm_row = 261;$col = 2;}
			|R11W {$operandSize=16; mal.rex_rb = true; mal.modrm_row = 271;$col = 3;}
			|R12W {$operandSize=16; mal.rex_rb = true; mal.modrm_row = 281;$col = 4;}
			|R13W {$operandSize=16; mal.rex_rb = true; mal.modrm_row = 291;$col = 5;}
			|R14W {$operandSize=16; mal.rex_rb = true; mal.modrm_row = 301;$col = 6;}
			|R15W {$operandSize=16; mal.rex_rb = true; mal.modrm_row = 311;$col = 7;}
			;
                        
r32	returns [int operandSize,int col]:	EAX{mal.rex_rb = false; mal.modrm_row = 24; $operandSize=32;$col = 0;}
			|EBX{mal.rex_rb = false; mal.modrm_row = 27; $operandSize=32;$col =3;}
			|ECX{mal.rex_rb = false; mal.modrm_row = 25; $operandSize=32;$col =1;}
			|EDX{mal.rex_rb = false; mal.modrm_row = 26; $operandSize=32;$col = 2;}
                        |ESP{mal.rex_rb = false; mal.modrm_row = 28; $operandSize=32;$col = 4;}
                        |EBP{mal.rex_rb = false; mal.modrm_row = 29; $operandSize=32;$col = 5;}
                        |ESI{mal.rex_rb = false; mal.modrm_row = 30; $operandSize=32;$col = 6;}
                        |EDI{mal.rex_rb = false; mal.modrm_row = 31; $operandSize=32;$col = 7;}
			|R8D{mal.rex_rb = true;$operandSize=32;mal.modrm_row = 241;$col = 0;}
			|R9D{mal.rex_rb = true;$operandSize=32;mal.modrm_row = 251;$col =1;}
			|R10D{mal.rex_rb = true;$operandSize=32;mal.modrm_row = 261;$col =2;}
			|R11D{mal.rex_rb = true;$operandSize=32;mal.modrm_row = 271;$col =3;}
			|R12D{mal.rex_rb = true;$operandSize=32;mal.modrm_row = 281;$col =4;}
			|R13D{mal.rex_rb = true;$operandSize=32;mal.modrm_row = 291;$col =5;}
			|R14D{mal.rex_rb = true;$operandSize=32;mal.modrm_row = 301;$col =6;}
			|R15D{mal.rex_rb = true;$operandSize=32;mal.modrm_row = 311;$col =7;}
			;

r64 returns [int operandSize,int col]:	{mal.is64 = true;}
                RAX {mal.rex_rb = false; mal.modrm_row = 241;$operandSize=64; $col = 0;}
	|	RBX {mal.rex_rb = false; mal.modrm_row = 271; $operandSize=64; $col =3;}
	|	RCX {mal.rex_rb = false; mal.modrm_row = 251; $operandSize=64;$col =1;}
	|       RDX {mal.rex_rb = false; mal.modrm_row = 261; $operandSize=64;$col =2;}
        |       RSP {mal.rex_rb = false; mal.modrm_row = 281; $operandSize=64;$col =4;}
        |       RBP {mal.rex_rb = false; mal.modrm_row = 291; $operandSize=64;$col =5;}
        |       RSI {mal.rex_rb = false; mal.modrm_row = 301; $operandSize=64;$col =6;}
        |       RDI {mal.rex_rb = false; mal.modrm_row = 311; $operandSize=64;$col =7;}
        |       R8  {mal.modrm_row = 241;  mal.rex_rb = true; $operandSize=64;$col =0;}
        |       R9  { mal.modrm_row = 251;  mal.rex_rb = true; $operandSize=64;$col =1;}
        |       R10  { mal.modrm_row = 261;  mal.rex_rb = true; $operandSize=64;$col =2;}
        |       R11  { mal.modrm_row = 271;  mal.rex_rb = true; $operandSize=64;$col =3;}
        |       R12  { mal.modrm_row = 281;  mal.rex_rb = true; $operandSize=64;$col =4;}
        |       R13  { mal.modrm_row = 291;  mal.rex_rb = true; $operandSize=64;$col =5;}
        |       R14  { mal.modrm_row = 301;  mal.rex_rb = true; $operandSize=64;$col = 6;}
        |       R15 { mal.modrm_row = 311;  mal.rex_rb = true; $operandSize=64;$col = 7;}
	;

r_m8 returns [boolean isReg]:	
                        r8 {$isReg = true;}
		|	m8{$isReg = false;}//{ mal.opsize = 8;}
		;

r_m16 returns [boolean isReg]:	r16 {$isReg = true;}
		|	m16{$isReg = false;}//{ mal.opsize = 16;}
		;

r_m32 returns [boolean isReg]:	r32{$isReg = true;}
		|	m32{$isReg = false;}//{ mal.opsize = 32;}
		;


r_m64 returns [boolean isReg]:	r64{$isReg = true;mal.is64=true;}
		|	m64{$isReg = false;mal.is64=true;}
                //{ mal.opsize = 64;}
		;


/*
r8	returns [int type, int typer]:	AL {$type=24;$typer=24;}|CL {$type=25;$typer=25;}|DL {$type=26;$typer=26;}|BL {$type=27;$typer=27;}|AH {$type=28;$typer=28;}|CH {$type=29;$typer=29;}|DH {$type=30;$typer=30;}|BH {$type=31;$typer=31;}|BPL {$type=29;$typer=29;}|SPL {$type=28;$typer=28;}|DIL {$type=31;$typer=31;}|SIL {$type=30;$typer=30;}|R8L{$type=24;$typer=32;}|R9L{$type=25;$typer=33;}|R10L{$type=26;$typer=34;}|R11L{$type=27;$typer=35;}|R12L{$type=28;$typer=36;}|R13L{$type=29;$typer=37;}|R14L{$type=30;$typer=38;}|R15L{$type=31;$typer=39;};
r16	returns [int type, int typer]:	AX{$type=24;$typer=24;}|CX{$type=25;$typer=25;}|DX{$type=26;$typer=26;}|BX{$type=27;$typer=27;}|SP{$type=28;$typer=28;}|BP{$type=29;$typer=29;}|SI{$type=30;$typer=30;}|DI{$type=31;$typer=31;}|R8W{$type=24;$typer=32;}|R9W{$type=25;$typer=33;}|R10W{$type=26;$typer=34;}|R11W{$type=27;$typer=35;}|R12W{$type=28;$typer=36;}|R13W{$type=29;$typer=37;}|R14W{$type=30;$typer=38;}|R15W{$type=31;$typer=39;};
r32	returns [int type, int typer]:	EAX{$type=24;$typer=24;}|ECX{$type=25;$typer=25;}|EDX{$type=26;$typer=26;}|EBX{$type=27;$typer=27;}|ESP{$type=28;$typer=28;}|EBP{$type=29;$typer=29;}|ESI{$type=30;$typer=30;}|EDI{$type=31;$typer=31;}|R8D{$type=24;$typer=32;}|R9D{$type=25;$typer=33;}|R10D{$type=26;$typer=34;}|R11D{$type=27;$typer=35;}|R12D{$type=28;$typer=36;}|R13D{$type=29;$typer=37;}|R14D{$type=30;$typer=38;}|R15D{$type=31;$typer=39;};
r64	returns [int type, int typer]:	RAX{$type=24;$typer=24;}|RCX{$type=25;$typer=25;}|RDX{$type=26;$typer=26;}|RBX{$type=27;$typer=27;}|RDI{$type=31;$typer=31;}|RSI{$type=30;$typer=30;}|RBP{$type=29;$typer=29;}|RSP{$type=28;$typer=28;}|R8{$type=24;$typer=32;}|R9{$type=25;$typer=33;}|R10{$type=26;$typer=34;}|R11{$type=27;$typer=35;}|R12{$type=28;$typer=36;}|R13{$type=29;$typer=37;}|R14{$type=30;$typer=38;}|R15{$type=31;$typer=39;};
*/

m8	returns [int operandSize, int expSize,String dispvalue]	:	BYTE_START (effectiveAddress16{$operandSize=8; $expSize=$effectiveAddress16.addressSize;mal.table16 =!$effectiveAddress16.isdisp32;$dispvalue = $effectiveAddress16.dispvalue;}|effectiveAddress32{$operandSize=8;$expSize=$effectiveAddress32.addressSize;$dispvalue = $effectiveAddress32.dispvalue;});
m16	returns [int operandSize, int expSize,String dispvalue]	:	WORD_START (effectiveAddress16{$operandSize=16;$expSize=$effectiveAddress16.addressSize;mal.table16=!$effectiveAddress16.isdisp32;$dispvalue = $effectiveAddress16.dispvalue;} |effectiveAddress32{$operandSize=16;$expSize=$effectiveAddress32.addressSize;$dispvalue = $effectiveAddress32.dispvalue;});
m32	returns [int operandSize, int expSize,String dispvalue]:	DWORD_START (effectiveAddress16{$operandSize=32;$expSize=$effectiveAddress16.addressSize;mal.table16 =!$effectiveAddress16.isdisp32;$dispvalue = $effectiveAddress16.dispvalue;} |effectiveAddress32{$operandSize=32;$expSize=$effectiveAddress32.addressSize;$dispvalue = $effectiveAddress32.dispvalue;}) ;
m64	returns [int operandSize, int expSize,String dispvalue]:	QWORD_START (effectiveAddress16 {$operandSize=64;$expSize=$effectiveAddress16.addressSize;mal.table16 =!$effectiveAddress16.isdisp32;$dispvalue = $effectiveAddress16.dispvalue;}|effectiveAddress32 {$operandSize=64;$expSize=$effectiveAddress32.addressSize;$dispvalue = $effectiveAddress32.dispvalue;}) ;

disp8	:	imm;
disp16	:	imm;
disp32	:	imm;

imm		:	MATH_EXPRESSION
		;

effectiveAddress16 returns [boolean memoryAccess, boolean register64Bits, int addressSize, String dispvalue,boolean isdisp32]
					:	OPEN_MID_BRACKET{$dispvalue = "no"; $isdisp32 = false;}
						(               BX ADD_ SI	{mal.modrm_row=0; $addressSize=16;}
							|	BX ADD_ DI			{mal.modrm_row=1; $addressSize=16;}
							|	BP ADD_ SI			{mal.modrm_row=2; $addressSize=16;}
							|	BP ADD_ DI			{mal.modrm_row=3; $addressSize=16;}
							|	SI				{mal.modrm_row=4; $addressSize=16;}
							|	DI				{mal.modrm_row=5; $addressSize=16;}
							|	disp16				{boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($disp16.text),16);
                                                                                                 boolean d_32 = !min && mal.bits==32; boolean d_64 = !min && mal.bits ==64; $isdisp32 = d_32||d_64;
                                                                                                 if(d_64){
                                                                                                    mal.encoder.init_SIB(true,5,4);
                                                                                                    mal.modrm_row = 4; $addressSize=64;
                                                                                                    $dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($disp16.text),32);
                                                                                                 }else if(d_32){
                                                                                                    mal.modrm_row = 5; $dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($disp16.text),32);
                                                                                                    $addressSize=32;
                                                                                                 }else{
                                                                                                    $addressSize=16; mal.modrm_row = 6;
                                                                                                    $dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($disp16.text),16);
                                                                                                 }
                                                                                                }
                                                        |	BX                              {mal.modrm_row=7; $addressSize=16;}
							|	BX ADD_ SI (ADD_ disp8)?	{$addressSize=16;$dispvalue = $disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8);mal.modrm_row=min?8:16; $dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:16); }
							|	BX ADD_ DI (ADD_ disp8)?	{$addressSize=16;$dispvalue = $disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8);mal.modrm_row=min?9:17; $dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:16); }
							|	BP ADD_ SI (ADD_ disp8)?	{$addressSize=16;$dispvalue = $disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8);mal.modrm_row=min?10:18;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:16); }
							|	BP ADD_ DI (ADD_ disp8)?	{$addressSize=16;$dispvalue = $disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8);mal.modrm_row=min?11:19;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:16); }
							|	SI (ADD_ disp8)?                {$addressSize=16;$dispvalue = $disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8);mal.modrm_row=min?12:20;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:16); }
							|	DI (ADD_ disp8)?                {$addressSize=16;$dispvalue = $disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8);mal.modrm_row=min?13:21;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:16); }
							|	BP (ADD_ disp8)?                {$addressSize=16;$dispvalue = $disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8);mal.modrm_row=min?14:22;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:16); }
							|	BX (ADD_ disp8)?                {$addressSize=16;$dispvalue = $disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8);mal.modrm_row=min?15:23;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:16); }
						
						)	CLOSE_MID_BRACKET										{$memoryAccess=true;}
						|	
						(
							(        EAX	{$register64Bits=true;}|AX|AL|MM0|XMM0)			{ mal.modrm_row=24;}
							|	(ECX	{$register64Bits=true;}|CX|CL|MM1|XMM1)			{ mal.modrm_row=25;}
							|	(EDX	{$register64Bits=true;}|DX|DL|MM2|XMM2)			{ mal.modrm_row=26;}
							|	(EBX	{$register64Bits=true;}|BX|BL|MM3|XMM3)			{ mal.modrm_row=27;}
							|	(ESP	{$register64Bits=true;}|SP|AH|MM4|XMM4)			{ mal.modrm_row=28;}
							|	(EBP	{$register64Bits=true;}|BP|CH|MM5|XMM5)			{ mal.modrm_row=29;}
							|	(ESI	{$register64Bits=true;}|SI|DH|MM6|XMM6)			{ mal.modrm_row=30;}
							|	(EDI	{$register64Bits=true;}|DI|BH|MM7|XMM7)			{ mal.modrm_row=31;}
						) 
					;

effectiveAddress32	returns [int type, boolean memoryAccess, boolean register64Bits, int addressSize,String dispvalue]
					:       OPEN_MID_BRACKET {$dispvalue = "no";$addressSize=32;mal.is64=true;mal.rex_rb =false;} 
                                                ( (EAX{mal.is64=false;}|RAX{mal.rex_rb = false;$addressSize=64;}|R8 { mal.rex_rb = true;$addressSize=64;})		{ mal.modrm_row=0;}
					|	(ECX{mal.is64=false;}|RCX{$addressSize=64;}|R9{ mal.rex_rb = true;$addressSize=64;})					{ mal.modrm_row=1;}
					|	(EDX{mal.is64=false;}|RDX{$addressSize=64;}|R10{ mal.rex_rb = true;$addressSize=64;})					{ mal.modrm_row=2;}
					|	(EBX{mal.is64=false;}|RBX{$addressSize=64;}|R11{ mal.rex_rb = true;$addressSize=64;})					{ mal.modrm_row=3;}
					|	sib     { mal.issib = true; mal.modrm_row= $sib.addWith8?12:4;
                                                          $dispvalue =$sib.addWith8?"0x00":$sib.dispvalue;mal.encoder.init_SIB(mal.issib,mal.sib_col,mal.sib_row);mal.encoder.initREX_X(mal.rex_x); $addressSize=mal.is64?64:32;}
					|	disp32	{ mal.is64=false;mal.modrm_row=5;$dispvalue =mal.encoder.handle_dispvalue(CalculatorLibrary.cal($disp32.text),32);}
					|	(ESI{mal.is64=false;}|RSI{$addressSize=64;}|R14{ mal.rex_rb = true;$addressSize=64;})					{ mal.modrm_row=6;}
					|	(EDI{mal.is64=false;}|RDI{$addressSize=64;}|R15{ mal.rex_rb = true;$addressSize=64;})					{ mal.modrm_row=7;}
					|	(EAX{mal.is64=false;}|RAX{$addressSize=64;}|R8 { mal.rex_rb = true;$addressSize=64;}) (ADD_ disp8)?		{$dispvalue =$disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8); mal.modrm_row=min?8:16;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:32); }
					|	(ECX{mal.is64=false;}|RCX{$addressSize=64;}|R9{ mal.rex_rb = true;$addressSize=64;})  (ADD_ disp8)?		{$dispvalue =$disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8); mal.modrm_row=min?9:17;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:32); }
					|	(EDX{mal.is64=false;}|RDX{$addressSize=64;}|R10{ mal.rex_rb = true;$addressSize=64;}) (ADD_ disp8)?		{$dispvalue =$disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8); mal.modrm_row=min?10:18;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:32); }
					|	(EBX{mal.is64=false;}|RBX{$addressSize=64;}|R11{ mal.rex_rb = true;$addressSize=64;}) (ADD_ disp8)?		{$dispvalue =$disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8); mal.modrm_row=min?11:19;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:32); }
					|	(EBP{mal.is64=false;}|RBP{$addressSize=64;}|R13{ mal.rex_rb = true;$addressSize=64;}) (ADD_ disp8)?		{$dispvalue =$disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8); mal.modrm_row=min?13:21;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:32); }
					|	(ESI{mal.is64=false;}|RSI{$addressSize=64;}|R14{ mal.rex_rb = true;$addressSize=64;}) (ADD_ disp8)?		{$dispvalue =$disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8); mal.modrm_row=min?14:22;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:32); }
					|	(EDI{mal.is64=false;}|RDI{$addressSize=64;}|R15{ mal.rex_rb = true;$addressSize=64;}) (ADD_ disp8)?		{$dispvalue =$disp8.text!=null?$disp8.text:"0x00";boolean min = mal.encoder.check_disp(CalculatorLibrary.cal($dispvalue),8); mal.modrm_row=min?15:23;$dispvalue = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($dispvalue),min?8:32); }
				
					)	CLOSE_MID_BRACKET								{$memoryAccess=true;}
					|	((EAX	{$register64Bits=true;}|AX|AL|MM0|XMM0)	{$type=24;}
					|	(ECX	{$register64Bits=true;}|CX|CL|MM1|XMM1)	{$type=25;}
					|	(EDX	{$register64Bits=true;}|DX|DL|MM2|XMM2)	{$type=26;}
					|	(EBX	{$register64Bits=true;}|BX|BL|MM3|XMM3)	{$type=27;}
					|	(ESP	{$register64Bits=true;}|SP|AH|MM4|XMM4)	{$type=28;}
					|	(EBP	{$register64Bits=true;}|BP|CH|MM5|XMM5)	{$type=29;}
					|	(ESI	{$register64Bits=true;}|SI|DH|MM6|XMM6)	{$type=30;}
					|	(EDI	{$register64Bits=true;}|DI|BH|MM7|XMM7)	{$type=31;}
					)
					;

sib returns [String dispvalue, boolean addWith8]        :	sibBaseRegisters	ADD_ sibScaledRegisters {$dispvalue = $sibScaledRegisters.dispvalue; $addWith8 = $sibScaledRegisters.sp_reg;}
                                                        |	sibScaledRegisters	ADD_ sibBaseRegisters   {$dispvalue = $sibScaledRegisters.dispvalue; $addWith8 = $sibScaledRegisters.sp_reg;}
                                                        |       sibScaledRegOnly    {$dispvalue = $sibScaledRegOnly.dispvalue; $addWith8 = $sibScaledRegOnly.sp_reg;}
                                                        ;

sibBaseRegisters         :{mal.is64=true; mal.rex_rb = false;}	(EAX{mal.is64=false;}|RAX|R8{ mal.rex_rb = true;mal.modrm_row = 241;}) { mal.sib_col=0;}
									|	(ECX{mal.is64=false;}|RCX|R9{ mal.rex_rb = true;mal.modrm_row = 251;}) { mal.sib_col=1;}
									|	(EDX{mal.is64=false;}|RDX|R10{ mal.rex_rb = true;mal.modrm_row = 261;}) { mal.sib_col=2;}
									|	(EBX{mal.is64=false;}|RBX|R11{ mal.rex_rb = true;mal.modrm_row = 271;}) { mal.sib_col=3;}
                                                                        |       (ESP{mal.is64=false;}|RSP|R12{ mal.rex_rb = true;mal.modrm_row = 281;}) { mal.sib_col = 4;}
                                                                        |       (EBP{mal.is64=false;}|RBP|R13{mal.modrm_row = 291;mal.rex_rb=true;}) {mal.sib_col =5;}
									|	(ESI{mal.is64=false;}|RSI|R14{mal.modrm_row = 301; mal.rex_rb = true;}) { mal.sib_col=6;}
									|	(EDI{mal.is64=false;}|RDI|R15{mal.modrm_row = 311; mal.rex_rb = true;}) { mal.sib_col=7;}
									;

sibScaledRegisters returns[String dispvalue,boolean sp_reg]          :                {mal.is64=true; $sp_reg = false;}            
                                                                                (EAX{mal.is64=false;}|RAX|R8{ mal.rex_x = true;})	{ mal.sib_row=0;$dispvalue ="no";}
									|	(ECX{mal.is64=false;}|RCX|R9{ mal.rex_x = true;})	{ mal.sib_row=1;$dispvalue ="no";}
									|	(EDX{mal.is64=false;}|RDX|R10{ mal.rex_x = true;})	{ mal.sib_row=2;$dispvalue ="no";}
									|	(EBX{mal.is64=false;}|RBX|R11{ mal.rex_x = true;})	{ mal.sib_row=3;$dispvalue ="no";}
                                                                        |	(ESP{mal.is64=false;}|RSP|R12{ mal.rex_x = true;})	{ mal.sib_row=4;$dispvalue ="no";}
									|	(EBP{mal.is64=false;}|RBP|R13{ mal.rex_x = true;})	{ mal.sib_row=5;$dispvalue ="no";}
									|	(ESI{mal.is64=false;}|RSI|R14{ mal.rex_x = true;})	{ mal.sib_row=6;$dispvalue ="no";}
									|	(EDI{mal.is64=false;}|RDI|R15{ mal.rex_x = true;})	{ mal.sib_row=7;$dispvalue ="no";}
                                                                        |       (EAX{mal.is64=false;}|RAX|R8{ mal.rex_x = true;})  TIME2  (ADD_ disp8)?         {mal.sib_row = 8; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(ECX{mal.is64=false;}|RCX|R9{ mal.rex_x = true;})  TIME2  (ADD_ disp8)?         {mal.sib_row = 9; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(EDX{mal.is64=false;}|RDX|R10{ mal.rex_x = true;}) TIME2  (ADD_ disp8)?         {mal.sib_row = 10; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(EBX{mal.is64=false;}|RBX|R11{ mal.rex_x = true;}) TIME2  (ADD_ disp8)?         {mal.sib_row = 11; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
                                                                        |	(ESP{mal.is64=false;}|RSP|R12{ mal.rex_x = true;}) TIME2  (ADD_ disp8)?         {mal.sib_row = 12; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(EBP{mal.is64=false;$sp_reg = true;}|RBP{$sp_reg = true;}|R13{ mal.rex_x = true;}) TIME2  (ADD_ disp8)?         {mal.sib_row = 13; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(ESI{mal.is64=false;}|RSI|R14{ mal.rex_x = true;}) TIME2  (ADD_ disp8)?         {mal.sib_row = 14; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(EDI{mal.is64=false;}|RDI|R15{ mal.rex_x = true;}) TIME2  (ADD_ disp8)?         {mal.sib_row = 15; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
                                                                        |       (EAX{mal.is64=false;}|RAX|R8{ mal.rex_x = true;})  TIME4  (ADD_ disp32)?        {mal.sib_row = 16; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(ECX{mal.is64=false;}|RCX|R9{ mal.rex_x = true;})  TIME4  (ADD_ disp32)?	{mal.sib_row = 17; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EDX{mal.is64=false;}|RDX|R10{ mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_row = 18; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EBX{mal.is64=false;}|RBX|R11{ mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_row = 19; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
                                                                        |	(ESP{mal.is64=false;}|RSP|R12{ mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_row = 20; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EBP{mal.is64=false;}|RBP|R13{ mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_row = 21; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(ESI{mal.is64=false;}|RSI|R14{ mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_row = 22; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EDI{mal.is64=false;}|RDI|R15{ mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_row = 23; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
                                                                        |       (EAX{mal.is64=false;}|RAX|R8{ mal.rex_x = true;})  TIME8  (ADD_ disp32)?        {mal.sib_row = 24; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(ECX{mal.is64=false;}|RCX|R9{ mal.rex_x = true;})  TIME8  (ADD_ disp32)?	{mal.sib_row = 25; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EDX{mal.is64=false;}|RDX|R10{ mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_row = 26; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EBX{mal.is64=false;}|RBX|R11{ mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_row = 27; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
                                                                        |	(ESP{mal.is64=false;}|RSP|R12{ mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_row = 28; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EBP{mal.is64=false;}|RBP|R13{ mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_row = 29; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(ESI{mal.is64=false;}|RSI|R14{ mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_row = 30; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EDI{mal.is64=false;}|RDI|R15{ mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_row = 31; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									;

sibScaledRegOnly     returns[String dispvalue,boolean sp_reg]                          :{mal.is64=true;mal.rex_rb = false; $sp_reg = false;}       
                                                                                (ESP{mal.is64=false;}|RSP|R12{mal.rex_rb  = true;mal.rex_x=true;})          {mal.sib_col = 4; mal.sib_row=4;$dispvalue ="no";}
                                                                        |       (EAX{mal.is64=false;}|RAX|R8{ mal.rex_rb = true;mal.rex_x = true;})  TIME2  (ADD_ disp8)?       {mal.sib_col = 0; mal.sib_row = 0; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(ECX{mal.is64=false;}|RCX|R9{ mal.rex_rb = true;mal.rex_x = true;})  TIME2  (ADD_ disp8)?       {mal.sib_col = 1; mal.sib_row = 1; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(EDX{mal.is64=false;}|RDX|R10{ mal.rex_rb = true;mal.rex_x = true;}) TIME2  (ADD_ disp8)?	{mal.sib_col = 2; mal.sib_row = 2; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(EBX{mal.is64=false;}|RBX|R11{ mal.rex_rb = true;mal.rex_x = true;}) TIME2  (ADD_ disp8)?	{mal.sib_col = 3; mal.sib_row = 3; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
                                                                        |       (ESP{mal.is64=false;}|RSP|R12{mal.rex_rb  = true;mal.rex_x=true;})   TIME2  (ADD_ disp8)?       {mal.sib_col = 4; mal.sib_row = 4; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(EBP{mal.is64=false;$sp_reg=true;}|RBP {$sp_reg =true;} |R13{ mal.rex_rb = true;mal.rex_x = true;}) TIME2  (ADD_ disp8)?	{mal.sib_col = 5; mal.sib_row = 5; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(ESI{mal.is64=false;}|RSI|R14{ mal.rex_rb = true;mal.rex_x = true;}) TIME2  (ADD_ disp8)?	{mal.sib_col = 6; mal.sib_row = 6; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
									|	(EDI{mal.is64=false;}|RDI|R15{ mal.rex_rb = true;mal.rex_x = true;}) TIME2  (ADD_ disp8)?	{mal.sib_col = 7; mal.sib_row = 7; $dispvalue =$disp8.text!=null?$disp8.text:"no";}
                                                                        |       (EAX{mal.is64=false;}|RAX|R8{ mal.rex_rb = true;mal.rex_x = true;})  TIME4  (ADD_ disp32)?      {mal.sib_col = 5; mal.sib_row = 16; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(ECX{mal.is64=false;}|RCX|R9{ mal.rex_rb = true;mal.rex_x = true;})  TIME4  (ADD_ disp32)?      {mal.sib_col = 5; mal.sib_row = 17; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EDX{mal.is64=false;}|RDX|R10{ mal.rex_rb = true;mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_col = 5; mal.sib_row = 18; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EBX{mal.is64=false;}|RBX|R11{ mal.rex_rb = true;mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_col = 5; mal.sib_row = 19; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
                                                                        |       (ESP{mal.is64=false;}|RSP|R12{mal.rex_rb  = true;mal.rex_x=true;})   TIME4  (ADD_ disp32)?      {mal.sib_col = 5; mal.sib_row = 20; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EBP{mal.is64=false;}|RBP|R13{ mal.rex_rb = true;mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_col = 5; mal.sib_row = 21; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(ESI{mal.is64=false;}|RSI|R14{ mal.rex_rb = true;mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_col = 5; mal.sib_row = 22; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EDI{mal.is64=false;}|RDI|R15{ mal.rex_rb = true;mal.rex_x = true;}) TIME4  (ADD_ disp32)?	{mal.sib_col = 5; mal.sib_row = 23; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
                                                                        |       (EAX{mal.is64=false;}|RAX|R8{ mal.rex_rb = true;mal.rex_x = true;})  TIME8  (ADD_ disp32)?      {mal.sib_col = 5; mal.sib_row = 24; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(ECX{mal.is64=false;}|RCX|R9{ mal.rex_rb = true;mal.rex_x = true;})  TIME8  (ADD_ disp32)?      {mal.sib_col = 5; mal.sib_row = 25; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EDX{mal.is64=false;}|RDX|R10{ mal.rex_rb = true;mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_col = 5; mal.sib_row = 26; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EBX{mal.is64=false;}|RBX|R11{ mal.rex_rb = true;mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_col = 5; mal.sib_row = 27; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
                                                                        |       (ESP{mal.is64=false;}|RSP|R12{mal.rex_rb  = true;mal.rex_x=true;})   TIME8  (ADD_ disp32)?      {mal.sib_col = 5; mal.sib_row = 28; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EBP{mal.is64=false;}|RBP|R13{ mal.rex_rb = true;mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_col = 5; mal.sib_row = 29; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(ESI{mal.is64=false;}|RSI|R14{ mal.rex_rb = true;mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_col = 5; mal.sib_row = 30; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									|	(EDI{mal.is64=false;}|RDI|R15{ mal.rex_rb = true;mal.rex_x = true;}) TIME8  (ADD_ disp32)?	{mal.sib_col = 5; mal.sib_row = 31; $dispvalue =$disp32.text!=null?$disp32.text:"0x00000000";}
									;

/*                                                                       ;
numbers		:	OPEN_SMALL_BRACKET numbers CLOSE_SMALL_BRACKET	# handle1
			|	left=numbers MUL_ right=numbers			# handleMultipy
			|	left=numbers ADD_ right=numbers			# handleAdd
			|	number											# dummy
			;

number		:	imm
			|	imm
			;
*/

assemble		:	lines EOF
			;

lines		:	line*
			;

line		:	bits16
		|	bits32
		|	bits64
			;

comment		:	LINE_COMMENT
			;

label		:	IDENTIFIER COLON LABEL_POSTFIX?
			;

instructions		:	aaa
				|	aad
				|	aam
				|	aas
				|	adc
//				|	adcx
				|	add
/*                              |       addpd
                                |       vaddpd
                                |       addps
                                |       vaddps
                                |       addsd
                                |       vaddsd
                                |       addss
                                |       vaddss
                                |       addsubpd
                                |       vaddsubpd
                                |       addsubps
                                |       vaddsubps*/
                                |       adox
                                /*|       aesdec
                                |       vaesdec
                                |       aesdeclast
                                |       vaesdeclast
                                |       aesenc
                                |       vaesenc
                                |       aesenclast
                                |       vaesenclast
                                |       aesimc
                                |       vaesimc
                                |       aeskeygenassist
                                |       vaeskeygenassist*/
                                |       and
                              /*  |       andn
                                |       andpd
                                |       vandpd
                                |       andps
                                |       vandps
                                |       andnpd
                                |       vandnpd
                                |       andnps
                                |       vandnps
                                |       arpl
                                |       bextr
                                |       blendpd
                                |       vblendpd
                                |       blendps
                                |       vblendps
                                |       blendvpd
                                |       vblendvpd
                                |       blendvps
                                |       vblendvps
                                |       blsi
                                |       blsmsk
                                |       blsr
                                |       bndcl
                                |       bndcu
                                |       bndcn
                                |       bndldx
                                |       bndmk
                                |       bndmov
                                |       bndstx
                                |       bound
                               */
                                |       bsf
                                |       bsr
                                |       bswap
                                |       bt
                                /*|       btc
                                |       btr*/
                                |       bts
                               /* |       bzhi
                                |       call
                                |       cbw
                                |       cwde
                                |       cdqe
                                |       clac
                                |       clc
                                |       cld
                                |       cldemote
                                |       clflush
                                |       clflushopt
                                |       cli
                                |       clts
                                |       clwb
                                |       cmc
                                |       cmova
                                |       cmovae
                                |       cmovb
                                |       cmovbe
                                |       cmovc
                                |       cmove
                                |       cmovg
                                |       cmovge
                                |       cmovl
                                |       cmovle
                                |       cmovna
                                |       cmovnae
                                |       cmovnb
                                |       cmovnbe
                                |       cmovnc
                                |       cmovne
                                |       cmovng
                                |       cmovnge
                                |       cmovnl
                                |       cmovnle
                                |       cmovno
                                |       cmovnp
                                |       cmovns
                                |       cmovnz
                                |       cmovo
                                |       cmovp
                                |       cmovpe
                                |       cmovpo
                                |       cmovs
                                |       cmovz*/
                                |       cmp
                                /*|       cmppd
                                |       vcmppd
                                |       cmpps
                                |       vcmpps
                                |       cmps
                                |       cmpsb
                                |       cmpsw
                                |       cmpsd
                                |       cmpsq
                                |       vcmpsd
                                |       cmpss
                                |       vcmpss
                                |       cmpxchg
                                |       cmpxchg8b
                                |       cmpxchg16b
                                |       comisd
                                |       vcomisd
                                |       comiss
                                |       vcomiss
                                |       cpuid
                                |       crc32
                                |       cvtdq2pd
                                |       vcvtdq2pd
                                |       cvtdq2ps
                                |       vcvtdq2ps
                                |       cvtpd2dq
                                |       vcvtpd2dq
                                |       cvtpd2pi
                                |       cvtpd2ps
                                |       vcvtpd2ps
                                |       cvtpi2pd
                                |       cvtpi2ps
                                |       cvtps2dq
                                |       vcvtps2dq
                                |       cvtps2pd
                                |       vcvtps2pd
                                |       cvtps2pi
                                |       cvtsd2si
                                |       vcvtsd2si
                                |       cvtsd2ss
                                |       vcvtsd2ss
                                |       cvtsi2sd
                                |       vcvtsi2sd
                                |       cvtsi2ss
                                |       vcvtsi2ss
                                |       cvtss2sd
                                |       vcvtss2sd
                                |       cvtss2si
                                |       vcvtss2si
                                |       cvttpd2dq
                                |       vcvttpd2dq
                                |       cvttpd2pi
                                |       cvttps2dq
                                |       vcvttps2dq
                                |       cvttps2pi
                                |       cvttsd2si
                                |       vcvttsd2si
                                |       cvttss2si
                                |       vcvttss2si*/
                                |       cwd
                                |       cdq
                                |       cqo
                                |       daa
                                |       das
                                |       dec
                                |       div
                              /*  |       divpd
                                |       vdivpd
                                |       divps
                                |       vdivps
                                |       divsd
                                |       vdivsd
                                |       divss
                                |       vdivss
                                |       dppd
                                |       vdppd
                                |       dpps
                                |       vdpps
                                |       emms
                                |       enter
                                |       extractps
                                |       vextractps
                                |       f2xm1
                                |       fabs
                                |       fadd
                                |       faddp
                                |       fiadd
                                |       fbld
                                |       fbstp
                                |       fchs
                                |       fclex
                                |       fnclex
                                |       fcmovb
                                |       fcmove
                                |       fcmovbe
                                |       fcmovu
                                |       fcmovnb
                                |       fcmovne
                                |       fcmovnbe
                                |       fcmovnu
                                |       fcom
                                |       fcomp
                                |       fcompp
                                |       fcomi
                                |       fcomip
                                |       fucomi
                                |       fucomip
                                |       fcos
                                |       fdecstp
                                |       fdiv
                                |       fdivp
                                |       fidiv
                                |       fdivr
                                |       fdivrp
                                |       fidivr
                                |       ffree
                                |       ficom
                                |       ficomp
                                |       fild
                                |       fincstp
                                |       finit
                                |       fninit
                                |       fist
                                |       fistp
                                |       fisttp
                                |       fld
                                |       fld1
                                |       fldl2t
                                |       fldl2e
                                |       fldpi
                                |       fldlg2
                                |       fldln2
                                |       fldz
                                |       fldcw
                                |       fldenv
                                |       fmul
                                |       fmulp
                                |       fimul
                                |       fnop
                                |       fpatan
                                |       fprem
                                |       fprem1
                                |       fptan
                                |       frndint
                                |       frstor
                                |       fsave
                                |       fnsave
                                |       fscale
                                |       fsin
                                |       fsincos
                                |       fsqrt
                                |       fst
                                |       fstp
                                |       fstcw
                                |       fnstcw
                                |       fstenv
                                |       fnstenv
                                |       fstsw
                                |       fnstsw
                                |       fsub
                                |       fsubp
                                |       fisub
                                |       fsubr
                                |       fsubrp
                                |       fisubr
                                |       ftst
                                |       fucom
                                |       ftcomp
                                |       fucompp
                                |       fxam
                                |       fxch
                                |       fxrstor
                                |       fxrstor64
                                |       fxsave
                                |       fxsave64
                                |       fxtract
                                |       fyl2x
                                |       fyl2xp1
                                |       gf2p8affineinvqb
                                |       gf2p8affineqb
                                |       gf2p8mulb
                                |       haddpd
                                |       vhaddpd
                                |       haddps
                                |       vhaddps
                                |       hlt
                                |       hsubpd
                                |       vhsubpd
                                |       hsubps
                                |       vhsubps
                                |       idiv
                                |       imul
                                |       in
                                |       inc
                                |       ins
                                |       insb
                                |       insw
                                |       insd
                                |       insertps
                                |       vinsertps
                                |       int3
          //                      |       int
                                |       int0
                                |       int1
                                |       invd
                                |       invlpg
                                |       invpcid
                                |       iret
                                |       iretd
                                |       iretq
                                |       ja
                                |       jae
                                |       jb
                                |       jbe
                                |       jc
                                |       jcxz
                                |       jecxz
                                |       jrcxz
                                |       je
                                |       jg
                                |       jge
                                |       jl
                                |       jle
                                |       jna
                                |       jnae
                                |       jnb
                                |       jnbe
                                |       jnc
                                |       jne
                                |       jng
                                |       jnge
                                |       jnl
                                |       jnle
                                |       jno
                                |       jnp
                                |       jns
                                |       jnz
                                |       jo
                                |       jp
                                |       jpe
                                |       jpo
                                |       js
                                |       jz
                                |       jmp
                                |       kaddw
                                |       kaddb
                                |       kaddq
                                |       kaddd
                                |       kandw
                                |       kandb
                                |       kandq
                                |       kandd
                                |       kandnw
                                |       kandnb
                                |       kandnq
                                |       kandnd
                                |       kmovw
                                |       kmovb
                                |       kmovq
                                |       kmovd
                                |       knotw
                                |       knotb
                                |       knotq
                                |       knotd
                                |       korw
                                |       korb
                                |       korq
                                |       kord
                                |       kortestw
                                |       kortestb
                                |       kortestq
                                |       kortestd
                                |       kshiftlw
                                |       kshiftlb
                                |       kshiftlq
                                |       kshiftld
                                |       kshiftrw
                                |       kshiftrb
                                |       kshiftrq
                                |       kshiftrd
                                |       ktestw
                                |       ktestb
                                |       ktestq
                                |       ktestd	
                                |       kunpckbw
                                |       kunpckwd
                                |       kunpckdq
                                |       kxnorw
                                |       kxnorb
                                |       kxnorq
                                |       kxnord
                                |       kxorw
                                |       kxorb
                                |       kxorq
                                |       kxord
                                |       lahf
                                |       lar
                                |       lddqu
                                |       vlddqu
                                |       ldmxcsr
                                |       vldmxcsr
                                |       lds
                                |       lss
                                |       les
                                |       lfs
                                |       lgs
                                |       lea
                                |       leave
                                |       lfence
                                |       lgdt
                                |       lidt
                                |       lldt
                                |       lmsw
                                |       lock
                                |       lods
                                |       lodsb
                                |       lodsw
                                |       lodsd
                                |       lodsq
                                |       loop
                                |       loope
                                |       loopne
                                |       lsl
                                |       ltr
                                |       lzcnt
                                |       maskmovdqu
                                |       vmaskmovdqu
                                |       maskmovq
                                |       maxpd
                                |       vmaxpd
                                |       maxps
                                |       vmaxps
                                |       maxsd
                                |       vmaxsd
                                |       maxss
                                |       vmaxss
                                |       mfence
                                |       minpd
                                |       vminpd
                                |       minps
                                |       vminps
                                |       minsd
                                |       vminsd
                                |       minss
                                |       vminss
                                |       monitor
                                |       mov
                                |       movapd
                                |       vmovapd
                                |       movaps
                                |       vmovaps
                                |       movbe
                                |       movd
                                |       movq
                                |       vmovd
                                |       vmovq
                                |       movddup
                                |       vmovddup
                                |       movdiri
                                |       movdir64b
                                |       movdqa
                                |       vmovdqa
                                |       vmovdqa32
                                |       vmovdqa64
                                |       movdqu
                                |       vmovdqu
                                |       vmovdqu8
                                |       vmovdqu16
                                |       vmovdqu32
                                |       vmovdqu64
                                |       movdq2q
                                |       movhlps
                                |       vmovhlps
                                |       movhpd
                                |       vmovhpd
                                |       movhps
                                |       vmovhps
                                |       movlhps
                                |       vmovlhps
                                |       movlpd
                                |       vmovlpd
                                |       movlps
                                |       vmovlps
                                |       movmskpd
                                |       vmovmskpd
                                |       movmskps
                                |       vmovmskps
                                |       movntdqa
                                |       vmovntdqa
                                |       movntdq
                                |       vmovntdq
                                |       movnti
                                |       movntpd
                                |       vmovntpd
                                |       movntps
                                |       vmovntps
                                |       movntq
                             //   |       movq
                             //   |       vmovq
                                |       movq2dq
                                |       movs
                                |       movsb
                                |       movsw
                                |       movsd
                                |       movsq
                                |       movsd
                                |       vmovsd
                                |       movshdup
                                |       vmovshdup
                                |       movsldup
                                |       vmovsldup
                                |       movss
                                |       vmovss
                                |       movsx
                                |       movsxd
                                |       movupd
                                |       vmovupd
                                |       movups
                                |       vmovups
                                |       movzx
                                |       mpsadbw
                                |       vmpsadbw
                                |       mul
                                |       mulpd
                                |       vmulpd
                                |       mulps
                                |       vmulps
                                |       mulsd
                                |       vmulsd
                                |       mulss
                                |       vmulss
                                |       mulx
                                |       mwait
                                |       neg
                                |       nop
                                |       not
                                |       or
                                |       orpd
                                |       vorpd
                                |       orps
                                |       vorps
                                |       out
                                |       outs
                                |       outsb
                                |       outsw
                                |       outsd
                                |       pabsb
                                |       pabsw
                                |       pabsd
                                |       vpabsb
                                |       vpabsw
                                |       vpabsd
                                |       vpabsq */
                      /*          |       packsswb
                                |       packssdw
                                |       vpacksswb
                                |       vpackssdw
                                |       packusdw
                                |       vpackusdw
                                |       packuswb
                                |       vpackuswb
                                |       paddb
                                |       paddw
                                |       paddd
                                |       paddq
                                |       vpaddb
                                |       vpaddw
                                |       vpaddd
                                |       vpaddq
                                |       paddsb
                                |       paddsw
                                |       vpaddsb
                                |       vpaddsw
                                |       paddusb
                                |       paddusw
                                |       vpaddusb
                                |       vpaddusw
                                |       palignr
                                |       vpalignr
                                |       pand
                                |       vpand
                                |       vpandd
                                |       vpandq
                                |       pandn
                                |       vpandn
                                |       vpandnd
                                |       vpandnq
                                |       pause
                                |       pavgb
                                |       pavgw
                                |       vpavgb
                                |       vpavgw
                                |       pblendvb
                                |       vpblendvb
                                |       pblendw
                                |       vpblendw
                                |       pclmulqdq
                                |       vpclmulqdq
                                |       pcmpeqb
                                |       pcmpeqw
                                |       pcmpeqd
                                |       vpcmpeqb
                                |       vpcmpeqw
                                |       vpcmpeqd
                                |       pcmpeqq
                                |       vpcmpeqq
                                |       pcmpestri
                                |       vpcmpestri
                                |       pcmpestrm
                                |       vpcmpestrm
                                |       pcmpgtb
                                |       pcmpgtw
                                |       pcmpgtd
                                |       vpcmpgtb
                                |       vpcmpgtw
                                |       vpcmpgtd
                                |       pcmpgtq
                                |       vpcmpgtq
                                |       pcmpistri
                                |       vpcmpistri
                                |       pcmpistrm
                                |       vpcmpistrm
                                |       pdep
                                |       pext
                                |       pextrb
                                |       pextrd
                                |       pextrq
                                |       vpextrb
                                |       vpextrd
                                |       vpextrq
                                |       pextrw
                                |       vpextrw
                                |       phaddw
                                |       phaddd
                                |       vphaddw
                                |       vphaddd
                                |       phaddsw
                                |       vphaddsw
                                |       phminposuw
                                |       vphminposuw
                                |       phsubw
                                |       phsubd
                                |       vphsubw
                                |       vphsubd
                                |       phsubsw
                                |       vphsubsw
                                |       pinsrb
                                |       pinsrd
                                |       pinsrq
                                |       vpinsrb
                                |       vpinsrd
                                |       vpinsrq
                                |       pinsrw
                                |       vpinsrw
                                |       pmaddubsw
                                |       vpmaddubsw
                                |       pmaddwd
                                |       vpmaddwd
                                |       pmaxsw
                                |       pmaxsb
                                |       pmaxsd
                                |       vpmaxsb
                                |       vpmaxsw
                                |       vpmaxsd
                                |       pmaxub
                                |       pmaxuw
                                |       vpmaxub
                                |       vpmaxuw
                                |       pmaxud
                                |       vpmaxud
                                |       vpmaxuq
                                |       pminsw
                                |       pminsb
                                |       vpminsb
                                |       vpminsw
                                |       pminsd
                                |       vpminsd
                                |       vpminsq
                                |       pminub
                                |       pminuw
                                |       vpminub
                                |       vpminuw
                                |       pminud
                                |       vpminud
                                |       vpminuq
                                |       pmovmskb
                                |       vpmovmskb
                                |       pmovsxbw
                                |       pmovsxbd
                                |       pmovsxbq
                                |       pmovsxwd
                                |       pmovsxwq
                                |       pmovsxdq
                                |       vpmovsxbw
                                |       vpmovsxbd
                                |       vpmovsxbq
                                |       vpmovsxwd
                                |       vpmovsxwq
                                |       vpmovsxdq
                                |       pmovzxbw
                                |       pmovzxbd
                                |       pmovzxbq
                                |       pmovzxwd
                                |       pmovzxwq
                                |       pmovzxdq
                                |       vpmovzxbw
                                |       vpmovzxbd
                                |       vpmovzxbq
                                |       vpmovzxwd
                                |       vpmovzxwq
                                |       vpmovzxdq
                                |       pmuldq
                                |       vpmuldq
                                |       pmulhrsw
                                |       vpmulhrsw
                                |       pmulhuw
                                |       vpmulhuw
                                |       pmulhw
                                |       vpmulhw
                                |       pmulld
                                |       vpmulld
                                |       vpmullq
                                |       pmullw
                                |       vpmullw
                                |       pmuludq
                                |       vpmuludq
                                |       pop
                                |       popa
                                |       popad
                                |       popcnt
                                |       popf
                                |       popfd
                                |       popfq
                                |       por
                                |       vpor
                                |       vpord
                                |       vporq
                                |       prefetcht0
                                |       prefetcht1
                                |       prefetcht2
                                |       prefetchta
                                |       prefetchw
                                |       psadbw
                                |       vpsadbw
                                |       pshufb
                                |       vpshufb
                                |       pshufd
                                |       vpshufd
                                |       pshufhw
                                |       vpshufhw
                                |       pshuflw
                                |       vpshuflw
                                |       pshufw
                                |       psignb
                                |       psignw
                                |       psignd
                                |       vpsignb
                                |       vpsignw
                                |       vpsignd
                                |       pslldq
                                |       vpslldq
                                |       psllw
                                |       pslld
                                |       psllq
                                |       vpsllw
                                |       vpslld
                                |       vpsllq
                                |       psraw
                                |       psrad
                                |       vpsraw
                                |       vpsrad
                                |       vpsraq
                                |       psrldq
                                |       vpsrldq
                                |       psrlw
                                |       psrld
                                |       psrlq
                                |       vpsrlw
                                |       vpsrld
                                |       vpsrlq
                                |       psubb
                                |       psubw
                                |       psubd
                                |       vpsubb
                                |       vpsubw
                                |       vpsubd
                                |       psubq
                                |       vpsubq
                                |       psubsb
                                |       psubsw
                                |       vpsubsb
                                |       vpsubsw
                                |       psubusb
                                |       psubusw
                                |       ptest
                                |       vptest
                                |       ptwrite
                                |       punpckhbw
                                |       punpckhbd
                                |       punpckhbq
                                |       punpckhqdq
                                |       vpunpckhbw
                                |       vpunpckhwd
                                |       vpunpckhdq
                                |       vpunpckhqdq
                                |       punpcklbw
                                |       punpcklbd
                                |       punpcklbq
                                |       punpcklqdq
                                |       vpunpcklbw
                                |       vpunpcklwd
                                |       vpunpckldq
                                |       vpunpcklqdq
                                |       push
                                |       pusha
                                |       pushad
                                |       puhsf
                                |       pushfd
                                |       pushfq
                                |       pxor
                                |       vpxor
                                |       vpxord
                                |       vpxorq
                                |       rcl
                                |       rcr
                                |       rol
                                |       rcpps
                                |       vrcpps
                                |       rcpss
                                |       vrcpss
                                |       rdfsbase
                                |       pdgsbase
                                |       rdmsr
                                |       rdpid
                                |       rdpkru
                                |       rdpmc
                                |       rdrand
                                |       rdseed
                                |       rdtsc
                                |       rdtscp
                                |       rep ins
                                |       rep movs
                                |       rep outs
                                |       rep lods
                                |       rep stos
                                |       repe cmps
                                |       repe scas
                                |       repne cmps
                                |       repne scas
                                |       ret
                                |       rorx
                                |       roundpd
                                |       vroundpd
                                |       roundps
                                |       vroundps
                                |       roundsd
                                |       vroundsd
                                |       roundss
                                |       vroundss
                                |       rsm
                                |       rsqrtps
                                |       vrsqrtps
                                |       vsqrtss
                                |       vrsqrtss
                                |       sahf
                                |       sal
                                |       sar
                                |       shr
                                |       sarx
                                |       shlx
                                |       shrx
                                |       sbb
                                |       scas
                                |       scasb
                                |       scasw
                                |       scasd
                                |       scasq
                                |       seta
                                |       setae
                                |       setb
                                |       setbe
                                |       setc
                                |       sete
                                |       setg
                                |       setge
                                |       setl
                                |       setle
                                |       setna
                                |       setnae
                                |       setnb
                                |       setnbe
                                |       setnc
                                |       setne
                                |       setng
                                |       setnge
                                |       setnl
                                |       setnle
                                |       setno
                                |       setnp
                                |       setns
                                |       setnz
                                |       seto
                                |       setp
                                |       setpe
                                |       setpo
                                |       sets
                                |       setz
                                |       sfence
                                |       sgdt
                                |       sha1rnds4
                                |       sha1nexte
                                |       sha1msg1
                                |       sha1msg2
                                |       sha256rnds2
                                |       sha256msg1
                                |       sha256msg2
                                |       shld
                                |       shrd
                                |       shufpd
                                |       vshufpd
                                |       shufps
                                |       vshufps
                                |       sidt
                                |       sldt
                                |       smsw
                                |       sqrtpd
                                |       vsqrtpd
                                |       sqrtps
                                |       vsqrtps
                                |       sqrtsd
                                |       vsqrtsd
                                |       sqrtss
                                |       vsqrtss
                                |       stac
                                |       stc
                                |       std
                                |       sti
                                |       stmxcsr
                                |       vstmxcsr
                                |       stos
                                |       stosb
                                |       stosw
                                |       stosd
                                |       stosq
                                |       str
                                |       sub
                                |       subpd
                                |       vsubpd
                                |       subps
                                |       vsubps
                                |       subsd
                                |       vsubsd
                                |       subss
                                |       vsubss
                                |       swapgs
                                |       syscall
                                |       sysenter
                                |       sysexit
                                |       sysret
                                |       test
                                |       tpause
                                |       tzcnt
                                |       ucomisd
                                |       vucomisd
                                |       ucomiss
                                |       vucomiss
                                |       ud0
                                |       ud1
                                |       ud2
                                |       umonitor
                                |       umwait
                                |       unpckhpd
                                |       vunpckhpd
                                |       unpckhps
                                |       vunpckhps
                                |       unpcklpd
                                |       vunpcklpd
                                |       unpcklps
                                |       vunpcklps
                                |       valignd
                                |       valignq
                                |       vblendmpd
                                |       vblendmps
                                |       vbroadcastss
                                |       vbroadcastsd
                                |       vbroadcastf128
                                |       vbroadcastf32x2
                                |       vbroadcastf32x4
                                |       vbroadcastf64x2
                                |       vbroadcastf32x8
                                |       vbroadcastf64x4
                                |       vcompresspd
                                |       vcompressps
                                |       vcvtpd2qq
                                |       vcvtpd2udq
                                |       vcvtpd2uqq
                                |       vcvtph2ps
                                |       vcvtps2ph
                                |       vcvtps2udq
                                |       vcvtps2qq
                                |       vcvtps2uqq
                                |       vcvtqq2pd
                                |       vcvtqq2ps
                                |       vcvtsd2usi
                                |       vcvtss2usi
                                |       vcvttpd2qq
                                |       vcvttpd2udq
                                |       vcvttpd2uqq
                                |       vcvttps2udq
                                |       vcvttps2qq
                                |       vcvttps2uqq
                                |       vcvttsd2usi
                                |       vcvttss2usi
                                |       vcvtudq2pd
                                |       vcvtudq2ps
                                |       vcvtuqq2pd
                                |       vcvtuqq2ps
                                |       vcvtusi2sd
                                |       vcvtusi2ss
                                |       vdbpsadbw
                                |       vexpandpd
                                |       vexpandps
                                |       verr
                                |       verw
                                |       vextractf128
                                |       vextracti128
                                |       vfixupimmpd
                                |       vfixupimmps
                                |       vfixupimmsd
                                |       vfixupimmss
                                |       vfmadd132pd
                                |       vfmadd123pd
                                |       vfmadd231pd
                                |       vfmadd132ps
                                |       vfmadd213ps
                                |       vfmadd231ps
                                |       vfmadd132sd
                                |       vfmadd213sd
                                |       vfmadd231sd
                                |       vfmadd132ss
                                |       vfmadd213ss
                                |       vfmadd231ss
                                |       vfmaddsub132pd
                                |       vfmaddsub213pd
                                |       vfmaddsub231pd
                                |       vfmaddsub132ps
                                |       vfmaddsub213ps
                                |       vfmaddsub231ps
                                |       vfmsubadd132pd
                                |       vfmsubadd213pd
                                |       vfmsubadd231pd
                                |       vfmsubadd132ps
                                |       vfmsubadd213ps
                                |       vfmsubadd231ps
                                |       vfmsub132pd
                                |       vfmsub213pd
                                |       vfmsub231pd
                                |       vfmsub132ps
                                |       vfmsub213ps
                                |       vfmsub231ps
                                |       vfmsub132sd
                                |       vfmsub213sd
                                |       vfmsub231sd
                                |       vfmsub132ss
                                |       vfmsub213ss
                                |       vfmsub231ss
                                |       vfnmadd132pd
                                |       vfnmadd213pd
                                |       vfnmadd231pd
                                |       vfnmadd132ps
                                |       vfnmadd213ps
                                |       vfnmadd231ps
                                |       vfnmadd132sd
                                |       vfnmadd213sd
                                |       vfnmadd231sd
                                |       vfnmadd132ss
                                |       vfnmadd213ss
                                |       vfnmadd231ss
                                |       vfnmsub132pd
                                |       vfnmsub213pd
                                |       vfnmsub231pd
                                |       vfnmsub132ps
                                |       vfnmsub213ps
                                |       vfnmsub231ps
                                |       vfnmsub132sd
                                |       vfnmsub213sd
                                |       vfnmsub231sd
                                |       vfnmsub132ss
                                |       vfnmsub213ss
                                |       vfnmsub231ss
                                |       vfpclasspd
                                |       vfpclassps
                                |       vfpclasssd
                                |       vfpclassss
                                |       vgatherdpd
                                |       vgatherqpd
                                |       vgatherdps
                                |       vgatherqps
                                |       vgetexppd
                                |       vgetexpps
                                |       vgetexpsd
                                |       vgetexpss
                                |       vgetmantpd
                                |       vgetmantps
                                |       vgetmantsd
                                |       vgetmantss
                                |       vinsertf128
                                |       vinsertf32x4
                                |       vinsertf64x2
                                |       vinsertf32x8
                                |       vinsertf64x4
                                |       vinserti128
                                |       vinserti32x4
                                |       vinserti64x2
                                |       vinserti32x8
                                |       vinserti64x4
                                |       vmaskmovps
                                |       vmaskmovpd
                                |       vpblendd
                                |       vpblendmb
                                |       vpblendmw
                                |       vpblendmd
                                |       vpblendmq
                                |       vpbroadcastb
                                |       vpbroadcastw
                                |       vpbroadcastd
                                |       vpbrpadcastq
                                |       vbroadcasti32x2
                                |       vbroadcasti128
                                |       vbroadcasti32x4
                                |       vbroadcasti64x2
                                |       vbroadcasti32x8
                                |       vbroadcasti64x4
                                |       vpbroadcastmb2q
                                |       vpbroadcastmw2d
                                |       vpcmpb
                                |       vpcmpub
                                |       vpcmpd
                                |       vpcmpud
                                |       vpcmpq
                                |       vpcmpuq
                                |       vpcmpw
                                |       vpcmpuw
                                |       vpcompressd
                                |       vpcompressq
                                |       vpconflictd
                                |       vpconflictq
                                |       vperm2f128
                                |       vperm2i128
                                |       vpermb
                                |       vpermd
                                |       vpermw
                                |       vpermi2b
                                |       vpermi2w
                                |       vpermi2d
                                |       vpermi2q
                                |       vpermi2ps
                                |       vpermi2pd
                                |       vpermilpd
                                |       vpermilps
                                |       vpermpd
                                |       vpermps
                                |       vpermq
                                |       vpermt2b
                                |       vpermt2w
                                |       vpermt2d
                                |       vpermt2q
                                |       vpermt2ps
                                |       vpermt2pd
                                |       vpexpandd
                                |       vpexpandq
                                |       vpgatherdd
                                |       vpgatherqd
                                |       vpgatherdq
                                |       vpgatherqq
                                |       vplzcntd
                                |       vplzcntq
                                |       vpmadd52huq
                                |       vpmadd52luq
                                |       vpmaskmovd
                                |       vpmaskmovq
                                |       vpmovb2m
                                |       vpmovdb
                                |       vpmovsdb
                                |       vpmovusdb
                                |       vpmovdw
                                |       vpmovsdw
                                |       vpmovusdw
                                |       vpmovm2b
                                |       vpmovm2w
                                |       vpmovm2d
                                |       vpmovm2q
                                |       vpmovqb
                                |       vpmovsqb
                                |       vpmovusqb
                                |       vpmovqd
                                |       vpmovsqd
                                |       vpmovusqd
                                |       vpmovqw
                                |       vpmovsqw
                                |       vpmovusqw
                                |       vpmovwb
                                |       vpmovswb
                                |       vpmovuswb
                                |       vpmultishiftqb
                                |       vprolvd
                                |       vprold
                                |       vprolvq
                                |       vprolq
                                |       vprorvd
                                |       vprord
                                |       vprorvq
                                |       vprorq
                                |       vpscatterdd
                                |       vpscatterdq
                                |       vpscatterqd
                                |       vpscatterqq
                                |       vpsllvd
                                |       vpsllvq
                                |       vpsllvw
                                |       vpsravd
                                |       vpsravw
                                |       vpsravq
                                |       vpsrlvd
                                |       vpsrlvq
                                |       vpsrlvw
                                |       vpternlogd
                                |       vpternlogq
                                |       vptesmb
                                |       vptesmw
                                |       vptesmd
                                |       vptesmq
                                |       vptestnmb
                                |       vptestnmw
                                |       vptestnmd
                                |       vptestnmq
                                |       vrangepd
                                |       vrangeps
                                |       vrangesd
                                |       vrangess
                                |       vrcp14pd
                                |       vrcp14sd
                                |       vrcp14ps
                                |       vrcp14ss
                                |       vreducepd
                                |       vreducesd
                                |       vreduceps
                                |       vreducess
                                |       vrndscalepd
                                |       vrndscalesd
                                |       vrndscaleps
                                |       vrndscaless
                                |       vrsqrt14pd
                                |       vrsqrt14sd
                                |       vrsqrt14ps
                                |       vrsqrt14ss
                                |       vscalefpd
                                |       vscalefsd
                                |       vscalefps
                                |       vscalefss
                                |       vscatterdps
                                |       vscatterdpd
                                |       vscatterqps
                                |       vscatterqpd
                                |       vshuff32x4
                                |       vshuff64x2
                                |       vshufi32x4
                                |       vshufi64x2
                                |       vtestps
                                |       vtestpd
                                |       vzeroall
                                |       vzeroupper
                                |       wait
                                |       fwait
                                |       wbinvd
                                |       wrfsbase
                                |       wrgsbase
                                |       wrmsr
                                |       wrpkru
                                |       xacquire
                                |       xrelease
                                |       xabort
                                |       xadd
                                |       xbegin
                                |       xchg
                                |       xend
                                |       xgetbv
                                |       xlat
                                |       xlatb
                                |       xor
                                |       xorpd
                                |       vxorpd
                                |       xorps
                                |       vxorps
                                |       xrstor
                                |       xrstor64
                                |       xrstors
                                |       xrstors64
                                |       xsave
                                |       xsave64
                                |       xsavec
                                |       xsavec64
                                |       xsaveopt
                                |       xsaveopt64
                                |       xsaves
                                |       xsaves64
                                |       xsetbv
                                |       xtest
                                ; 
*/
			;

aaa			:       AAA     { mal.encoder.output_opmode(1,"0x37");};

aad			:       AAD           { mal.encoder.output_opmode(1,"0xD5","0x0A");}
			|       AAD imm      {  String imm_value = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($imm.text),8);
                                                mal.encoder.output_opmode(1,"0xD5", imm_value);}
			;

aam			:       AAM		{ mal.encoder.output_opmode(1,"0xD4","0x0A");}
			|       AAM imm		{   String imm_value = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($imm.text),8);
                                                    mal.encoder.output_opmode(1,"0xD4", imm_value);}
			;

aas			:	AAS { mal.encoder.output_opmode(1,"0x3F");}
			;   


adc				:	ADC AL		COMMA imm		{   mal.encoder.leftInit(
                                                                                            true,
                                                                                            8,
                                                                                            8,
                                                                                            2,
                                                                                            24,
                                                                                            "no",
                                                                                            mal.rex_rb
                                                                                    );
                                                                                    mal.encoder.rightInit(
                                                                                            false,
                                                                                            true,
                                                                                            8,
                                                                                            8,
                                                                                            2,
                                                                                            24,
                                                                                            "no",
                                                                                            false
                                                                                    );
                                                                                    //if(mal.encoder.check_disp(CalculatorLibrary.cal($imm.text),8)){
                                                                                         mal.encoder.checkPrefix();
                                                                                         String imm_value = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($imm.text),8);
                                                                                         mal.encoder.output(false,"0x14",imm_value);
                                                                                   /* }else{
                                                                                        String a[] = {"0x80","0x81","0x83"}; 
                                                                                        mal.encoder.checkPrefix();
                                                                                        mal.encoder.output_imm(a,CalculatorLibrary.cal($imm.text));
                                                                                    }*/}
				|	ADC AX		COMMA imm		{ mal.encoder.leftInit(
                                                                                            true,
                                                                                            16,
                                                                                            16,
                                                                                            2,
                                                                                            24,
                                                                                            "no",
                                                                                            mal.rex_rb
                                                                                    );
                                                                                    mal.encoder.rightInit(
                                                                                            false,
                                                                                            true,
                                                                                            16,
                                                                                            16,
                                                                                            2,
                                                                                            24,
                                                                                            "no",
                                                                                            false
                                                                                    );
                                                                                    if(mal.encoder.check_disp(CalculatorLibrary.cal($imm.text),16)){
                                                                                        mal.encoder.checkPrefix();
                                                                                        String imm_value = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($imm.text),16);
                                                                                        mal.encoder.output(false,"0x15",imm_value);
                                                                                    }else{
                                                                                        String a[] = {"0x80","0x81","0x83"};
                                                                                        mal.encoder.checkPrefix();
                                                                                        mal.encoder.output_imm(a,CalculatorLibrary.cal($imm.text),32);
                                                                                    }
                                                                                }
				|	ADC EAX		COMMA imm		{ mal.encoder.leftInit(
                                                                                            true,
                                                                                            32,
                                                                                            32,
                                                                                            2,
                                                                                            24,
                                                                                            "no",
                                                                                            mal.rex_rb
                                                                                    );
                                                                                    mal.encoder.rightInit(
                                                                                            false,
                                                                                            true,
                                                                                            32,
                                                                                            32,
                                                                                            2,
                                                                                            24,
                                                                                            "no",
                                                                                            false
                                                                                    );
                                                                                if(mal.encoder.check_disp(CalculatorLibrary.cal($imm.text),8)){    
                                                                                    String a[] = {"0x80","0x81","0x83"}; 
                                                                                    mal.encoder.checkPrefix();
                                                                                    mal.encoder.output_imm(a,CalculatorLibrary.cal($imm.text),32);
                                                                                }else{
                                                                                    String imm_value = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($imm.text),32);
                                                                                    mal.encoder.checkPrefix();
                                                                                    mal.encoder.output(false,"0x15",imm_value);
                                                                                }
                                                                               }
				|	ADC RAX		COMMA imm		{ mal.encoder.leftInit(
                                                                                            true,
                                                                                            64,
                                                                                            64,
                                                                                            2,
                                                                                            24,
                                                                                            "no",
                                                                                            mal.rex_rb
                                                                                    );
                                                                                    mal.encoder.rightInit(
                                                                                            false,
                                                                                            true,
                                                                                            64,
                                                                                            64,
                                                                                            2,
                                                                                            24,
                                                                                            "no",
                                                                                            false
                                                                                    );
                                                                                    if(mal.encoder.check_condition(false,true) ){
											if(mal.encoder.check_disp(CalculatorLibrary.cal($imm.text),64)){    
                                                                                            mal.encoder.checkPrefix();
                                                                                            String imm_value = mal.encoder.handle_dispvalue(CalculatorLibrary.cal($imm.text),32);
                                                                                            mal.encoder.output(true,"0x15",imm_value);
                                                                                        }else{
                                                                                            String a[] = {"0x80","0x81","0x83"}; 
                                                                                            mal.encoder.checkPrefix();
                                                                                            mal.encoder.output_imm(a,CalculatorLibrary.cal($imm.text),32);
                                                                                        }}
                                                                                    }
				
				|   { mal.leftIsReg = true;  mal.rightIsReg = true;}ADC adc_left COMMA adc_right
                                ;

adc_left:	r_m8		{
						boolean isReg = $r_m8.ctx.isReg;
						mal.encoder.leftInit(
							isReg,
							isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize,
							isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().expSize,
							isReg?$r_m8.ctx.r8().col:0,
							mal.modrm_row,
							isReg?"no":$r_m8.ctx.m8().dispvalue,
							isReg?mal.rex_rb:false
						);

						/*mal.leftIsReg = $r_m8.ctx.isReg;  mal.leftOpSize =  mal.leftIsReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize;
						mal.opcol =  mal.leftIsReg?$r_m8.ctx.r8().col:0;  mal.oprow = mal.modrm_row;
                                                mal.rex_b = mal.leftIsReg? mal.rex_rb: false;
						mal.leftExpSize =  mal.leftIsReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().expSize;
                                                mal.disp = mal.leftIsReg?"no":$r_m8.ctx.m8().dispvalue;*/
				}
		|	r_m16	{	
						boolean isReg = $r_m16.ctx.isReg;
						mal.encoder.leftInit(
							isReg,
							isReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize,
							isReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().expSize,
							isReg?$r_m16.ctx.r16().col:0,
							mal.modrm_row,
							isReg?"no":$r_m16.ctx.m16().dispvalue,
							isReg?mal.rex_rb:false
						);

						/*mal.leftIsReg = $r_m16.ctx.isReg;  mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
                                                mal.opcol =  mal.leftIsReg?$r_m16.ctx.r16().col:0;  mal.oprow = mal.modrm_row;
                                                mal.rex_b = mal.leftIsReg? mal.rex_rb: false;
						mal.leftExpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().expSize;
                                                mal.disp = mal.leftIsReg?"no":$r_m16.ctx.m16().dispvalue;*/
                                        }
		|	r_m32	{	
						boolean isReg = $r_m32.ctx.isReg;
						mal.encoder.leftInit(
							isReg,
							isReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize,
							isReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().expSize,
							isReg?$r_m32.ctx.r32().col:0,
							mal.modrm_row,
							isReg?"no":$r_m32.ctx.m32().dispvalue,
							isReg?mal.rex_rb:false
						);

						/*mal.leftIsReg = $r_m32.ctx.isReg;  mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
                                                mal.opcol =  mal.leftIsReg?$r_m32.ctx.r32().col:0;  mal.oprow = mal.modrm_row;
                                                mal.rex_b = mal.leftIsReg? mal.rex_rb: false;
						mal.leftExpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().expSize;
                                                mal.disp = mal.leftIsReg?"no":$r_m32.ctx.m32().dispvalue;*/
                                        }
		|	r_m64	{	
						boolean isReg = $r_m64.ctx.isReg;
						mal.encoder.leftInit(
							isReg,
							isReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize,
							isReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().expSize,
							isReg?$r_m64.ctx.r64().col:0,
							mal.modrm_row,
							isReg?"no":$r_m64.ctx.m64().dispvalue,
							isReg?mal.rex_rb:false
						);

						/*mal.leftIsReg = $r_m64.ctx.isReg;  mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
                                                mal.opcol =  mal.leftIsReg?$r_m64.ctx.r64().col:0;  mal.oprow =  mal.modrm_row;
                                                mal.rex_b =  mal.leftIsReg? mal.rex_rb: false;  //mal.rex_b = ! mal.leftIsReg? mal.rex_rb: mal.rex_r;
						mal.leftExpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().expSize;
                                                mal.disp = mal.leftIsReg?"no":$r_m64.ctx.m64().dispvalue;*/
                                        }
		;

adc_right:  r_m8   { 
						boolean isReg = $r_m8.ctx.isReg;
						mal.encoder.rightInit(
							isReg,
							false,
							isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize,
							isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().expSize,
							isReg?$r_m8.ctx.r8().col:0,
							mal.modrm_row,
							isReg?"no":$r_m8.ctx.m8().dispvalue,
							isReg?mal.rex_rb:false
						);
                                                if(mal.encoder.check_condition(mal.table16,mal.is64))
						{
                                                    mal.encoder.checkPrefix();	
                                                    mal.encoder.output_left_right(false,isReg?"0x10":"0x12");
                                                }else
                                                {
                                                    mal.printError("mismatch in operand sizes" );
                                                }
                }
                   
		|	r_m16 { 
						boolean isReg = $r_m16.ctx.isReg;
						mal.encoder.rightInit(
							isReg,
							false,
							isReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize,
							isReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().expSize,
							isReg?$r_m16.ctx.r16().col:0,
							mal.modrm_row,
							isReg?"no":$r_m16.ctx.m16().dispvalue,
							isReg?mal.rex_rb:false
						);
                          if(mal.encoder.check_condition(mal.table16,mal.is64))
                          {
							mal.encoder.checkPrefix();
							mal.encoder.output_left_right(false,isReg?"0x11":"0x13");
                          }else{
							mal.printError("mismatch in operand sizes" );
                          }

				/*mal.rightIsReg = $r_m16.ctx.isReg;  mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
                                mal.opcol =  mal.rightIsReg?$r_m16.ctx.r16().col:mal.opcol;
                                mal.rex_r =  mal.rightIsReg? mal.rex_rb: mal.rex_b;
                                mal.rex_b = !mal.rightIsReg?mal.rex_rb:mal.rex_b;
                                mal.disp = mal.rightIsReg?mal.disp:$r_m16.ctx.m16().dispvalue;  
                               if( mal.checkcondition(mal.leftIsReg,mal.leftOpSize,mal.rightIsReg,mal.rightOpSize)){
							mal.rightExpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().expSize;
							mal.checkPrefix(mal.leftIsReg, mal.leftOpSize, 
								mal.rightIsReg, mal.rightOpSize, mal.leftExpSize, mal.rightExpSize, false); 
                                  mal.outputLR( mal.opcol, false, mal.disp, mal.rightIsReg?"0x11":"0x13");}
                                else{mal.printError("mismatch in operand sizes" );}*/
                }
		|	r_m32 { 
						boolean isReg = $r_m32.ctx.isReg;
						mal.encoder.rightInit(
							isReg,
							false,
							isReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize,
							isReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().expSize,
							isReg?$r_m32.ctx.r32().col:0,
							mal.modrm_row,
							isReg?"no":$r_m32.ctx.m32().dispvalue,
							isReg?mal.rex_rb:false
						);
                                                if(mal.encoder.check_condition(mal.table16,mal.is64))
                                                {
                                                    mal.encoder.checkPrefix();
                                                    mal.encoder.output_left_right(false,isReg?"0x11":"0x13");
                                                }else
                                                {
                                                    mal.printError("mismatch in operand sizes" );
                                                }
				/*mal.rightIsReg = $r_m32.ctx.isReg;  mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
                                mal.opcol =  mal.rightIsReg?$r_m32.ctx.r32().col:mal.opcol;
                                mal.rex_r =  mal.rightIsReg? mal.rex_rb:mal.rex_b; 
                                mal.rex_b = !mal.rightIsReg?mal.rex_rb:mal.rex_b;
                                mal.disp = mal.rightIsReg?mal.disp:$r_m32.ctx.m32().dispvalue;
                               if (mal.checkcondition(mal.leftIsReg,mal.leftOpSize,mal.rightIsReg,mal.rightOpSize)){
							mal.rightExpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().expSize;
							mal.checkPrefix(mal.leftIsReg, mal.leftOpSize, 
								mal.rightIsReg, mal.rightOpSize, mal.leftExpSize, mal.rightExpSize, false); 
							mal.outputLR( mal.opcol, false,mal.disp,  mal.rightIsReg?"0x11":"0x13");}
                                else{mal.printError("mismatch in operand sizes" );}*/
                }
		|	r_m64  { 
						boolean isReg = $r_m64.ctx.isReg;
						mal.encoder.rightInit(
							isReg,
							false,
							isReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize,
							isReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().expSize,
							isReg?$r_m64.ctx.r64().col:0,
							mal.modrm_row,
							isReg?"no":$r_m64.ctx.m64().dispvalue,
							isReg?mal.rex_rb:false
						);
						if(mal.encoder.check_condition(mal.table16,mal.is64))
                                                {
                                                    mal.encoder.checkPrefix();
                                                    mal.encoder.output_left_right(true, isReg?"0x11":"0x13");
                                                }else
                                                {
                                                    mal.printError("mismatch in operand sizes" );
                                                }

						/*mal.rightIsReg = $r_m64.ctx.isReg;  mal.rightOpSize =  mal.rightIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
                                 mal.opcol =  mal.rightIsReg?$r_m64.ctx.r64().col:mal.opcol;
                                 mal.rex_r =  mal.rightIsReg? mal.rex_rb: mal.rex_b;
                                 mal.rex_b = !mal.rightIsReg?mal.rex_rb:mal.rex_b;
                                 mal.disp = mal.rightIsReg?mal.disp:$r_m64.ctx.m64().dispvalue;
                                if(mal.checkcondition(mal.leftIsReg,mal.leftOpSize,mal.rightIsReg,mal.rightOpSize,mal.oprow,mal.modrm_row) ){
							mal.rightExpSize =  mal.rightIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().expSize;
							mal.checkPrefix(mal.leftIsReg, mal.leftOpSize, 
								mal.rightIsReg, mal.rightOpSize, mal.leftExpSize, mal.rightExpSize, false); 
							//mal.outputREX(true);
							mal.outputLR( mal.opcol, true,mal.disp,  mal.rightIsReg?"0x11":"0x13");}
                                 else{mal.printError("mismatch in operand sizes" );}*/
                }
		|	imm		{   mal.encoder.rightInit(
                                                                                  false,
                                                                                  true,
                                                                                  32,
                                                                                  32,
                                                                                  2,
                                                                                  mal.modrm_row,
                                                                                  "no",
                                                                                  false
                                                                                  );
                                                                                  if(mal.encoder.check_condition(mal.table16,mal.is64)){
                                                                                    mal.encoder.checkPrefix();
                                                                                    String a[] = {"0x80","0x81","0x83"}; 
                                                                                    mal.encoder.output_imm(a,CalculatorLibrary.cal($imm.text),32);
                                                                                  }else{
                                                                                        System.out.println("nothing");
                                                                                  }
					}	
		
		;

adcx			:	ADCX r32	COMMA r_m32 { mal.oprow =  mal.modrm_row;    mal.output("0x66");
                                                              mal.outputR($r32.col,"0x0f","0x38","0xf6" );}
			|	ADCX r64{ mal.rex_r =  mal.rex_rb;}	COMMA r_m64{ mal.rex_b =  mal.rex_rb;  mal.oprow =  mal.modrm_row; 
                                                                                                        mal.output("0x66");  mal.outputREX(true); 
                                                                                                        mal.outputR($r64.col,"0x0f","0x38","0xf6" ); }
			;


add			:	ADD AL		COMMA imm			{ mal.checkPrefix(true,16,false, mal.bits); mal.output("0x04", CalculatorLibrary.cal($imm.text));}	# ADD_AL__imm8
			|	ADD AX		COMMA imm			{ mal.checkPrefix(true, 16,false, mal.bits);  mal.output("0x05", CalculatorLibrary.cal($imm.text));}	# ADD_AX__imm16
			|	ADD EAX		COMMA imm			{ mal.checkPrefix(true,32,false, mal.bits);    mal.output("0x05", CalculatorLibrary.cal($imm.text));}	# ADD_EAX__imm32
			|	ADD RAX		COMMA imm			{ mal.checkPrefix(true,64, false,  mal.bits);    mal.outputREX(true);  mal.output("0x05",CalculatorLibrary.cal($imm.text)); }# ADD_RAX__imm32
			|	ADD r_m8	COMMA imm			{boolean isReg = $r_m8.ctx.isReg;
                                                                                mal.encoder.leftInit(
                                                                                    isReg,
                                                                                    isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize,
                                                                                    isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().expSize,
                                                                                    0,
                                                                                    mal.modrm_row,
                                                                                    isReg? "no":$r_m8.ctx.m8().dispvalue,
                                                                                    isReg?mal.rex_rb:false
                                                                                  );
                                                                                mal.encoder.rightInit(
                                                                                    false,
                                                                                    true,
                                                                                    isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize,
                                                                                    isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().expSize,
                                                                                    isReg? $r_m8.ctx.r8().col:0,
                                                                                    mal.modrm_row,
                                                                                    isReg? "no":$r_m8.ctx.m8().dispvalue,
                                                                                    isReg?mal.rex_rb:false
                                                                              );
                                                                              if(mal.encoder.check_condition(mal.table16,mal.is64)){
                                                                                mal.encoder.checkPrefix();
                                                                                String a[] = {"0x80","0x81","0x83"};
                                                                                mal.encoder.output_imm( a,CalculatorLibrary.cal($imm.text),32);
                                                                              }
                                                                    }	# ADD_R_M8__imm8//not yet done 
			|	ADD r_m16	COMMA imm			{ mal.checkPrefix($r_m16.ctx.isReg, $r_m16.ctx.isReg?$r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize,false, mal.bits);    mal.outputEa(false,0,"no", CalculatorLibrary.cal($imm.text),"0x81");}	# ADD_R_M16__imm16
			|	ADD r_m32	COMMA imm			{ mal.checkPrefix($r_m32.ctx.isReg, $r_m32.ctx.isReg?$r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize,false,  mal.bits);    mal.outputEa(false,0,"no", CalculatorLibrary.cal($imm.text),"0x81");}	# ADD_R_M32__imm32
			|	ADD r_m64 { mal.rex_b =  mal.rex_rb;}	COMMA imm { mal.checkPrefix($r_m64.ctx.isReg, $r_m64.ctx.isReg?$r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize,false,  mal.bits);    mal.outputREX(true);  mal.outputEa(true,0,"no",CalculatorLibrary.cal($imm.text),"0x81");}			# ADD_R_M64__imm32
			|	ADD r_m16	COMMA imm			{ mal.checkPrefix($r_m16.ctx.isReg, $r_m16.ctx.isReg?$r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize,false, mal.bits); mal.outputEa(false,0,"no", CalculatorLibrary.cal($imm.text),"0x83");}	# ADD_R_M16__imm8
			|	ADD r_m32	COMMA imm			{ mal.checkPrefix($r_m32.ctx.isReg, $r_m32.ctx.isReg?$r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize,false,  mal.bits); mal.outputEa(false, 0,"no",CalculatorLibrary.cal($imm.text),"0x83");}	# ADD_R_M32__imm8
			|	ADD r_m64 { mal.rex_b =  mal.rex_rb;}	COMMA imm { mal.checkPrefix($r_m64.ctx.isReg, $r_m64.ctx.isReg?$r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize,false,  mal.bits);  mal.outputREX(true);  mal.outputEa(true,0,"no", CalculatorLibrary.cal($imm.text),"0x83");}			# ADD_R_M64__imm8
			/*|	ADD r_m8	{ mal.oprow =  mal.modrm_row;}COMMA r8 { mal.outputR("0x00",$r8.ctx.col);}			# ADD_R_M8__R8
			|	ADD r_m16	COMMA r16			# ADD_R_M16__R16
			|	ADD r_m32	COMMA r32			# ADD_R_M32__R32
			|	ADD r_m64	COMMA r64			# ADD_R_M64__R64
			|	ADD r8		COMMA r_m8			# ADD_R8__R_M8
			|	ADD r16		COMMA r_m16			# ADD_R16__R_M16
			|	ADD r32		COMMA r_m32			# ADD_R32__R_M32
			|	ADD r64		COMMA r_m64			# ADD_R64__R_M64*/
			|   { mal.leftIsReg = true;  mal.rightIsReg = true;}ADD add_left COMMA add_right { mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);}# fuck1
			;

add_left        :	r_m8   {boolean isReg = $r_m8.ctx.isReg;
                                mal.encoder.leftInit(
                                isReg,
                                isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize,
                                isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().expSize,
                                isReg?$r_m8.ctx.r8().col:0,
                                mal.modrm_row,
                                isReg?"no":$r_m8.ctx.m8().dispvalue,
                                isReg?mal.rex_rb:false
                                );}
                    |	r_m16   {boolean isReg = $r_m16.ctx.isReg;
                                mal.encoder.leftInit(
                                isReg,
                                isReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize,
                                isReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().expSize,
                                isReg?$r_m16.ctx.r16().col:0,
                                mal.modrm_row,
                                isReg?"no":$r_m16.ctx.m16().dispvalue,
                                isReg?mal.rex_rb:false
                                );}
		|	r_m32   {boolean isReg = $r_m32.ctx.isReg;
                                mal.encoder.leftInit(
                                isReg,
                                isReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize,
                                isReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().expSize,
                                isReg?$r_m32.ctx.r32().col:0,
                                mal.modrm_row,
                                isReg?"no":$r_m32.ctx.m32().dispvalue,
                                isReg?mal.rex_rb:false
                                );}
		|	r_m64   {boolean isReg = $r_m64.ctx.isReg;
                                mal.encoder.leftInit(
                                isReg,
                                isReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize,
                                isReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().expSize,
                                isReg?$r_m64.ctx.r64().col:0,
                                mal.modrm_row,
                                isReg?"no":$r_m64.ctx.m64().dispvalue,
                                isReg?mal.rex_rb:false
                                );}
		;

add_right       :       r_m8   {boolean isReg = $r_m8.ctx.isReg;
				mal.encoder.rightInit(
                                    isReg,
                                    false,
                                    isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize,
                                    isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().expSize,
                                    isReg?$r_m8.ctx.r8().col:0,
                                    mal.modrm_row,
                                    isReg?"no":$r_m8.ctx.m8().dispvalue,
                                    isReg?mal.rex_rb:false
                                    );
                                if(mal.encoder.check_condition(mal.table16,mal.is64))
                                {
                                    mal.encoder.checkPrefix();	
                                    mal.encoder.output_left_right(false,isReg?"0x00":"0x02");
                                }else
                                {
                                    mal.printError("mismatch in operand sizes" );
                                }}
		|	r_m16 {boolean isReg = $r_m16.ctx.isReg;
				mal.encoder.rightInit(
                                    isReg,
                                    false,
                                    isReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize,
                                    isReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().expSize,
                                    isReg?$r_m16.ctx.r16().col:0,
                                    mal.modrm_row,
                                    isReg?"no":$r_m16.ctx.m16().dispvalue,
                                    isReg?mal.rex_rb:false
                                    );
                                if(mal.encoder.check_condition(mal.table16,mal.is64))
                                {
                                    mal.encoder.checkPrefix();	
                                    mal.encoder.output_left_right(false,isReg?"0x01":"0x03");
                                }else
                                {
                                    mal.printError("mismatch in operand sizes" );
                                }}
		|	r_m32 {boolean isReg = $r_m32.ctx.isReg;
				mal.encoder.rightInit(
                                    isReg,
                                    false,
                                    isReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize,
                                    isReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().expSize,
                                    isReg?$r_m32.ctx.r32().col:0,
                                    mal.modrm_row,
                                    isReg?"no":$r_m32.ctx.m32().dispvalue,
                                    isReg?mal.rex_rb:false
                                    );
                                if(mal.encoder.check_condition(mal.table16,mal.is64))
                                {
                                    mal.encoder.checkPrefix();	
                                    mal.encoder.output_left_right(false,isReg?"0x01":"0x03");
                                }else
                                {
                                    mal.printError("mismatch in operand sizes" );
                                }}
		|	r_m64  {boolean isReg = $r_m64.ctx.isReg;
				mal.encoder.rightInit(
                                    isReg,
                                    false,
                                    isReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize,
                                    isReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().expSize,
                                    isReg?$r_m64.ctx.r64().col:0,
                                    mal.modrm_row,
                                    isReg?"no":$r_m64.ctx.m64().dispvalue,
                                    isReg?mal.rex_rb:false
                                    );
                                if(mal.encoder.check_condition(mal.table16,mal.is64))
                                {
                                    mal.encoder.checkPrefix();	
                                    mal.encoder.output_left_right(false,isReg?"0x01":"0x03");
                                }else
                                {
                                    mal.printError("mismatch in operand sizes" );
                                }}
                ;

/*addpd           :       ADDPD xmm1 COMMA xmm2_m128;
vaddpd          :       VADDPD xmm1 COMMA xmm2 COMMA xmm3_m128
                |       VADDPD ymm1 COMMA ymm2 COMMA ymm3_m256
                |       VADDPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                |       VADDPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                |       VADDPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};

addps           :       ADDPS xmm1 COMMA xmm2_m128;
vaddps          :       VADDPS xmm1 COMMA xmm2 COMMA xmm3_m128
                |       VADDPS ymm1 COMMA ymm2 COMMA ymm3_m256
                |       VADDPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                |       VADDPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                |       VADDPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst {er};

addsd           :       ADDSD xmm1 COMMA xmm2_m64;
vaddsd          :       VADDSD xmm1 COMMA xmm2 COMMA xmm3_m64
                |       VADDSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};

addss           :       ADDSS xmm1 COMMA xmm2_m32;
vaddss          :       VADDSS xmm1 COMMA xmm2 COMMA xmm3_m32
                |       VADDSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};

addsubpd        :       ADDSUBPD xmm1 COMMA xmm2_m128;
vaddsubpd       :       VADDSUBPD xmm1 COMMA xmm2 COMMA xmm3_m128
                |       VADDSUBPD ymm1 COMMA ymm2 COMMA ymm3_m256;

addsubps        :       ADDSUBPS xmm1 COMMA xmm2_m128;
vaddsubps       :       VADDSUBPS xmm1 COMMA xmm2 COMMA xmm3_m128
                |       VADDSUBPS ymm1 COMMA ymm2 COMMA ymm3_m256;*/

adox            :       ADOX r32 COMMA r_m32 { mal.oprow =  mal.modrm_row;  mal.outputR($r32.col, "0xf3","0x0f", "0x38","0xf6");} 
                |       ADOX r64{ mal.rex_r =  mal.rex_rb;} COMMA r_m64 { mal.rex_b =  mal.rex_rb;  mal.output("0xf3"); 
                         mal.oprow =  mal.modrm_row;  mal.outputR($r64.col, "0x0f","0x38","0xf6");}
                ;

/*aesdec          :       AESDEC xmm1 COMMA xmm2_m128;
vaesdec         :       VAESDEC xmm1 COMMA xmm2 COMMA xmm3_m128;

aesdeclast      :       AESDECLAST xmm1 COMMA xmm2_m128;
vaesdeclast     :       VAESDECLAST xmm1 COMMA xmm2 COMMA xmm3_m128;

aesenc          :       AESENC xmm1 COMMA xmm2_m128;
vaesenc         :       VAESENC xmm1 COMMA xmm2 COMMA xmm3_m128;

aesenclast      :       AESENCLAST xmm1 COMMA xmm2_m128;
vaesenclast     :       VAESENCLAST xmm1 COMMA xmm2 COMMA xmm3_m128;

aesimc          :       AESIMC xmm1 COMMA xmm2_m128;
vaesimc         :       VAESIMC xmm1 COMMA xmm2_m128;

aeskeygenassist :       AESKEYGENASSIST xmm1 COMMA xmm2_m128 COMMA imm;
vaeskeygenassist:       VAESKEYGENASSIST xmm1 COMMA xmm2_m128 COMMA imm;
*/

and			:       AND AL		COMMA imm		{ mal.checkPrefix(true,16,false, mal.bits);     mal.output("0x24", CalculatorLibrary.cal($imm.text));}	# AND_AL__imm8
			|       AND AX		COMMA imm		{ mal.checkPrefix(true,16,false, mal.bits);    mal.output("0x25", CalculatorLibrary.cal($imm.text));}	# AND_AX__imm16
			|       AND EAX		COMMA imm		{ mal.checkPrefix(true,32,false, mal.bits);  mal.output("0x25", CalculatorLibrary.cal($imm.text));}	# AND_EAX__imm32
			|       AND RAX	{ mal.rex_b =  mal.rex_rb;}	COMMA imm{ mal.checkPrefix(true,64,false, mal.bits); mal.outputREX(true);  mal.output("0x25",CalculatorLibrary.cal($imm.text));}		# AND_RAX__imm32
			/*|       AND r_m8	COMMA imm8		{ mal.oprow =  mal.modrm_row;  mal.checkPrefix($r_m8.ctx.isReg, $r_m8.ctx.isReg?$r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize,false, mal.bits); mal.outputEa(4, $imm8.text,"0x80");}	# AND_R_M8__imm8                                                                                 
			|       AND r_m16	COMMA imm16		{ mal.oprow =  mal.modrm_row; mal.checkPrefix($r_m16.ctx.isReg, $r_m16.ctx.isReg?$r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize,false, mal.bits); mal.outputEa(4, $imm16.text,"0x81");}	# AND_R_M16__imm16
			|       AND r_m32	COMMA imm32		{ mal.oprow =  mal.modrm_row; mal.checkPrefix($r_m32.ctx.isReg, $r_m32.ctx.isReg?$r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize,false, mal.bits); mal.outputEa(4, $imm32.text,"0x81");}	# AND_R_M32__imm32
			|       AND r_m64{ mal.rex_b= mal.rex_rb;}	COMMA imm32{ mal.oprow =  mal.modrm_row; mal.checkPrefix($r_m64.ctx.isReg, $r_m64.ctx.isReg?$r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize,false, mal.bits);
                                                             mal.outputREX(true); mal.outputEa(4, $imm32.text,"0x81");}		# AND_R_M64__imm32
			|       AND r_m16	COMMA imm8		{ mal.oprow =  mal.modrm_row; mal.checkPrefix($r_m16.ctx.isReg, $r_m16.ctx.isReg?$r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize,false, mal.bits);  mal.outputEa(4, $imm8.text,"0x83");}	# AND_R_M16__imm8
			|       AND r_m32	COMMA imm8		{ mal.oprow =  mal.modrm_row; mal.checkPrefix($r_m32.ctx.isReg, $r_m32.ctx.isReg?$r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize,false, mal.bits);  mal.outputEa(4, $imm8.text,"0x83");}	# AND_R_M32__imm8
			|       AND r_m64{ mal.rex_b= mal.rex_rb;}	COMMA imm8{ mal.oprow =  mal.modrm_row; mal.checkPrefix($r_m64.ctx.isReg, $r_m64.ctx.isReg?$r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize,false, mal.bits);
                                        	 mal.outputREX(true); mal.outputEa(4, $imm8.text,"0x83");}	# AND_R_M64__imm
			*//*|       AND r_m8{ mal.oprow =  mal.modrm_row;}		COMMA r8 { mal.checkPrefix($r_m8.ctx.isReg,$r_m8.ctx.isReg?$r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize, true,$r8.operandSize);
                                                     mal.outputR($r8.col, "0x20");}				# AND_R_M8__R_8
			|       AND r_m16{ mal.oprow =  mal.modrm_row;}	COMMA r16	{ mal.checkPrefix($r_m16.ctx.isReg,$r_m16.ctx.isReg?$r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize, true,$r16.operandSize); mal.outputR($r16.col,"0x21");}				# AND_R_M16__R_16
			|       AND r_m32{ mal.oprow =  mal.modrm_row;}	COMMA r32	{ mal.checkPrefix($r_m32.ctx.isReg,$r_m32.ctx.isReg?$r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize, true,$r32.operandSize); mal.outputR($r32.col,"0x21");}				# AND_R_M32__R_32
			|       AND r_m64{ mal.oprow =  mal.modrm_row; mal.rex_b =  mal.rex_rb;}	COMMA r64{ mal.rex_r= mal.rex_rb;  mal.checkPrefix($r_m64.ctx.isReg,$r_m64.ctx.isReg?$r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize, true,$r64.operandSize);
                                             mal.outputREX(true); mal.outputR($r64.col,"0x21");}		# AND_R_M64_R_64
			|       AND r8		COMMA r_m8{ mal.oprow= mal.modrm_row;  mal.checkPrefix(true, $r8.operandSize, $r_m8.ctx.isReg, $r_m8.ctx.isReg?$r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize);   mal.outputR($r8.col,"0x22");}				# AND_R8__R_M8                            
			|       AND r16		COMMA r_m16		{ mal.oprow= mal.modrm_row;  mal.checkPrefix(true, $r16.operandSize, $r_m16.ctx.isReg, $r_m16.ctx.isReg?$r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize); mal.outputR($r16.col, "0x23");}				# AND_R16__R_M16
			|       AND r32		COMMA r_m32		{ mal.oprow= mal.modrm_row;  mal.checkPrefix(true, $r32.operandSize, $r_m32.ctx.isReg, $r_m32.ctx.isReg?$r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize); mal.outputR($r32.col, "0x23");}				# AND_R32__R_M32
			|       AND r64	{ mal.rex_r = mal.rex_rb;}	COMMA r_m64             { mal.oprow= mal.modrm_row; mal.rex_b =  mal.rex_rb;  mal.checkPrefix(true, $r64.operandSize, $r_m64.ctx.isReg, $r_m64.ctx.isReg?$r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize);
                                                                         mal.outputREX(true); mal.outputR($r64.col, "0x23");}# AND_R64__R_M64*/
			|       { mal.leftIsReg = false;  mal.rightIsReg = false;} AND and_left COMMA and_right #LR_3
                        ;

and_left:	r_m8   {		mal.leftIsReg = $r_m8.ctx.isReg;  mal.leftOpSize =  mal.leftIsReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize;
						mal.opcol =  mal.leftIsReg?$r_m8.ctx.r8().col:0;  mal.oprow =  mal.modrm_row;
						mal.leftExpSize =  mal.leftIsReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().expSize;
					}
		|	r_m16 {		mal.leftIsReg = $r_m16.ctx.isReg;  mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
                           mal.opcol =  mal.leftIsReg?$r_m16.ctx.r16().col:0;  mal.oprow = mal.modrm_row;
						mal.leftExpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().expSize;
					}
		|	r_m32 { mal.leftIsReg = $r_m32.ctx.isReg;  mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
                           mal.opcol =  mal.leftIsReg?$r_m32.ctx.r32().col:0;  mal.oprow =  mal.modrm_row;
						mal.leftExpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().expSize;
					}
		|	r_m64 { mal.leftIsReg = $r_m64.ctx.isReg;  mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
                           mal.opcol =  mal.leftIsReg?$r_m64.ctx.r64().col:0;  mal.oprow =  mal.modrm_row;
                           mal.rex_r =  mal.leftIsReg? mal.rex_rb: mal.rex_b;  mal.rex_b = ! mal.leftIsReg? mal.rex_rb: mal.rex_r;
						mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().expSize;
					}
		;

and_right:  r_m8   { mal.rightIsReg = $r_m8.ctx.isReg;  mal.rightOpSize =  mal.rightIsReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize;
                     mal.opcol =  mal.rightIsReg?$r_m8.ctx.r8().col:mal.opcol;
				   mal.rightExpSize = mal.rightIsReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().expSize;
                    if( mal.leftOpSize > mal.rightOpSize) {
                                     mal.printError("mismatch in operand sizes" );
                    } else{
                             mal.checkPrefix( mal.leftIsReg,  mal.leftExpSize,  mal.rightIsReg,  mal.rightExpSize);
                             mal.outputLR( mal.opcol, mal.rightIsReg, mal.rightIsReg? "0x20":"0x22");}}
		|	r_m16 { mal.rightIsReg = $r_m16.ctx.isReg;  mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
                         mal.opcol =  mal.rightIsReg?$r_m16.ctx.r16().col:mal.opcol;
					mal.rightExpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().expSize;
                        if( mal.leftOpSize > mal.rightOpSize) {
                           mal.printError("mismatch in operand sizes" );
			} else{
                             mal.checkPrefix( mal.leftIsReg,  mal.leftExpSize,  mal.rightIsReg,  mal.rightExpSize);
                             mal.outputLR( mal.opcol, mal.rightIsReg,  mal.rightIsReg?"0x21":"0x23");}}
		|	r_m32 { mal.rightIsReg = $r_m32.ctx.isReg;  mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
                                mal.opcol =  mal.rightIsReg?$r_m32.ctx.r32().col:mal.opcol;
							mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().expSize;	
                               if( mal.leftOpSize > mal.rightOpSize) {
                                     mal.printError("mismatch in operand sizes" );
				} else{
                                     mal.checkPrefix( mal.leftIsReg,  mal.leftExpSize,  mal.rightIsReg,  mal.rightExpSize);
                                     mal.outputLR( mal.opcol, mal.rightIsReg,  mal.rightIsReg?"0x21":"0x23");}}
		|	r_m64  { mal.rightIsReg = $r_m64.ctx.isReg;  mal.rightOpSize =  mal.rightIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
                                mal.opcol =  mal.rightIsReg?$r_m64.ctx.r64().col:mal.opcol; 
                                mal.rex_r =  mal.rightIsReg? mal.rex_rb: mal.rex_b;  mal.rex_b = ! mal.rightIsReg? mal.rex_rb:false;
                                mal.rightOpSize =  mal.rightIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().expSize;
							if( mal.leftOpSize > mal.rightOpSize) {
                                     mal.printError("mismatch in operand sizes" );
				} else{
                                 mal.checkPrefix( mal.leftIsReg,  mal.leftExpSize,  mal.rightIsReg,  mal.rightExpSize);
                                 mal.outputREX(true); mal.outputLR( mal.opcol, mal.rightIsReg,  mal.rightIsReg?"0x21":"0x23");}}
        |	imm
									{
										if( mal.bits != 64 &&  mal.leftOpSize == 64) {
											mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
										} else {
											 mal.rightIsReg = false; 
											 mal.rightOpSize = 8;
											 mal.checkPrefix( mal.leftIsReg,  mal.leftExpSize,  mal.rightIsReg,  16, true);
											
											if(mal.leftOpSize == 8) {
												 mal.outputEa(false,4,"no", CalculatorLibrary.cal($imm.text), "0X80");
											}else {
												if(mal.leftOpSize == 64) {
													 mal.outputREX(true); 
												}
												 mal.outputEa(false,4,"no", CalculatorLibrary.cal($imm.text), "0X83");	
											}
										}
									}
						|       imm
									{
										if( mal.bits != 64 &&  mal.leftOpSize == 64) {
											mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
										} else if ( mal.leftOpSize != 16) {
											System.out.println(mal.leftOpSize);
											System.out.println("error: invalid combination of opcode and operands");
										}else {
											 mal.rightIsReg = false; 
											 mal.rightOpSize = 16;
											 mal.checkPrefix( mal.leftIsReg,  mal.leftExpSize,  mal.rightIsReg, 16, true);
											 mal.outputEa(false,4,"no", CalculatorLibrary.cal($imm.text), "0X81");
										}
									}
						|       imm
									{
										if( mal.bits != 64 &&  mal.leftOpSize == 64) {
											mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
										} else if ( mal.leftOpSize != 32 ||  mal.leftOpSize != 64) {
											System.out.println("error: invalid combination of opcode and operands");
										}else {
											mal.rightIsReg = false; 
											mal.rightOpSize = 32;
											mal.checkPrefix( mal.leftIsReg,  mal.leftExpSize,  mal.rightIsReg,  32, true);
											if(mal.leftOpSize == 64) {
													 mal.outputREX(true); 
											} 
											mal.outputEa(false,4,"no", CalculatorLibrary.cal($imm.text), "0X81");
										}
									};        

/*andn            :       ANDN r32a COMMA r32b COMMA r_m32
                |       ANDN r64a COMMA r64b COMMA r_m64;

andpd           :       ANDPD xmm1 COMMA xmm2_m128;
vandpd          :       VANDPD xmm1 COMMA xmm2 COMMA xmm3_m128
                |       VANDPD ymm1 COMMA ymm2 COMMA ymm3_m256
                |       VANDPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                |       VANDPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                |       VANDPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

andps           :       ANDPS xmm1 COMMA xmm2_m128;
vandps          :       VANDPS xmm1 COMMA xmm2 COMMA xmm3_m128
                |       VANDPS ymm1 COMMA ymm2 COMMA ymm3_m256
                |       VANDPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                |       VANDPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                |       VANDPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;

andnpd          :       ANDNPD xmm1 COMMA xmm2_m128;
vandnpd         :       VANDNPD xmm1 COMMA xmm2 COMMA xmm3_m128
                |       VANDNPD ymm1 COMMA ymm2 COMMA ymm3_m256
                |       VANDNPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                |       VANDNPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                |       VANDNPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

andnps          :       ANDNPS xmm1 COMMA xmm2_m128;
vandnps         :       VANDNPS xmm1 COMMA xmm2 COMMA xmm3_m128
                |       VANDNPS ymm1 COMMA ymm2 COMMA ymm3_m256
                |       VANDNPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                |       VANDNPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                |       VANDNPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
*/
arpl		:       ARPL r_m16{ mal.oprow =  mal.modrm_row;} COMMA r16{ mal.checkPrefix($r_m16.ctx.isReg,$r_m16.ctx.isReg?$r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize, true,$r16.operandSize);
                                    mal.outputR($r16.col, "0x63");}    #APRL_R_M16__R16 ;
/*
bextr           :       BEXTR r32a COMMA r_m32 COMMA r32b
                |       BEXTR r64a COMMA r_m64 COMMA r64b;

blendpd         :       BLENDPD xmm1 COMMA xmm2_m128 COMMA imm;
vblendpd        :       VBLENDPD xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                |       VBLENDPD ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm;

blendps         :       BLENDPS xmm1 COMMA xmm2_m128 COMMA imm;
vblendps        :       VBLENDPS xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                |       VBLENDPS ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm;

blendvpd        :       BLENDVPD xmm1 COMMA xmm2_m128 COMMA <XMM0>;
vblendvpd       :       VBLENDVPD xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA xmm4
                |       VBLENDVPD ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA ymm4;

blendvps        :       BLENDVPS xmm1 COMMA xmm2_m128 COMMA <XMM0>;
vblendvps       :       VBLENDVPS xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA xmm4
                |       VBLENDVPS ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA ymm4;
*/
blsi            :       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
						BLSI r32 
							{
								 mal.leftIsReg = true; 
								 mal.leftOpSize = 32;
							}
						COMMA r_m32
							{
								 mal.rightIsReg = $r_m32.ctx.isReg; 
								 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
								 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
							} 
                |       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
						BLSI r64 
							{
								 mal.leftIsReg = true; 
								 mal.leftOpSize = 64;
							}
						COMMA r_m64
							{
								 mal.rightIsReg = $r_m64.ctx.isReg; 
								 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
								 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
							};
blsmsk          :		{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
						BLSMSK r32
							{
								 mal.leftIsReg = true; 
								 mal.leftOpSize = 32;
							}
						COMMA r_m32
							{
								 mal.rightIsReg = $r_m32.ctx.isReg; 
								 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
								 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
							} 
				|       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
						BLSMSK r64 COMMA r_m64;

blsr            :       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
						BLSR r32
							{
								 mal.leftIsReg = true; 
								 mal.leftOpSize = 32;
							}
						COMMA r_m32
							{
								 mal.rightIsReg = $r_m32.ctx.isReg; 
								 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
								 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
							} 
                |       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
						BLSR r64 
							{
								 mal.leftIsReg = true; 
								 mal.leftOpSize = 64;
							}
						COMMA r_m64
							{
								 mal.rightIsReg = $r_m64.ctx.isReg; 
								 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
								 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
							};
/*
bndcl           :       BNDCL bnd COMMA r_m32
                |       BNDCL bnd COMMA r_m64;

bndcu           :       BNDCU bnd COMMA r_m32
                |       BNDCU bnd COMMA r_m64;
bndcn           :       BNDCN bnd COMMA r_m32
                |       BNDCN bnd COMMA r_m64;

bndldx          :       BNDLDX bnd COMMA mib;

bndmk           :       BNDMK bnd COMMA m32
                |       BNDMK bnd COMMA m64;

bndmov          :       BNDMOV bnd1 COMMA bnd2_m64
                |       BNDMOV bnd1 COMMA bnd2_m128
                |       BNDMOV bnd1_m64 COMMA bnd2
                |       BNDMOV bnd1_m128 COMMA bnd2;

bndstx          :       BNDSTX mib COMMA bnd;

bound           :       BOUND r16 COMMA m16AND16
                |       BOUND r32 COMMA m32AND32;
*/
bsf					:			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BSF r16	
									{
										 mal.leftIsReg = true; 
										 mal.leftOpSize = 16; 
										 mal.opcol = $r16.ctx.col;
									}	
								COMMA	r_m16
									{
										 mal.rightIsReg = $r_m16.ctx.isReg; 
										 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xbc");
									}		# BSF_R16__R_M16

						|		{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BSF r32	
									{	
										 mal.leftIsReg = true; 
										 mal.leftOpSize = 32;
										 mal.opcol = $r32.ctx.col;
									}	
								COMMA	r_m32 
									{	 mal.rightIsReg = $r_m32.ctx.isReg; 
										 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row; 
										 mal.outputR( mal.opcol,"0x0f", "0xbc");
									}			# BSF_R32__R_M32

						|       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BSF r64	
									{
										 mal.leftIsReg = true; 
										 mal.leftOpSize = 64;
										 mal.opcol = $r64.ctx.col;
										 mal.rex_r =  mal.rex_rb;
									}	
								COMMA	r_m64 
									{
										 mal.rightIsReg = $r_m64.ctx.isReg; 
										 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.rex_b =  mal.rex_rb;
										 mal.outputREX(true); 
										 mal.oprow =  mal.modrm_row; 
										 mal.outputR( mal.opcol,"0x0f", "0xbc");
									}			# BSF_R64__R_M64;

/*bsr						:       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BSR r16	
									{
										 mal.leftIsReg = true; 
										 mal.leftOpSize = 16;
										 mal.opcol = $r16.ctx.col;
									}	
								COMMA r_m16 
									{	 mal.rightIsReg = $r_m16.ctx.isReg; 
										 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
										 mal.rightIsReg = $r_m16.ctx.isReg; 
										 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row; 
										 mal.outputR( mal.opcol,"0x0f", "0xbd");
									}			# BSR_R16__R_M16
						|       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BSR r32	
									{
										 mal.leftIsReg = true; 
										 mal.leftOpSize = 32;
										 mal.opcol = $r32.ctx.col;
									}	
								COMMA r_m32 
									{
										 mal.rightIsReg = $r_m32.ctx.isReg; 
										 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row; 
										 mal.outputR( mal.opcol,"0x0f", "0xbd");
									}			# BSR_R32__R_M32
						|       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BSR r64	
									{
										 mal.leftIsReg = true; 
										 mal.leftOpSize = 64;
										 mal.opcol = $r64.ctx.col;
										 mal.rex_r =  mal.rex_rb;
									}	
								COMMA r_m64 
									{
										 mal.rightIsReg = $r_m64.ctx.isReg; 
										 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.rex_b =  mal.rex_rb;
										 mal.outputREX(true); 
										 mal.oprow =  mal.modrm_row; 
										 mal.outputR( mal.opcol,"0x0f", "0xbd");
									}		    # BSR_R64__R_M64;
*/

bsr						:	{ mal.leftIsReg = false;  mal.rightIsReg = false;} BSR bsr_left COMMA bsr_right;

bsr_left					:	r16
								{
									 mal.leftIsReg = true; 
									 mal.leftOpSize = 16;
									 mal.opcol = $r16.ctx.col;
								}	
						|	r32
								{
									 mal.leftIsReg = true; 
									 mal.leftOpSize = 32;
									 mal.opcol = $r32.ctx.col;
								}
						|	r64
								{
									 mal.leftIsReg = true; 
									 mal.leftOpSize = 64;
									 mal.opcol = $r64.ctx.col;
									 mal.rex_r =  mal.rex_rb;
								};
bsr_right				:	r_m16
								{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0xbd");
								}
						|	r_m32
								{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0xbd");
								}
						|	r_m64
								{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0xbd");
								};




bswap					:       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BSWAP r32	{ mal.checkPrefix(true, $r32.ctx.operandSize,false, mal.bits); mal.outputAddRD(0xc8, "0x0f");}	      	        	# BSWAP_R32
						|			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BSWAP r64	{ mal.checkPrefix(true, $r64.ctx.operandSize,false, mal.bits); mal.rex_b =  mal.rex_rb;  mal.outputREX(true);  mal.outputAddRD(0xc8,"0x0f");}	      	        	# BSWAP_R64;

/*bt                      :			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BT r_m16 
									{	
										 mal.leftIsReg = $r_m16.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
										 mal.opcol = $r16.ctx.col;
									}
								COMMA r16
									{	
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 16; 
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xa3");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BT r_m32
									{
										 mal.leftIsReg = $r_m32.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
										 mal.opcol = $r32.ctx.col;
									}
								
								COMMA r32
									{
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xa3");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BT r_m64
									{
										 mal.leftIsReg = $r_m64.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
										 mal.rex_b =  mal.rex_rb;
									}
								COMMA r64
									{
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 64;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.rex_r =  mal.rex_rb;
										 mal.outputREX(true); 
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xa3");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BT r_m16 
									{	 mal.leftIsReg = $r_m16.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 16;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputEa(4, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BT r_m32 
									{
										 mal.leftIsReg = $r_m32.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 16;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputEa(4, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BT r_m64
									{
										 mal.leftIsReg = $r_m64.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
										 mal.rex_b =  mal.rex_rb;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 16;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputREX(true); 
										 mal.outputEa(7, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									};
*/
bt						:		{ mal.leftIsReg = false;  mal.rightIsReg = false;} BT bt_left COMMA bt_right;

bt_left					:	r_m16
							{	 mal.leftIsReg = $r_m16.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						|	r_m32
							{
								 mal.leftIsReg = $r_m32.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						|	r_m64
							{
								 mal.leftIsReg = $r_m64.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
								 mal.rex_b =  mal.rex_rb;
								 mal.oprow =  mal.modrm_row;
							}
						;
		
bt_right					:	r16
								{	
									
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 16; 
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.opcol = $r16.ctx.col;
										 mal.outputR( mal.opcol,"0x0f", "0xa3");
									}
								}
						|	r32
								{
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.opcol = $r32.ctx.col;
										 mal.outputR( mal.opcol,"0x0f", "0xa3");
									}
								}
						|	r64
								{
									 mal.rightIsReg = true; 
									 mal.rightOpSize = 64;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_r =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.opcol = $r64.ctx.col;
									 mal.outputR( mal.opcol,"0x0f", "0xa3");
								}
						|	imm 
								{
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 16;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize, true);
										
										if( mal.leftOpSize == 16 ||  mal.leftOpSize == 32) {
											 mal.outputEa(false,4,"no", CalculatorLibrary.cal($imm.text), "0X0f", "ba");
										}else {
											 mal.outputREX(true); 
											 mal.outputEa(false,4,"no", CalculatorLibrary.cal($imm.text), "0X0f", "ba");
										}
									}
								};

/*btc						:		{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTC r_m16 
									{	 mal.leftIsReg = $r_m16.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
										 mal.opcol = $r16.ctx.col;
									}
								COMMA r16
									{	 mal.rightIsReg = true; 
										 mal.rightOpSize = 16; 
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xbb");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTC r_m32 
									{
										 mal.leftIsReg = $r_m32.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
										 mal.opcol = $r32.ctx.col;
									}
								COMMA r32
									{
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xbb");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTC r_m64
									{
										 mal.leftIsReg = $r_m64.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
										 mal.rex_b =  mal.rex_rb;
									}
								COMMA r64
									{
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 64;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.rex_r =  mal.rex_rb;
										 mal.outputREX(true); 
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xbb");
									}
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTC r_m16
									{	 mal.leftIsReg = $r_m16.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 16;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputEa(7, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									}
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTC r_m32 
									{
										 mal.leftIsReg = $r_m32.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputEa(7, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTC r_m64
									{
										 mal.leftIsReg = $r_m64.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
										 mal.rex_b =  mal.rex_rb;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 64;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputREX(true); 
										 mal.outputEa(7, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									};*/

btc						:		{ mal.leftIsReg = false;  mal.rightIsReg = false;} BTC btc_left COMMA btc_right;

btc_left					:	r_m16
							{	 mal.leftIsReg = $r_m16.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						|	r_m32
							{
								 mal.leftIsReg = $r_m32.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						|	r_m64
							{
								 mal.leftIsReg = $r_m64.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						;
		
btc_right				:	r16
								{	
									
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 16; 
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.opcol = $r16.ctx.col;
										 mal.outputR( mal.opcol,"0x0f", "0xbb");
									}
								}
						|	r32
								{
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.opcol = $r32.ctx.col;
										 mal.outputR( mal.opcol,"0x0f", "0xbb");
									}
								}
						|	r64
								{
									 mal.rightIsReg = true; 
									 mal.rightOpSize = 64;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_r =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.opcol = $r64.ctx.col;
									 mal.outputR( mal.opcol,"0x0f", "0xbb");
								}
						|	imm 
								{
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 16;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize, true);
										
										if( mal.leftOpSize == 16 ||  mal.leftOpSize == 32) {
											 mal.outputEa(false,7,"no", CalculatorLibrary.cal($imm.text), "0X0f", "ba");
										}else {
											 mal.outputREX(true); 
											 mal.outputEa(false,7,"no", CalculatorLibrary.cal($imm.text), "0X0f", "ba");
										}
									}
								};

/*btr                     :			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTR r_m16 
									{
										 mal.leftIsReg = $r_m16.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
										 mal.opcol = $r16.ctx.col;
									}
								COMMA r16
									{	 mal.rightIsReg = $r_m16.ctx.isReg; 
										 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xb3");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTR r_m32 
									{
										 mal.leftIsReg = $r_m32.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
										 mal.opcol = $r32.ctx.col;
									}
								COMMA r32
									{
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xb3");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTR r_m64
									{
										 mal.leftIsReg = $r_m64.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
										 mal.rex_b =  mal.rex_rb;
									}
								COMMA r64
									{
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 64;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.rex_r =  mal.rex_rb;
										
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xb3");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTR r_m16 
									{	 mal.leftIsReg = $r_m16.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 16;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputEa(6, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTR r_m32
									{
										 mal.leftIsReg = $r_m32.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputEa(6, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTR r_m64 
									{
										 mal.leftIsReg = $r_m64.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 64;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputREX(true); 
										 mal.outputEa(6, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									};
*/
btr						:		{ mal.leftIsReg = false;  mal.rightIsReg = false;} BTR btr_left COMMA btr_right;

btr_left					:	r_m16
							{	 mal.leftIsReg = $r_m16.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						|	r_m32
							{
								 mal.leftIsReg = $r_m32.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						|	r_m64
							{
								 mal.leftIsReg = $r_m64.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						;
		
btr_right					:	r16
								{	
									
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 16; 
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.opcol = $r16.ctx.col;
										 mal.outputR( mal.opcol,"0x0f", "0xb3");
									}
								}
						|	r32
								{
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.opcol = $r32.ctx.col;
										 mal.outputR( mal.opcol,"0x0f", "0xb3");
									}
								}
						|	r64
								{
									 mal.rightIsReg = true; 
									 mal.rightOpSize = 64;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_r =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.opcol = $r64.ctx.col;
									 mal.outputR( mal.opcol,"0x0f", "0xb3");
								}
						|	imm 
								{
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 16;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize, true);
										
										if( mal.leftOpSize == 16 ||  mal.leftOpSize == 32) {
											 mal.outputEa(false,6,"no", CalculatorLibrary.cal($imm.text), "0X0f", "b3");
										}else {
											 mal.outputREX(true); 
											 mal.outputEa(false,6,"no", CalculatorLibrary.cal($imm.text), "0X0f", "b3");
										}
									}
								};

/*bts                     :			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTS r_m16 
									{	
										 mal.leftIsReg = $r_m16.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
										 mal.opcol = $r16.ctx.col;
									}
								COMMA r16
									{	
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 16; 
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xab");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTS r_m32 
									{
										 mal.leftIsReg = $r_m32.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
										 mal.opcol = $r32.ctx.col;
									}
								COMMA r32
									{
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xab");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTS r_m64
									{
										 mal.leftIsReg = $r_m64.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
										 mal.rex_b =  mal.rex_rb;
									}
								COMMA r64
									{
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 64;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.rex_r =  mal.rex_rb;
										 mal.outputREX(true); 
										 mal.oprow =  mal.modrm_row;
										 mal.outputR( mal.opcol,"0x0f", "0xab");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTS r_m16 
									{	
										 mal.leftIsReg = $r_m16.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 16;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputEa(5, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									}
                        |			{ mal.leftIsReg = false;  mal.rightIsReg = false;} 
								BTS r_m32
									{
										 mal.leftIsReg = $r_m32.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputEa(5, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									}
                        |			{ mal.leftIsReg = true;  mal.rightIsReg = true;}
								BTS r_m64 
									{
										 mal.leftIsReg = $r_m64.ctx.isReg; 
										 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
										 mal.rex_b =  mal.rex_rb;
									}
								COMMA imm
									{
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 64;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.outputREX(true); 
										 mal.outputEa(5, CalculatorLibrary.cal($imm.text), "0X0f", "ba");
									};
*/
bts						:		{ mal.leftIsReg = false;  mal.rightIsReg = false;} BTS bts_left COMMA bts_right;

bts_left					:	r_m16
							{	 mal.leftIsReg = $r_m16.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						|	r_m32
							{
								 mal.leftIsReg = $r_m32.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						|	r_m64
							{
								 mal.leftIsReg = $r_m64.ctx.isReg; 
								 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
								 mal.oprow =  mal.modrm_row;
							}
						;
		
bts_right				:	r16
								{	
									
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 16; 
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.opcol = $r16.ctx.col;
										 mal.outputR( mal.opcol,"0x0f", "0xab");
									}
								}
						|	r32
								{
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = true; 
										 mal.rightOpSize = 32;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
										 mal.opcol = $r32.ctx.col;
										 mal.outputR( mal.opcol,"0x0f", "0xab");
									}
								}
						|	r64
								{
									 mal.rightIsReg = true; 
									 mal.rightOpSize = 64;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_r =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.opcol = $r64.ctx.col;
									 mal.outputR( mal.opcol,"0x0f", "0xab");
								}
						|	imm 
								{
									if( mal.bits != 64 &&  mal.leftOpSize == 64) {
										mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
									} else {
										 mal.rightIsReg = false; 
										 mal.rightOpSize = 16;
										 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize, true);
										
										if( mal.leftOpSize == 16 ||  mal.leftOpSize == 32) {
											 mal.outputEa(false,5,"no", CalculatorLibrary.cal($imm.text), "0X0f", "ab");
										}else {
											 mal.outputREX(true); 
											 mal.outputEa(false,5,"no", CalculatorLibrary.cal($imm.text), "0X0f", "ab");
										}
									}
								};
/*bzhi                    :       BZHI r32a COMMA r_m32 COMMA r32b
                        |       BZHI r64a COMMA r_m64 COMMA r64b;

call                    :       CALL rel16
                        |       CALL rel32
                        |       CALL r_m16
                        |       CALL r_m32
                        |       CALL r_m64;
                        |       CALL ptr16COLON16
                        |       CALL ptr16COLON32
                        |       CALL m16COLON16
                        |       CALL m16COLON32
                        |       CALL m16COLON64;
                        */
cbw				:       CBW	{ mal.output("0x98");}   # C_B_W;

cwde			:       CWDE	{ mal.output("0x98");}	# C_W_D_E;

cdqe			:       CDQE	{ mal.outputREX(true);  mal.output("0x98");} # C_D_Q_E;

clac			:       CLAC  { mal.output("0x0F", "0x01","0xCA");} #C_L_A_C;

clc				:       CLC	{ mal.output("0xF8");}	# C_L_C;

cld				:       CLD	{ mal.output("0xFC");}	# C_L_D;

cldemote		:       CLDEMOTE m8 { mal.oprow= mal.modrm_row;  mal.outputR(0, "0x0F", "0x1C");};

clflush			:       CLFLUSH m8 { mal.oprow= mal.modrm_row;  mal.outputR(7, "0x0F", "0xAE");};

clflushopt		:       CLFLUSHOPT m8 { mal.oprow= mal.modrm_row;  mal.outputR(7, "0x66", "0x0F", "0xAE");};

cli				:       CLI	{ mal.output("0xFA");}	# C_L_I;

clts			:       CLTS { mal.output("0x0F","0x06");}# C_L_T_S;
	
clwb			:       CLWB m8{ mal.oprow= mal.modrm_row;  mal.outputR(6, "0x66", "0x0F", "0xAE");};

cmc				:       CMC	{ mal.output("0xF5");}	# C_M_C;

cmova                   :       { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVA cmova_left COMMA cmova_right;

cmova_left              :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmova_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x47");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x47");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x47");
                                    }
                        ;
cmovae                  :       { mal.leftIsReg = true;  mal.rightIsReg = false;}CMOVAE cmovae_left COMMA cmovae_right;
cmovae_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovae_right            :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x43");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x43");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x43");
                                    }
                        ;
cmovb                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVB cmovb_left COMMA cmovb_right;
cmovb_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovb_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x42");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x42");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x42");
                                    }
                        ;

cmovbe                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVBE cmovbe_left COMMA cmovbe_right;
cmovbe_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovbe_right            :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x46");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x46");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x46");
                                    }
                        ;
cmovc                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVC cmovc_left COMMA cmovc_right;
cmovc_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovc_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x42");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x42");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x42");
                                    }
                        ;
cmove                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVE cmove_left COMMA cmove_right;
cmove_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmove_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x44");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x44");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x44");
                                    }
                        ;
cmovg                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVG cmovg_left COMMA cmovg_right;
cmovg_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovg_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4f");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4f");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4f");
                                    }
                        ;
cmovge                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVGE cmovge_left COMMA cmovge_right;
cmovge_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovge_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4d");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4d");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4d");
                                    }
                        ;
cmovl                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVL cmovl_left COMMA cmovl_right;
cmovl_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovl_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4c");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4c");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4c");
                                    }
                        ;
cmovle                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVLE cmovle_left COMMA cmovle_right;
cmovle_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovle_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4e");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4e");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4e");
                                    }
                        ;
cmovna                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNA cmovna_left COMMA cmovna_right;
cmovna_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovna_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x46");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x46");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x46");
                                    }
                         ;
cmovnae                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNAE cmovnae_left COMMA cmovnae_right;
cmovnae_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovnae_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x42");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x42");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x42");
                                    }
                        ;
cmovnb                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNB cmovnb_left COMMA cmovnb_right;
cmovnb_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovnb_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x43");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x43");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x43");
                                    }
                        ;
cmovnbe                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNBE cmovnbe_left COMMA cmovnbe_right;
cmovnbe_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovnbe_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x47");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x47");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x47");
                                    }
                        ;
cmovnc                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNC cmovnc_left COMMA cmovnc_right;
cmovnc_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovnc_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x43");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x43");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x43");
                                    }
                        ;
cmovne                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNE cmovne_left COMMA cmovne_right;
cmovne_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovne_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x45");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x45");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x45");
                                    }
                        ;
cmovng                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNG cmovng_left COMMA cmovng_right;
cmovng_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovng_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4e");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4e");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4e");
                                    }
                        ;
cmovnge                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNGE cmovnge_left COMMA cmovnge_right;
cmovnge_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovnge_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4c");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4c");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4c");
                                    }
                        ;
cmovnl                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNL cmovnl_left COMMA cmovnl_right;
cmovnl_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovnl_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4d");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4d");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4d");
                                    }
                        ;
cmovnle                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNLE cmovnle_left COMMA cmovnle_right;
cmovnle_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovnle_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4f");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4f");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x4f");
                                    }
                        ;
cmovno                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNO cmovno_left COMMA cmovno_right;
cmovno_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
cmovno_right             :       r_m16{	
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
									 mal.rightIsReg = $r_m16.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x41");
                                    }
			|	r_m32{
									 mal.rightIsReg = $r_m32.ctx.isReg; 
									 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x41");
                                    }
			|	r_m64{
									 mal.rightIsReg = $r_m64.ctx.isReg; 
									 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
									 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
									 mal.rex_b =  mal.rex_rb;
									 mal.outputREX(true); 
									 mal.oprow =  mal.modrm_row; 
									 mal.outputR( mal.opcol,"0x0f", "0x41");
                                    }
                        ;
/*cmovnp                  :      { mal.leftIsReg = true;  mal.rightIsReg = false;} CMOVNP cmovnp_left COMMA cmovnp_right;
cmovnp_left             :       r16 { mal.leftOpSize = $r16.ctx.operandSize;  mal.opcol = $r16.ctx.col;}
                        |       r32 { mal.leftOpSize = $r32.ctx.operandSize;  mal.opcol = $r32.ctx.col;}
                        |       r64 { mal.rex_r =  mal.rex_rb;  mal.leftOpSize = $r64.ctx.operandSize;  mal.opcol = $r64.ctx.col;}
                        ;
*/
/*cmp                     :     { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP AL COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP AX COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP EAX COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP RAX COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m8 COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m16 COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m32 COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m64 COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m16 COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m32 COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m64 COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m8 COMMA r8
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m16 COMMA r16
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m32 COMMA r32
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r_m64 COMMA r64
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r8 COMMA r_m8
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r16 COMMA r_m16
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r32 COMMA r_m32
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP r64 COMMA r_m64;*/
cmp						:       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP AL COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP AX COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP EAX COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP RAX COMMA imm
                        |       { mal.leftIsReg = false;  mal.rightIsReg = false;} CMP cmp_left COMMA cmp_right;

cmp_left                 :			r8
										{
											 mal.leftIsReg = true; 
											 mal.leftOpSize = 8;
											 mal.opcol = $r8.ctx.col;
										}
                         |			r16
										{
											 mal.leftIsReg = true; 
											 mal.leftOpSize = 16;
											 mal.opcol = $r16.ctx.col;
										}	
                         |			r32
										{
											 mal.leftIsReg = true; 
											 mal.leftOpSize = 32;
											 mal.opcol = $r32.ctx.col;
										}	
					    |			r64
										{
											 mal.leftIsReg = true; 
											 mal.leftOpSize = 64;
											 mal.rex_r =  mal.rex_rb;
											 mal.opcol = $r64.ctx.col;
										}
						|			r_m8
										{
											 mal.leftIsReg = $r_m8.ctx.isReg; 
											 mal.leftOpSize =  mal.leftIsReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize;
											 mal.oprow =  mal.modrm_row;
										}
						|			r_m16
										{	 mal.leftIsReg = $r_m16.ctx.isReg; 
											 mal.leftOpSize =  mal.leftIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize;
											 mal.oprow =  mal.modrm_row;
										}
						|			r_m32
										{
											 mal.leftIsReg = $r_m32.ctx.isReg; 
											 mal.leftOpSize =  mal.leftIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
											 mal.oprow =  mal.modrm_row;
										}
						|			r_m64
										{
											 mal.leftIsReg = $r_m64.ctx.isReg; 
											 mal.leftOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
											 mal.oprow =  mal.modrm_row;
											 mal.rex_b =  mal.rex_rb;
										}
						;

cmp_right				:			
									r8	
										{
											if( mal.leftOpSize != 8) {
												System.out.println("error: invalid combination of opcode and operands");
											}else {
												 mal.rightIsReg = true; 
												 mal.rightOpSize = 8; 
												 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
												 mal.opcol = $r8.ctx.col;
												 mal.outputR( mal.opcol,"0x38");
											}
										}
						|			r16
										{	
											if( mal.leftOpSize != 16){
												System.out.println("error: invalid combination of opcode and operands");
											}else if( mal.bits != 64 &&  mal.leftOpSize == 64) {
												mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
											} else {
												 mal.rightIsReg = true; 
												 mal.rightOpSize = 16; 
												 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
												 mal.opcol = $r16.ctx.col;
												 mal.outputR( mal.opcol,"0x39");
											}
										}
						|			r32
										{
											if( mal.leftOpSize != 32){
												System.out.println("error: invalid combination of opcode and operands");
											}else if( mal.bits != 64 &&  mal.leftOpSize == 64) {
												mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
											} else {
												 mal.rightIsReg = true; 
												 mal.rightOpSize = 32;
												 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
												 mal.opcol = $r32.ctx.col;
												 mal.outputR( mal.opcol,"0x39");
											}
										}
						|			r64
										{
											if( mal.leftOpSize != 64){
												System.out.println("error: invalid combination of opcode and operands");
											}else {
												 mal.rightIsReg = true; 
												 mal.rightOpSize = 64;
												 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
												 mal.rex_r =  mal.rex_rb;
												 mal.outputREX(true); 
												 mal.opcol = $r64.ctx.col;
												 mal.outputR( mal.opcol, "0x39");
											}
										}
						|			r_m8
										{	
											 mal.rightIsReg = $r_m8.ctx.isReg;
											if(! mal.rightIsReg && ! mal.leftIsReg) {
												System.out.println("error: invalid combination of opcode and operands");
											}else{
												 mal.rightOpSize =  mal.rightIsReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize; 
												 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
												 mal.oprow =  mal.modrm_row; 
												 mal.outputR( mal.opcol,"0x3a");
											}
										}
						|	r_m16
										{	
											 mal.rightIsReg = $r_m16.ctx.isReg; 
											if(! mal.rightIsReg && ! mal.leftIsReg) {
												System.out.println("error: invalid combination of opcode and operands");
											}else{
												 mal.rightOpSize =  mal.rightIsReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize; 
												 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
												 mal.oprow =  mal.modrm_row; 
												 mal.outputR( mal.opcol,"0x3b");
											}
										}
						|	r_m32
										{
											if(! mal.rightIsReg && ! mal.leftIsReg) {
												System.out.println("error: invalid combination of opcode and operands");
											}else{
												 mal.rightIsReg = $r_m32.ctx.isReg; 
												 mal.rightOpSize =  mal.rightIsReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize;
												 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
												 mal.oprow =  mal.modrm_row; 
												 mal.outputR( mal.opcol,"0x3b");
											}
										}
						|	r_m64
										{
											if(! mal.rightIsReg && ! mal.leftIsReg) {
												System.out.println("error: invalid combination of opcode and operands");
											}else{
												 mal.rightIsReg = $r_m64.ctx.isReg; 
												 mal.rightOpSize =  mal.leftIsReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize;
												 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize);
												 mal.rex_b =  mal.rex_rb;
												 mal.outputREX(true); 
												 mal.oprow =  mal.modrm_row; 
												 mal.outputR( mal.opcol,"0x3b");
											}
										}
						|       imm
									{
										if( mal.bits != 64 &&  mal.leftOpSize == 64) {
											mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
										} else {
											 mal.rightIsReg = false; 
											 mal.rightOpSize = 8;
											 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize, true);
											
											if( mal.leftOpSize == 8) {
												 mal.outputEa(false,7,"no", CalculatorLibrary.cal($imm.text), "0X80");
											}else {
												if( mal.leftOpSize == 64) {
													 mal.outputREX(true); 
												}
												 mal.outputEa(false,7,"no", CalculatorLibrary.cal($imm.text), "0X80");	
											}
										}
									}
						|       imm
									{
										if( mal.bits != 64 &&  mal.leftOpSize == 64) {
											mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
										} else if ( mal.leftOpSize != 16) {
											System.out.println("error: invalid combination of opcode and operands");
										}else {
											 mal.rightIsReg = false; 
											 mal.rightOpSize = 16;
											 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize, true);
											 mal.outputEa(false,7,"no", CalculatorLibrary.cal($imm.text), "0X81");
										}
									}
						|       imm
									{
										if( mal.bits != 64 &&  mal.leftOpSize == 64) {
											mal.printError("error: instruction not supported in " +  mal.bits + "-bit mode");
										} else if ( mal.leftOpSize != 32 ||  mal.leftOpSize != 64) {
											System.out.println("error: invalid combination of opcode and operands");
										}else {
											 mal.rightIsReg = false; 
											 mal.rightOpSize = 32;
											 mal.checkPrefix( mal.leftIsReg,  mal.leftOpSize,  mal.rightIsReg,  mal.rightOpSize, true);
											 mal.outputEa(false,7,"no", CalculatorLibrary.cal($imm.text), "0X81");
										}
									};
/*
cmppd                   :       CMPPD xmm1 COMMA xmm2_m128 COMMA imm;
vcmppd                  :       VCMPPD xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VCMPPD ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm
                        |       VCMPPD k1 {k2} COMMA xmm2 COMMA xmm3_m128_m64bcst COMMA imm
                        |       VCMPPD k1 {k2} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VCMPPD k1 {k2} COMMA zmm2 COMMA zmm3_m512_m64bcst{sae} COMMA imm;

cmpps                   :       CMPPS xmm1 COMMA xmm2_m128 COMMA imm;
vcmpps                  :       VCMPPS xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VCMPPS ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm
                        |       VCMPPS k1 {k2} COMMA xmm2 COMMA xmm3_m128_m32bcst COMMA imm
                        |       VCMPPS k1 {k2} COMMA ymm2 COMMA ymm3_m256_m32bcst COMMA imm
                        |       VCMPPS k1 {k2} COMMA zmm2 COMMA zmm3_m512_m32bcst{sae} COMMA imm;*/


/*cmpsb                   :       CMPSB;
cmpsw                   :       CMPSW;
cmpsd                   :       CMPSD
                        |       CMPSD xmm1 COMMA xmm2_m64 COMMA imm;
cmpsq                   :       CMPSQ;

vcmpsd                  :       VCMPSD xmm1 COMMA xmm2 COMMA xmm3_m64 COMMA imm
                        |       VCMPSD k1 {k2} COMMA xmm2 COMMA xmm3_m64{sae} COMMA imm;

cmpss                   :       CMPSS xmm1 COMMA xmm2_m32 COMMA imm;
vcmpss                  :       VCMPSS xmm1 COMMA xmm2 COMMA xmm3_m32 COMMA imm
                        |       VCMPSS k1 {k2} COMMA xmm2 COMMA xmm3_m32{sae} COMMA imm;

cmpxchg                 :       CMPXCHG r_m8 COMMA r8
                        |       CMPXCHG r_m16 COMMA r16
                        |       CMPXCHG r_m32 COMMA r32
                        |       CMPXCHG r_m64 COMMA r64;

cmpxchg8b               :       CMPXCHG8B m64;
cmpxchg16b              :       CMPXCHG16B m128;

comisd                  :       COMISD xmm1 COMMA xmm2_m64;
vcomisd                 :       VCOMISD xmm1 COMMA xmm2_m64
                        |       VCOMISD xmm1 COMMA xmm2_m64{sae};

comiss                  :       COMISS xmm1 COMMA xmm2_m32
vcomiss                 :       VCOMISS xmm1 COMMA xmm2_m32
                        |       VCOMISS xmm1 COMMA xmm2_m32{sae};

cpuid			:       CPUID { mal.output("0x0F","0xA2");}# C_P_U_I_D;*/

crc32                   :       CRC32 r32 COMMA r_m8 { mal.oprow =  mal.modrm_row;  mal.outputR($r32.col, "0xf2","0x0f","0x38","0xf0");}
                        |       CRC32 r32 COMMA r_m16 { mal.oprow =  mal.modrm_row;  mal.outputR($r32.col, "0xf2","0x0f","0x38","0xf1");}
                        |       CRC32 r32 COMMA r_m32{ mal.oprow =  mal.modrm_row;  mal.outputR($r32.col, "0xf2","0x0f","0x38","0xf1");}
                        |       CRC32 r64 { mal.rex_b =  mal.rex_rb;} COMMA r_m8{ mal.oprow =  mal.modrm_row; mal.output("0xf2"); mal.outputREX(true);  mal.outputR($r64.col,"0x0f","0x38","0xf0");}
                        |       CRC32 r64{ mal.rex_b =  mal.rex_rb;}  COMMA r_m64{ mal.oprow =  mal.modrm_row; mal.output("0xf2"); mal.outputREX(true);  mal.outputR($r64.col,"0x0f","0x38","0xf0");};

/*cvtdq2pd                :       CVTDQ2PD xmm1 COMMA xmm2_m64;
vcvtdq2pd               :       VCVTDQ2PD xmm1 COMMA xmm2_m64
                        |       VCVTDQ2PD ymm1 COMMA xmm2_m128
                        |       VCVTDQ2PD xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTDQ2PD ymm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTDQ2PD zmm1 {k1}{z} COMMA ymm2_m256_m32bcst;

cvtdq2ps                :       CVTDQ2PS xmm1 COMMA xmm2_m128;
vcvtdq2ps               :       VCVTDQ2PS xmm1 COMMA xmm2_m128
                        |       VCVTDQ2PS ymm1 COMMA ymm2_m256
                        |       VCVTDQ2PS xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTDQ2PS ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VCVTDQ2PS zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{er};

cvtpd2dq                :       CVTPD2DQ xmm1 COMMA xmm2_m128;
vcvtpd2dq               :       VCVTPD2DQ xmm1 COMMA xmm2_m128
                        |       VCVTPD2DQ xmm1 COMMA ymm2_m256
                        |       VCVTPD2DQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTPD2DQ xmm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTPD2DQ ymm1 {k1}{z} COMMA zmm2_m512_m64bcst{er};

cvtpd2pi                :       CVTPD2PI mm COMMA xmm_m128;

cvtpd2ps                :       CVTPD2PS xmm1 COMMA xmm2_m128;
vcvtpd2ps               :       VCVTPD2PS xmm1 COMMA xmm2_m128
                        |       VCVTPD2PS xmm1 COMMA ymm2_m256
                        |       VCVTPD2PS xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTPD2PS xmm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTPD2PS ymm1 {k1}{z} COMMA zmm2_m512_m64bcst{er};

cvtpi2pd                :       CVTPI2PD xmm COMMA mm_m64;

cvtpi2ps                :       CVTPI2PS xmm COMMA mm_m64;

cvtps2dq                :       CVTPS2DQ xmm1 COMMA xmm2_m128;
vcvtps2dq               :       VCVTPS2DQ xmm1 COMMA xmm2_m128
                        |       VCVTPS2DQ ymm1 COMMA ymm2_m256
                        |       VCVTPS2DQ xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTPS2DQ ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VCVTPS2DQ zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{er};

cvtps2pd                :       CVTPS2PD xmm1 COMMA xmm2_m64
vcvtps2pd               :       VCVTPS2PD xmm1 COMMA xmm2_m64
                        |       VCVTPS2PD ymm1 COMMA xmm2_m128
                        |       VCVTPS2PD xmm1 {k1}{z} COMMA xmm2_m64_m32bcst
                        |       VCVTPS2PD ymm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTPS2PD zmm1 {k1}{z} COMMA ymm2_m256_m32bcst{sae};

cvtps2pi                :       CVTPS2PI mm COMMA xmm_m64;

cvtsd2si                :       CVTSD2SI r32 COMMA xmm1_m64
                        |       CVTSD2SI r64 COMMA xmm1_m64;
vcvtsd2si               :       VCVTSD2SI r32 COMMA xmm1_m64
                        |       VCVTSD2SI r53 COMMA xmm1_m64
                        |       VCVTSD2SI r32 COMMA xmm1_m64{er}
                        |       VCVTSD2SI r64 COMMA xmm1_m64{er};

cvtsd2ss                :       CVTSD2SS xmm1 COMMA xmm2_m64;
vcvtsd2ss               :       VCVTSD2SS xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VCVTSD2SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};

cvtsi2sd                :       CVTSI2SD xmm1 COMMA r32_m32
                        |       CVTSI2SD xmm1 COMMA r_m64;
vcvtsi2sd               :       VCVTSI2SD xmm1 COMMA xmm2 COMMA r_m32
                        |       VCVTSI2SD xmm1 COMMA xmm2 COMMA r_m64
                        |       VCVTSI2SD xmm1 COMMA xmm2 COMMA r_m32
                        |       VCVTSI2SD xmm1 COMMA xmm2 COMMA r_m64{er};

cvtsi2ss                :       CVTSI2SS xmm1 COMMA r_m32
                        |       CVTSI2SS xmm1 COMMA r_m64;
vcvtsi2ss               ;       VCVTSI2SS xmm1 COMMA xmm2 COMMA r_m32
                        |       VCVTSI2SS xmm1 COMMA xmm2 COMMA r_m64
                        |       VCVTSI2SS xmm1 COMMA xmm2 COMMA r_m32{er}
                        |       VCVTSI2SS xmm1 COMMA xmm2 COMMA r_m64{er};

cvtss2sd                :       CVTSS2SD xmm1 COMMA xmm2_m32;
vcvtss2sd               :       VCVTSS2SD xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VCVTSS2SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{sae};

cvtss2si                :       CVTSS2SI r32 COMMA xmm1_m32
                        |       CVTSS2SI r64 COMMA xmm1_m32;

vcvtss2si               :       VCVTSS2SI r32 COMMA xmm1_m32
                        |       VCVTSS2SI r64 COMMA xmm1_m32
                        |       VCVTSS2SI r32 COMMA xmm1_m32{er}
                        |       VCVTSS2SI r64 COMMA xmm1_m32{er};

cvttpd2dq               :       CVTTPD2DQ xmm1 COMMA xmm2_m128;
vcvttpd2dq              :       VCVTTPD2DQ xmm1 COMMA xmm2_m128
                        |       VCVTTPD2DQ xmm1 COMMA ymm2_m256
                        |       VCVTTPD2DQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTTPD2DQ xmm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTTPD2DQ ymm1 {k1}{z} COMMA zmm2_m512_m64bcst{sae};

cvttpd2pi               :       CVTTPD2PI mm COMMA xmm_m128;

cvttps2dq               :       CVTTPS2DQ xmm1 COMMA xmm2_m128;
vcvttps2dq              :       VCVTTPS2DQ xmm1 COMMA xmm2_m128
                        |       VCVTTPS2DQ ymm1 COMMA ymm2_m256
                        |       VCVTTPS2DQ xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTTPS2DQ ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VCVTTPS2DQ zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{sae};

cvttps2pi               :       CVTTPS2PI mm COMMA xmm_m64;

cvttsd2si               :       CVTTSD2SI r32 COMMA xmm1_m64
                        |       CVTTSD2SI r64 COMMA xmm1_m64;
vcvttsd2si              :       VCVTTSD2SI r32 COMMA xmm1_m64
                        |       VCVTTSD2SI r64 COMMA xmm1_m64
                        |       VCVTTSD2SI r32 COMMA xmm1_m64{sae}
                        |       VCVTTSD2SI r64 COMMA xmm1_m64{sae};

cvttss2si               :       CVTTSS2SI r32 COMMA xmm1_m32
                        |       CVTTSS2SI r64 COMMA xmm1_m32;
vcvttss2si              :       VCVTTSS2SI r32 COMMA xmm1_m32
                        |       VCVTTSS2SI r64 COMMA xmm1_m32
                        |       VCVTTSS2SI r32 COMMA xmm1_m32{sae}
                        |       VCVTTSS2SI r64 COMMA xmm1_m32{sae};*/

cwd                     :       CWD{ mal.output("0x99");};
cdq                     :       CDQ{ mal.output("0x99");};
cqo                     :       CQO{ mal.outputREX(true);  mal.outputOPMODE(2,"0x99");};
daa			:       DAA	{ mal.outputOPMODE(1,"0x27");}	# D_A_A;

das			:       DAS	{ mal.outputOPMODE(1,"0x2f");}	# D_A_S;


dec                     :       DEC r_m8 { mal.checkPrefix($r_m8.ctx.isReg,$r_m8.ctx.isReg?$r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize,false, mal.bits ); mal.oprow =  mal.modrm_row;  mal.outputR(1,"0xfe");}
                        |       DEC r_m16 { mal.checkPrefix($r_m16.ctx.isReg,$r_m16.ctx.isReg?$r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize,false, mal.bits ); mal.oprow =  mal.modrm_row;  mal.outputR(1,"0xff");}
                        |       DEC r_m32 { mal.checkPrefix($r_m32.ctx.isReg,$r_m32.ctx.isReg?$r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize,false, mal.bits ); mal.oprow =  mal.modrm_row;  mal.outputR(1,"0xff");}
                        |       DEC r_m64 { mal.checkPrefix($r_m64.ctx.isReg,$r_m64.ctx.isReg?$r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize,false, mal.bits ); mal.rex_b =  mal.rex_rb; mal.oprow =  mal.modrm_row;  mal.outputREX(true);  mal.outputR(1,"0xff");}
                        |       DEC r16  { mal.checkPrefix(true,$r16.ctx.operandSize,false, mal.bits ); mal.outputAddRD(0x48);}
                        |       DEC r32  { mal.checkPrefix(true,$r32.ctx.operandSize,false, mal.bits ); mal.outputAddRD(0x48);}
                        ;

div                     :       DIV r_m8 { mal.checkPrefix($r_m8.ctx.isReg, $r_m8.ctx.isReg?$r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize,false, mal.bits);
                                           mal.oprow =  mal.modrm_row;  mal.outputR(6,"0xf6");}
                        |       DIV r_m16 { mal.checkPrefix($r_m16.ctx.isReg, $r_m16.ctx.isReg?$r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize,false, mal.bits);
                                           mal.oprow =  mal.modrm_row;  mal.outputR(6,"0xf7");}
                        |       DIV r_m32{ mal.checkPrefix($r_m32.ctx.isReg, $r_m32.ctx.isReg?$r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize,false, mal.bits);
                                           mal.oprow =  mal.modrm_row;  mal.outputR(6,"0xf7");}
                        |       DIV r_m64{ mal.checkPrefix($r_m64.ctx.isReg, $r_m64.ctx.isReg?$r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize,false, mal.bits);
                                           mal.rex_b =  mal.rex_rb;  mal.outputREX(true);
                                           mal.oprow =  mal.modrm_row;  mal.outputR(6,"0xf7");}
                        ;

/*divpd                   :       DIVPD xmm1 COMMA xmm2_m128;
vdivpd                  :       VDIVPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VDIVPD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VDIVPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VDIVPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VDIVPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};

divps                   :       DIVPS xmm1 COMMA xmm2_m128;
vdivps                  :       VDIVPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VDIVPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VDIVPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VDIVPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VDIVPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};

divsd                   :       DIVSD xmm1 COMMA xmm2_m64;
vdivsd                  :       VDIVSD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VDIVSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};


divss                   :       DIVSS xmm1 COMMA xmm2_m32;
vdivss                  :       VDIVSS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VDIVSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};

dppd                    :       DPPD xmm1 COMMA xmm2_m128 COMMA imm;
vdppd                   :       VDPPD xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm;

dpps                    :       DPPS xmm1 COMMA xmm2_m128 COMMA imm;
vddps                   :       VDPPS xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VDPPS ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm;
*/
emms                    :       EMMS { mal.output("0x0F", "0x77");};

enter                   :       ENTER imm COMMA ZERO { mal.output("0xc8",CalculatorLibrary.cal($imm.text),"0x00");}
                        |       ENTER imm COMMA ONE { mal.output("0xc8",CalculatorLibrary.cal($imm.text),"0x01");}
                        |       ENTER imm COMMA imm{ mal.output("0xc8",CalculatorLibrary.cal($imm.text),CalculatorLibrary.cal($imm.text));} ;

/*extractps               :       EXTRACTPS reg_m32 COMMA xmm1 COMMA imm;
vextractps              :       VEXTRACTPS reg_m32 COMMA xmm1 COMMA imm
                        |       VEXTRACTPS reg_m32 COMMA xmm1 COMMA imm;

f2xm1			:       F2XM1	{ mal.output("0xD9","0XF0");}	# F_2_X_M_1;
*/
fabs			:       FABS	{ mal.output("0xd9","0xe1");}	# F_A_B_S;

/*fadd                    :       FADD m32fp
                        |       FADD m64fp
                        |       FADD ST(0) COMMA ST(1)
                        |       FADD ST(1) COMMA ST(0);
faddp                   :       FADDP ST(1) COMMA ST(0)
                        |       FADDP;
fiadd                   :       FIADD m32int
                        |       FIADD m16int;*/

/*fbld                  :       FBLD m80bcd;

//fbstp                 :       FBSTP m80bcd;*/

fchs                    :       FCHS	{ mal.output("0xd9","0xe0");}	# F_C_H_S;

fclex				   :       FCLEX  { mal.output("0x9b","0xdb","0xe2");};
fnclex				   :       FNCLEX  { mal.output("0xdb","0xe2");};

/*fcmovb                  :       FCMOVB ST(0) COMMA ST(1);
fcmove                  :       FCMOVE ST(0) COMMA ST(1);
fcmovbe                 :       FCMOVBE ST(0) COMMA ST(1);
fcmovu                  :       FCMOVU ST(0) COMMA ST(1);
fcmovnb                 :       FCMOVNB ST(0) COMMA ST(1);
fcmovne                 :       FCMOVNE ST(0) COMMA ST(1);
fcmovnbe                :       FCMOVNBE ST(0) COMMA ST(1);
fcmovnu                 :       FCMOVNU ST(0) COMMA ST(1);*/

/*fcom                    :       FCOM m32fp
                        |       FCOM m64fp
                        |       FCOM ST(1)
                        |       FCOM;

fcomp                   :       FCOMP m32fp
                        |       FCOMP m64fp
                        |       FCOMP ST(1)
                        |       FCOMP;*/

fcompp                  :       FCOMPP{ mal.output("0xde","0xd9");} #F_C_O_M_P_P;

/*fcomi                   :       FCOMI ST COMMA ST(1);
fcomip                  :       FCOMIP ST COMMA ST(1);
fucomi                  :       FUCOMI ST COMMA ST(1);
fucomip                 :       FUCOMIP ST COMMA ST(1);*/

fcos			:       FCOS	{ mal.output("0xd9","0xff");}	# F_C_O_S ;

fdecstp			:       FDECSTP	{ mal.output("0xd9","0xf6");}   	# F_D_E_C_S_T_P;

//fdiv                    :       FDIV m32fp
 //                       |       FDIV m64fp
  //                      |       FDIV ST(0) COMMA ST(1)
 //                       |       FDIV ST(1) COMMA ST(0);
//fdivp                   :       FDIVP ST(1) COMMA ST(0)
//                        |       FDIVP;
/*fidiv                   :       FIDIV m32int
                        |       FIDIV m16int;

//fdivr                   :       FDIVR m32fp
  //                      |       FDIVR m64fp
  //                      |       FDIVR ST(0) COMMA ST(1)
 //                       |       FDIVR ST(1) COMMA ST(0);
//fdivrp                  :       FDIVRP ST(1) COMMA ST(0)
 //                       |       FDIVRP;
/*fidivr                  :       FIDIVR m32int
                        |       FIDIVR m16int;

//ffree                   :       FFREE ST(1);

/*ficom                   :       FICOM m16int
                        |       FICOM m32int;
ficomp                  :       FICOMP m16int
                        |       FICOMP m32int;

fild                    :       FILD m16int
                        |       FILD m32int
                        |       FILD m64int;
fincstp			:       FINCSTP	{ mal.output("0xD9","0xF7");}	# F_I_N_C_S_T_P;*/

finit                   :       FINIT { mal.output("0x9b","0xdb","0xe3");};
fninit                  :       FNINIT { mal.output("0xdb","0xe3");};

/*fist                    :       FIST m16int
                        |       FIST m32int;
fistp                   :       FISTP m16int
                        |       FISTP m32int
                        |       FISTP m64int;

fisttp                  :       FISTTP m16int
                        |       FISTTP m32int
                        |       FISTTP m64int;

/*fld                     :       FLD m32fp
                        |       FLD m64fp
                        |       FLD m80fp
                        |       FLD ST(1);*/

fld1			:       FLD1	{ mal.output("0xd9","0xe8");}	# F_L_D_1;
fldl2t			:       FLDL2T	{ mal.output("0xd9","0xe9");}	# F_L_D_L_2_T;
fldl2e			:       FLDL2E	{ mal.output("0xd9","0xea");}	# F_L_D_L_2_E;
fldpi			:       FLDPI	{ mal.output("0xd9","0xeb");}	# F_L_D_P_I;
fldlg2			:       FLDLG2	{ mal.output("0xd9","0xec");}	# F_L_D_L_G_2;
fldln2			:       FLDLN2	{ mal.output("0xd9","0xed");}	# F_L_D_L_N_2;
fldz			:       FLDZ	{ mal.output("0xd9","0xee");}	# F_L_D_Z;

/*fldcw                   :       FLDCW m2byte;
fldenv                  :       FLDENV m14_28byte;
fmul                    :       FMUL m32fp
                        |       FMUL m64fp
                        |       FMUL ST(0) COMMA ST(1)
                        |       FMUL ST(1) COMMA ST(0);
fmulp                   :       FMULP ST(1) COMMA ST(0)
                        |       FMULP;
fimul                   :       FIMUL m32int
                        |       FIMUL m16int;
*/
fnop				       :       FNOP	{ mal.output("0xd9","0xd0");}	# F_N_O_P;

fpatan			       :       FPATAN	{ mal.output("0xd9","0xf3");}	# F_P_A_T_A_N;

fprem                   :       FPREM{ mal.output("0xd9","0xf8");};

fprem1                  :       FPREM1 { mal.output("0xd9","0xf5");};

fptan                   :       FPTAN { mal.output("0xd9","0xf2");};

frndint			       :       FRNDINT	{ mal.output("0xd9","0xfc");}	# F_R_N_D_I_N_T;

/*frstor                  :       FRSTOR m94_108byte;

fsave                   :       FSAVE m94_108byte;
fnsave                  :       FNSAVE m94_108byte;

fscale			:       FSCALE	{ mal.output("0xD9","0xFD");}	# F_S_C_A_L_E;

fsin			:       FSIN	{ mal.output("0xD9","0xFE");}	# F_S_I_N;

fsincos			:       FSINCOS	{ mal.output("0xD9","0xFB");}	# F_S_I_N_C_O_S;

fsqrt			:       FSQRT	{ mal.output("0xD9","0xFA");}	# F_S_Q_R_T;

/*fst                     :       FST m32fp
                        |       FST m64fp
                        |       FST ST(1);
fstp                    :       FSTP m32fp
                        |       FSTP m64fp
                        |       FSTP m80fp
                        |       FSTP ST(1);

fstcw                   :       FSTCW m2byte;
fnstcw                  :       FNSTCW m2byte;

fstenv                  :       FSTENV m14_28byte;
fnstenv                 :       FNSTENV m14_28byte;*/

/*fstsw                   :       FSTSW m2byte
                        |       FSTSW AX;
fnstsw                  :       FNSTSW m2byte
                        |       FNSTSW AX;

*/
/*fsub                    :       FSUB m32fp
                        |       FSUB m64fp
                        |       FSUB ST(0) COMMA ST(1)
                        |       FSUB ST(1) COMMA ST(0);
fsubp                   :       FSUBP ST(1) COMMA ST(0)
                        |       FSUBP;
fisub                   :       FISUB m32int
                        |       FISUB m16int;

fsubr                   :       FSUBR m32fp
                        |       FSUBR m64fp
                        |       FSUBR ST(0) COMMA ST(1)
                        |       FSUBR ST(1) COMMA ST(0);
fsubrp                  :       FSUBRP ST(1) COMMA ST(0)
                        |       FSUBRP;
fisubr                  :       FISUBR m32int
                        |       FISUBR m16int;

ftst			:       FTST	{ mal.output("0xD9","0xE4");}	# F_T_S_T;

fucom                   :       FUCOM ST(1)
                        |       FUCOM;
fucomp                  :       FUCOMP ST(1)
                        |       FUCOMP;
fucompp                 :       FUCOMPP;

fxam			:       FXAM	{ mal.output("0xD9","0xE5");}	# F_X_A_M;

fxch                    :       FXCH ST(1)
                        |       FXCH;*/

/*fxrstor                 :       FXRSTOR m512byte;
fxrstor64               :       FXRSTOR64 m512byte;

fxsave                  :       FXSAVE m512byte;
fxsave64                :       FXSAVE64 m512byte;

fxtract                 :       FXTRACT;

fyl2x			:       FYL2X	{ mal.output("0xD9","0xF1");}	# F_Y_L_2_X;

fyl2xp1			:       FYL2XP1	{ mal.output("0xD9","0xF9");}	# F_Y_L_2_X_P_1;

gf2p8affineinvqb        :       GF2P8AFFINEINVQB xmm1 COMMA xmm2_m128 COMMA imm;

gf2p8affineqb           :       GF2P8AFFINEQB xmm1 COMMA xmm2_m128 COMMA imm;

gf2p8mulb               :       GF2P8MULB xmm1 COMMA xmm2_m128;

haddpd                  :       HADDPD xmm1 COMMA xmm2_m128;
vhaddpd                 :       VHADDPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VHADDPD ymm1 COMMA ymm2 COMMA ymm3_m256;

haddps                  :       HADDPS xmm1 COMMA xmm2_m128;
vhaddps                 :       VHADDPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VHADDPS ymm1 COMMA ymm2 COMMA ymm3_m256;

hlt			:       HLT	{ mal.output("0xF4");}	# H_L_T;

hsubpd                  :       HSUBPD xmm1 COMMA xmm2_m128;
vhsubpd                 :       VHSUBPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VHSUBPD ymm1 COMMA ymm2 COMMA ymm3_m256;

hsubps                  :       HSUBPS xmm1 COMMA xmm2_m128;
vhsubps                 :       VHSUBPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VHSUBPS ymm1 COMMA ymm2 COMMA ymm3_m256;*/

idiv                    :       IDIV r_m8   { mal.oprow =  mal.modrm_row;
                                              mal.checkPrefix($r_m8.ctx.isReg, $r_m8.ctx.isReg? $r_m8.ctx.r8().operandSize:$r_m8.ctx.m8().operandSize, false, mal.bits);
                                              mal.outputR(7,"0xf6");}    #IDIV_R_M8
                        |       IDIV r_m16  { mal.oprow =  mal.modrm_row;
                                              mal.checkPrefix($r_m16.ctx.isReg, $r_m16.ctx.isReg? $r_m16.ctx.r16().operandSize:$r_m16.ctx.m16().operandSize, false, mal.bits);
                                              mal.outputR(7,"0xf7");} #IDIV_R_M16
                        |       IDIV r_m32  { mal.oprow =  mal.modrm_row;
                                              mal.checkPrefix($r_m32.ctx.isReg, $r_m32.ctx.isReg? $r_m32.ctx.r32().operandSize:$r_m32.ctx.m32().operandSize, false, mal.bits);
                                              mal.outputR(7,"0xf7");} #IDIV_R_M32
                        |       IDIV r_m64  { mal.oprow =  mal.modrm_row;  mal.rex_b =  mal.rex_rb;
                                              mal.checkPrefix($r_m64.ctx.isReg, $r_m64.ctx.isReg? $r_m64.ctx.r64().operandSize:$r_m64.ctx.m64().operandSize, false, mal.bits);
                                              mal.outputREX(true);
                                              mal.outputR(7,"0xf7");} #IDIV_R_M64
                        ;
/*
imul                    :       IMUL r_m8   #IMUL_R_M8
                        |       IMUL r_m16  #IMUL_R_16
                        |       IMUL r_m32  #IMUL_R_M32
                        |       IMUL r_m64  #IMUL_R_M64 
                        |       IMUL r16 COMMA r_m16
                        |       IMUL r32 COMMA r_m32
                        |       IMUL r64 COMMA r_m64
                        |       IMUL r16 COMMA r_m16 COMMA imm
                        |       IMUL r32 COMMA r_m32 COMMA imm
                        |       IMUL r64 COMMA r_m64 COMMA imm
                        |       IMUL r16 COMMA r_m16 COMMA imm
                        |       IMUL r32 COMMA r_m32 COMMA imm
                        |       IMUL r64 COMMA r_m64 COMMA imm
                        ;
/*
in			:       IN AL COMMA imm		{ mal.output("0xE4", CalculatorLibrary.cal($imm.text));}	# IN_AL__imm
			|       IN AX COMMA imm		{ mal.output("0xE5", CalculatorLibrary.cal($imm.text));}	# IN_AX__imm 
	        	|       IN EAX COMMA imm		{ mal.output("0xE5", CalculatorLibrary.cal($imm.text));}	# IN_EAX__imm
	        	|       IN AL COMMA DX                  { mal.output("0xEC");}                   # IN_AL__DX
	        	|       IN AX COMMA DX   		{ mal.output("0xED");}	                # IN_AX__DX
	        	|       IN EAX COMMA DX		        { mal.output("0xED");}	                # IN_EAX__DX
			;

inc                     :       INC r_m8
                        |       INC r_m16
                        |       INC r_m32
                        |       INC r_m64
                        |       INC r16
                        |       INC r32;

ins			:       INS m8 COMMA DX	{ mal.output("0x6C");}	# INS_M8__DX
			|       INS m16 COMMA DX{ mal.output("0x6D");}	# INS_M16__DX
			|       INS m32 COMMA DX{ mal.output("0x6D");}	# INS_M32__DX;
insb			:       INSB	{ mal.output("0x6C");}	# I_N_S_B;
insw			:       INSW	{ mal.output("0x6D");}	# I_N_S_W;
insd			:       INSD	{ mal.output("0x6D");}	# I_N_S_D;

insertps                :       INSERTPS xmm1 COMMA xmm2_m32 COMMA imm;
vinsertps               :       VINSERTPS xmm1 COMMA xmm2 COMMA xmm3_m32 COMMA imm
                        |       VINSERTPS xmm1 COMMA xmm2 COMMA xmm3_m32 COMMA imm;

int3			:       INT3	{ mal.output("0xCC");}	# I_N_T_3;
//int			:       INT imm{ mal.output("0xCD", CalculatorLibrary.cal($imm.text));}	# INT_imm;
int0			:       INT0 { mal.outputOPMODE("0xCE");}	# I_N_T_0;
int1			:       INT1 { mal.output("0xF1");}	# I_N_T_1;

invd			:       INVD { mal.output("0x0F","0x08");}	# I_N_V_D;

invlpg                  :       INVLPG m;

invpcid                 :       INVPCID r32 COMMA m128
                        |       INVPCID r64 COMMA m128;

iret                    :       IRET;
iretd                   :       IRETD;
iretq                   :       IRETQ;

ja                      :       JA rel8
                        |       JA rel16
                        |       JA rel32;
jae                     :       JAE rel8
                        |       JAE rel16
                        |       JAE rel32;
jb                      :       JB rel8
                        |       JB rel16
                        |       JB rel32;
jbe                     :       JBE rel8
                        |       JBE rel16
                        |       JBE rel32;
jc                      :       JC rel8
                        |       JC rel16
                        |       JC rel32;
jcxz                    :       JCXZ rel8;
jecxz                   :       JECXZ rel8;
jrcxz                   :       JRCXZ rel8;
je                      :       JE rel8
                        |       JE rel16
                        |       JE rel32;
jg                      :       JG rel8
                        |       JG rel16
                        |       JG rel32;
jge                     :       JGE rel8
                        |       JGE rel16
                        |       JGE rel32;
jl                      :       JL rel8
                        |       JL rel16
                        |       JL rel32;
jle                     :       JLE rel8
                        |       JLE rel16
                        |       JLE rel32;
jna                     :       JNA rel8
                        |       JNA rel16
                        |       JNA rel32;
jnae                    :       JNAE rel8
                        |       JNAE rel16
                        |       JNAE rel32;
jnb                     :       JNB rel8
                        |       JNB rel16
                        |       JNB rel32;
jnbe                    :       JNBE rel8
                        |       JNBE rel16
                        |       JNBE rel32;
jnc                     :       JNC rel8
                        |       JNC rel16
                        |       JNC rel32;
jne                     :       JNE rel8
                        |       JNE rel16
                        |       JNE rel32;
jng                     :       JNG rel8
                        |       JNG rel16
                        |       JNG rel32;
jnge                    :       JNGE rel8
                        |       JNGE rel16
                        |       JNGE rel32;
jnl                     :       JNL rel8
                        |       JNL rel16
                        |       JNL rel32;
jnle                    :       JNLE rel8
                        |       JNLE rel16
                        |       JNLE rel32;
jno                     :       JNO rel8
                        |       JNO rel16
                        |       JNO rel32;
jnp                     :       JNP rel8
                        |       JNP rel16
                        |       JNP rel32;
jns                     :       JNS rel8
                        |       JNS rel16
                        |       JNS rel32;
jnz                     :       JNZ rel8
                        |       JNZ rel16
                        |       JNZ rel32;
jo                      :       JO rel8
                        |       JO rel16
                        |       JO rel32;
jp                      :       JP rel8
                        |       JP rel16
                        |       JP rel32;
jpe                     :       JPE rel8
                        |       JPE rel16
                        |       JPE rel32;
jpo                     :       JPO rel8
                        |       JPO rel16
                        |       JPO rel32;
js                      :       JS rel8
                        |       JS rel16
                        |       JS rel32;
jz                      :       JZ rel8
                        |       JZ rel16 //OCCURS TWICE
                        |       JZ rel32;//OCCURS TWICE

jmp                     :       JMP rel8
                        |       JMP rel16
                        |       JMP rel32
                        |       JMP r_m16
                        |       JMP r_m32
                        |       JMP r_m64
                        |       JMP ptr16COLON16
                        |       JMP ptr16COLON32
                        |       JMP m16COLON16
                        |       JMP m16COLON32
                        |       JMP m16COLON64;

kaddw                   :       KADDW k1 COMMA k2 COMMA k3;
kaddb                   :       KADDB k1 COMMA k2 COMMA k3;
kaddq                   :       KADDQ k1 COMMA k2 COMMA k3;
kaddd                   :       KADDD k1 COMMA k2 COMMA k3;

kandw                   :       KANDW k1 COMMA k2 COMMA k3;
kandb                   :       KANDB k1 COMMA k2 COMMA k3;
kandq                   :       KANDQ k1 COMMA k2 COMMA k3;
kandd                   :       KANDD k1 COMMA k2 COMMA k3;

kandnw                  :       KANDNW k1 COMMA k2 COMMA k3;
kandnb                  :       KANDNB k1 COMMA k2 COMMA k3;
kandnq                  :       KANDNQ k1 COMMA k2 COMMA k3;
kandnd                  :       KANDND k1 COMMA k2 COMMA k3;

kmovw                   :       KMOVW k1 COMMA k2_m16
                        |       KMOVW m16 COMMA k1
                        |       KMOVW k1 COMMA r32
                        |       KMOVW r32 COMMA k1;
kmovb                   :       KMOVB k1 COMMA k2_m8
                        |       KMOVB m8 COMMA k1
                        |       KMOVB k1 COMMA r32
                        |       KMOVB r32 COMMA k1;
kmovq                   :       KMOVQ k1 COMMA k2_m64
                        |       KMOVQ m64 COMMA k1
                        |       KMOVQ k1 COMMA r32
                        |       KMOVQ r32 COMMA k1;
kmovd                   :       KMOVD k1 COMMA k2_m32
                        |       KMOVD m32 COMMA k1
                        |       KMOVD k1 COMMA r32
                        |       KMOVD r32 COMMA k1;

knotw                   :       KNOTW k1 COMMA k2;
knotb                   :       KNOTB k1 COMMA k2;
knotq                   :       KNOTQ k1 COMMA k2;
knotd                   :       KNOTD k1 COMMA k2;

korw                    :       KORW k1 COMMA k2 COMMA k3;
korb                    :       KORB k1 COMMA k2 COMMA k3;
korq                    :       KORQ k1 COMMA k2 COMMA k3;
kord                    :       KORD k1 COMMA k2 COMMA k3;

kortestw                :       KORTESTW k1 COMMA k2;
kortestb                :       KORTESTB k1 COMMA k2;
kortestq                :       KORTESTQ k1 COMMA k2;
kortestd                :       KORTESTD k1 COMMA k2;

kshiftlw                :       KSHIFTLW k1 COMMA k2 COMMA imm;
kshiftlb                :       KSHIFTLB k1 COMMA k2 COMMA imm;
kshiftlq                :       KSHIFTLQ k1 COMMA k2 COMMA imm;
kshiftld                :       KSHIFTLD k1 COMMA k2 COMMA imm;

kshiftrw                :       KSHIFTRW k1 COMMA k2 COMMA imm;
kshiftrb                :       KSHIFTRB k1 COMMA k2 COMMA imm;
kshiftrq                :       KSHIFTRQ k1 COMMA k2 COMMA imm;
kshiftrd                :       KSHIFTRD k1 COMMA k2 COMMA imm;

ktestw                  :       KTESTW k1 COMMA k2;
ktestb                  :       KTESTB k1 COMMA k2;
ktestq                  :       KTESTQ k1 COMMA k2;
ktestd                  :       KTESTD k1 COMMA k2;

kunpckbw                :       KUNPCKBW k1 COMMA k2 COMMA k3;
kunpckbd                :       KUNPCKBD k1 COMMA k2 COMMA k3;
kunpckbq                :       KUNPCKBQ k1 COMMA k2 COMMA k3;

kxnorw                  :       KXNORW k1 COMMA k2 COMMA k3;
kxnorb                  :       KXNORB k1 COMMA k2 COMMA k3;
kxnorq                  :       KXNORQ k1 COMMA k2 COMMA k3;
kxnord                  :       KXNORD k1 COMMA k2 COMMA k3;

kxorw                  :       KXORW k1 COMMA k2 COMMA k3;
kxorb                  :       KXORB k1 COMMA k2 COMMA k3;
kxorq                  :       KXORQ k1 COMMA k2 COMMA k3;
kxord                  :       KXORD k1 COMMA k2 COMMA k3;

lahf			:       LAHF { mal.output("0x9F");}	# L_A_H_F;

lar                     :       LAR r16 COMMA r16_m16
                        |       LAR reg COMMA r32_m16;

lddqu                   :       LDDQU xmm1 COMMA mem;
vlddqu                  :       VLDDQU xmm1 COMMA m128
                        |       VLDDQU ymm1 COMMA m256;

ldmxcsr                 :       LDMXCSR m32;
vldmxcsr                :       VLDMXCSR m32;

lds                     :       LDS r16 COMMA m16COLON16
                        |       LDS r32 COMMA m16COLON32;
lss                     :       LSS r16 COMMA m16COLON16
                        |       LSS r32 COMMA m16COLON32
                        |       LSS r64 COMMA m16COLON64;
les                     :       LES r16 COMMA m16COLON16
                        |       LES r32 COMMA m16COLON32;
lfs                     :       LFS r16 COMMA m16COLON16
                        |       LFS r32 COMMA m16COLON32
                        |       LFS r64 COMMA m16COLON64;
lgs                     :       LGS r16 COMMA m16COLON16
                        |       LGS r32 COMMA m16COLON32
                        |       LGS r64 COMMA m16COLON64;

lea                     :       LEA r16 COMMA m
                        |       LEA r32 COMMA m
                        |       LEA r64 COMMA m;

leave			:       LEAVE { mal.output("0xC9");}	# L_E_A_V_E;//three combined in one

lfence                  :       LFENCE;

lgdt                    :       LGDT m16AND32
                        |       LGDT m16AND64;
lidt                    :       LIDT m16AND32
                        |       LIDT m16AND64;

lldt                    :       LLDT r_m16;

lmsw                    :       LMSW r_m16;

lock			:       LOCK { mal.output("0xF0");}	# L_O_C_K;

lods                    :       LODS m8
                        |       LODS m16
                        |       LODS m32
                        |       LODS m64;
lodsb                   :       LODSB;
lodsw                   :       LODSW;
lodsd                   :       LODSD;
lodsq                   :       LODSQ;

loop                    :       LOOP rel8;
loope                   :       LOOPE rel8;
loopne                  :       LOOPNE rel8;

lsl                     :       LSL r16 COMMA r16_m16
                        |       LSL r32 COMMA r32_m16
                        |       LSL r64 COMMA r32_m16;

ltr                     :       LTR r_m16;

lzcnt                   :       LZCNT r16 COMMA r_m16
                        |       LZCNT r32 COMMA r_m32
                        |       LZCNT r64 COMMA r_m64;

maskmovdqu              :       MASKMOVDQU xmm1 COMMA xmm2;
vmaskmovdqu             :       VMASKMOVDQU xmm1 COMMA xmm2;

maskmovq                :       MASKMOVQ mm1 COMMA mm2;

maxpd                   :       MAXPD xmm1 COMMA xmm2_m128;
vmaxpd                  :       VMAXPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VMAXPD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VMAXPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VMAXPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VMAXPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{sae};

maxps                   :       MAXPS xmm1 COMMA xmm2_m128;
vmaxps                  :       VMAXPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VMAXPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VMAXPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VMAXPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VMAXPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{sae};

maxsd                   :       MAXSD xmm1 COMMA xmm2_m64;
vmaxsd                  :       VMAXSD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VMAXSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{sae};

maxss                   :       MAXSS xmm1 COMMA xmm2_m32;
vmaxss                  :       VMAXSS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VMAXSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{sae};

mfence                  :       MFENCE;

minpd                   :       MINPD xmm1 COMMA xmm2_m128;
vminpd                  :       VMINPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VMINPD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VMINPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VMINPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VMINPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{sae};

minps                   :       MINPS xmm1 COMMA xmm2_m128;
vminps                  :       VMINPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VMINPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VMINPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VMINPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VMINPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{sae};

minsd                   :       MINSD xmm1 COMMA xmm2_m64;
vminsd                  :       VMINSD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VMINSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{sae};

minss                   :       MINSS xmm1 COMMA xmm2_m32;
vminss                  :       VMINSS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VMINSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{sae};

monitor			:       MONITOR                 { mal.output("0x0F","0x01","0xC8");}	# M_O_N_I_T_O_R;

mov                     :       MOV r_m8 COMMA r8
                        |       MOV r_m16 COMMA r16
                        |       MOV r_m32 COMMA r32
                        |       MOV r_m64 COMMA r64
                        |       MOV r8 COMMA r_m8
                        |       MOV r16 COMMA r_m16
                        |       MOV r32 COMMA r_m32
                        |       MOV r64 COMMA r_m64
                        |       MOV r_m16 COMMA Sreg
                        |       MOV r16_r32_m16 COMMA Sreg
                        |       MOV r64_m16 COMMA Sreg
                        |       MOV Sreg COMMA r_m16
                        |       MOV Sreg COMMA r_m64
                        |       MOV AL COMMA moffs8
                        |       MOV AX COMMA moffs16
                        |       MOV EAX COMMA moffs32
                        |       MOV RAX COMMA moffs64
                        |       MOV moffs8 COMMA AL
                        |       MOV moffs16 COMMA AX
                        |       MOV moffs32 COMMA EAX
                        |       MOV moffs64 COMMA RAX
                        |       MOV r8 COMMA imm
                        |       MOV r16 COMMA imm
                        |       MOV r32 COMMA imm
                        |       MOV r64 COMMA imm64
                        |       MOV r_m8 COMMA imm
                        |       MOV r_m16 COMMA imm
                        |       MOV r_m32 COMMA imm
                        |       MOV r_m64 COMMA imm
                        |       MOV r32 COMMA CR0TOCR7
                        |       MOV r64 COMMA CR0TOCR7
                        |       MOV r64 COMMA CR8
                        |       MOV CR0TOCR7 COMMA r32
                        |       MOV CR0TOCR7 COMMA r64
                        |       MOV CR8 COMMA r64
                        |       MOV r32 COMMA DR0TODR7
                        |       MOV r64 COMMA DR0TODR7
                        |       MOV DR0TODR7 COMMA r32
                        |       MOV DR0TODR7 COMMA r64;
movapd                  :       MOVAPD xmm1 COMMA xmm2_m128
                        |       MOVAPD xmm2_m128 COMMA xmm1;
vmovapd                 :       VMOVAPD xmm1 COMMA xmm2_m128
                        |       VMOVAPD xmm2_m128 COMMA xmm1
                        |       VMOVAPD ymm1 COMMA ymm2_m256
                        |       VMOVAPD ymm2_m256 COMMA ymm1
                        |       VMOVAPD xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVAPD ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVAPD zmm1 {k1}{z} COMMA zmm2_m512
                        |       VMOVAPD xmm2_m128 {k1}{z} COMMA xmm1
                        |       VMOVAPD ymm2_m256 {k1}{z} COMMA ymm1
                        |       VMOVAPD zmm2_m512 {k1}{z} COMMA zmm1;

movaps                  :       MOVAPS xmm1 COMMA xmm2_m128
                        |       MOVAPS xmm2_m128 COMMA xmm1;
vmovaps                 :       VMOVAPS xmm1 COMMA xmm2_m128
                        |       VMOVAPS xmm2_m128 COMMA xmm1
                        |       VMOVAPS ymm1 COMMA ymm2_m256
                        |       VMOVAPS ymm2_m256 COMMA ymm1
                        |       VMOVAPS xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVAPS ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVAPS zmm1 {k1}{z} COMMA zmm2_m512
                        |       VMOVAPS xmm2_m128 {k1}{z} COMMA xmm1
                        |       VMOVAPS ymm2_m256 {k1}{z} COMMA ymm1
                        |       VMOVAPS zmm2_m512 {k1}{z} COMMA zmm1;

movbe                   :       MOVBE r16 COMMA m16
                        |       MOVBE r32 COMMA m32
                        |       MOVBE r64 COMMA m64
                        |       MOVBE m16 COMMA r16
                        |       MOVBE m32 COMMA r32
                        |       MOVBE m64 COMMA r64;

movd                    :       MOVD mm COMMA r_m32
                        |       MOVD r_m32 COMMA mm
                        |       MOVD xmm COMMA r_m32
                        |       MOVD r_m32 COMMA xmm;
movq                    :       MOVQ mm COMMA r_m64
                        |       MOVQ r_m64 COMMA mm
                        |       MOVQ xmm COMMA r_m64
                        |       MOVQ r_m64 COMMA xmm;
vmovd                   :       VMOVD xmm1 COMMA r32_m32
                        |       VMOVD r32_m32 COMMA xmm1;
vmovq                   :       VMOVQ xmm1 COMMA r64_m64
                        |       VMOVQ r64_m64 COMMA xmm1;

movddup                 :       MOVDDUP xmm1 COMMA xmm2_m64;
vmovddup                :       VMOVDDUP xmm1 COMMA xmm2_m64
                        |       VMOVDDUP ymm1 COMMA ymm2_m64
                        |       VMOVDDUP xmm1 {k1}{z} COMMA xmm2_m64
                        |       VMOVDDUP ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVDDUP zmm1 {k1}{z} COMMA zmm2_m512;

movdiri                 :       MOVDIRI m32 COMMA r32
                        |       MOVDIRI m64 COMMA r64;

movdir64b               :       MOVDIR64B r16_r32_r64 COMMA m512;

movdqa                  :       MOVDQA xmm1 COMMA xmm2_m128
                        |       MOVDQA xmm2_m128 COMMA xmm1;
vmovdqa                 :       VMOVDQA xmm1 COMMA xmm2_m128
                        |       VMOVDQA xmm2_m128 COMMA xmm1
                        |       VMOVDQA ymm1 COMMA ymm2_m256
                        |       VMOVDQA ymm2_m256 COMMA ymm1;
vmovdqa32               :       VMOVDQA32 xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVDQA32 ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVDQA32 zmm1 {k1}{z} COMMA zmm2_m512
                        |       VMOVDQA32 xmm2_m128 {k1}{z} COMMA xmm1
                        |       VMOVDQA32 ymm2_m256 {k1}{z} COMMA ymm1
                        |       VMOVDQA32 zmm2_m512 {k1}{z} COMMA zmm1;
vmovdqa64               :       VMOVDQA64 xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVDQA64 ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVDQA64 zmm1 {k1}{z} COMMA zmm2_m512
                        |       VMOVDQA64 xmm2_m128 {k1}{z} COMMA xmm1
                        |       VMOVDQA64 ymm2_m256 {k1}{z} COMMA ymm1
                        |       VMOVDQA64 zmm2_m512 {k1}{z} COMMA zmm1;

movdqu                  :       MOVDQU xmm1 COMMA xmm2_m128
                        |       MOVDQU xmm2_m128 COMMA xmm1;
vmovdqu                 :       VMOVDQU xmm1 COMMA xmm2_m128
                        |       VMOVDQU xmm2_m128 COMMA xmm1
                        |       VMOVDQU ymm1 COMMA ymm2_m256
                        |       VMOVDQU ymm2_m256 COMMA ymm1;
vmovdqu8                :       VMOVDQU8 xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVDQU8 ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVDQU8 zmm1 {k1}{z} COMMA zmm2_m512
                        |       VMOVDQU8 xmm2_m128 {k1}{z} COMMA xmm1
                        |       VMOVDQU8 ymm2_m256 {k1}{z} COMMA ymm1
                        |       VMOVDQU8 zmm2_m512 {k1}{z} COMMA zmm1;
vmovdqu16               :       VMOVDQU16 xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVDQU16 ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVDQU16 zmm1 {k1}{z} COMMA zmm2_m512
                        |       VMOVDQU16 xmm2_m128 {k1}{z} COMMA xmm1
                        |       VMOVDQU16 ymm2_m256 {k1}{z} COMMA ymm1
                        |       VMOVDQU16 zmm2_m512 {k1}{z} COMMA zmm1;
vmovdqu32               :       VMOVDQU32 xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVDQU32 ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVDQU32 zmm1 {k1}{z} COMMA zmm2_m512
                        |       VMOVDQU32 xmm2_m128 {k1}{z} COMMA xmm1
                        |       VMOVDQU32 ymm2_m256 {k1}{z} COMMA ymm1
                        |       VMOVDQU32 zmm2_m512 {k1}{z} COMMA zmm1;
vmovdqu64               :       VMOVDQU64 xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVDQU64 ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVDQU64 zmm1 {k1}{z} COMMA zmm2_m512
                        |       VMOVDQU64 xmm2_m128 {k1}{z} COMMA xmm1
                        |       VMOVDQU64 ymm2_m256 {k1}{z} COMMA ymm1
                        |       VMOVDQU64 zmm2_m512 {k1}{z} COMMA zmm1;

movdq2q                 :       MOVDQ2Q mm COMMA xmm;

movhlps                 :       MOVHLPS xmm1 COMMA xmm2;
vmovhlps                :       VMOVHLPS xmm1 COMMA xmm2 COMMA xmm3;

movhpd                  :       MOVHPD xmm1 COMMA m64
                        |       MOVHPD m64 COMMA xmm1;
vmovhpd                 :       VMOVHPD xmm2 COMMA xmm1 COMMA m64
                        |       VMOVHPD m64 COMMA xmm1;

movhps                  :       MOVHPS xmm1 COMMA m64
                        |       MOVHPS m64 COMMA xmm1;
vmovhps                 :       VMOVHPS xmm2 COMMA xmm1 COMMA m64
                        |       VMOVHPS m64 COMMA xmm1;

movlhps                 :       MOVLHPS xmm1 COMMA xmm2;
vmovlhps                :       VMOVLHPS xmm1 COMMA xmm2 COMMA xmm3;

movlpd                  :       MOVLPD xmm1 COMMA m64
                        |       MOVLPD m64 COMMA xmm1;
vmovlpd                 :       VMOVLPD xmm2 COMMA xmm1 COMMA m64
                        |       VMOVLPD m64 COMMA xmm1;

movlps                  :       MOVLPS xmm1 COMMA m64
                        |       MOVLPS m64 COMMA xmm1;
vmovlps                 :       VMOVLPS xmm2 COMMA xmm1 COMMA m64
                        |       VMOVLPS m64 COMMA xmm1;

movmskpd                :       MOVMSKPD reg COMMA xmm;
vmovmskpd               :       VMOVMSKPD reg COMMA xmm2
                        |       VMOVMSKPD reg COMMA ymm2;

movmskps                :       MOVMSKPS reg COMMA xmm;
vmovmskps               :       VMOVMSKPS reg COMMA xmm2
                        |       VMOVMSKPS reg COMMA ymm2;

movntdqa                :       MOVNTDQA xmm1 COMMA m128;
vmovntdqa               :       VMOVNTDQA xmm1 COMMA m128
                        |       VMOVNTDQA ymm1 COMMA m256
                        |       VMOVNTDQA zmm1 COMMA m512;

movntdq                 :       MOVNTDQ m128 COMMA xmm1;
vmovntdq                :       VMOVNTDQ m128 COMMA xmm1
                        |       VMOVNTDQ m256 COMMA ymm1
                        |       VMOVNTDQ m512 COMMA zmm1;

movnti                  :       MOVNTI m32 COMMA r32
                        |       MOVNTI m64 COMMA r64;

movntpd                 :       MOVNTPD m128 COMMA xmm1;
vmovntpd                :       VMOVNTPD m128 COMMA xmm1
                        |       VMOVNTPD m256 COMMA ymm1
                        |       VMOVNTPD m512 COMMA zmm1;

movntps                 :       MOVNTPS m128 COMMA xmm1;
vmovntps                :       VMOVNTPS m128 COMMA xmm1
                        |       VMOVNTPS m256 COMMA ymm1
                        |       VMOVNTPS m512 COMMA zmm1;

movntq                  :       MOVNTQ m64 COMMA mm;

movq                    :       MOVQ mm COMMA mm_m64
                        |       MOVQ mm_m64 COMMA mm
                        |       MOVQ xmm1 COMMA xmm2_m64
                        |       MOVQ xmm2_m64 COMMA xmm1;
vmovq                   :       VMOVQ xmm1 COMMA xmm2_m64
                        |       VMOVQ xmm1_m64 COMMA xmm2;

movq2dq                 :       MOVQ2DQ xmm COMMA mm;

movs                    :       MOVS m8 COMMA m8
                        |       MOVS m16 COMMA m16
                        |       MOVS m32 COMMA m32
                        |       MOVS m64 COMMA m64;
movsb                   :       MOVSB;
movsw                   :       MOVSW;
movsd                   :       MOVSD;
movsq                   :       MOVSQ;

movsd                   :       MOVSD xmm1 COMMA xmm2
                        |       MOVSD xmm1 COMMA m64
                        |       MOVSD xmm1_m64 COMMA xmm2;
vmovsd                  :       VMOVSD xmm1 COMMA xmm2 COMMA xmm3
                        |       VMOVSD xmm1 COMMA m64
                        |       VMOVSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3
                        |       VMOVSD xmm1 {k1}{z} COMMA m64
                        |       VMOVSD m64 {k1} COMMA xmm1;

movshdup                :       MOVSHDUP xmm1 COMMA xmm2_m128;
vmovshdup               :       VMOVSHDUP xmm1 COMMA xmm2_m128
                        |       VMOVSHDUP ymm1 COMMA ymm2_m256
                        |       VMOVSHDUP xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVSHDUP ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVSHDUP zmm1 {k1}{z} COMMA zmm2_m512;

movsldup                :       MOVSLDUP xmm1 COMMA xmm2_m128;
vmovsldup               :       VMOVSLDUP xmm1 COMMA xmm2_m128
                        |       VMOVSLDUP ymm1 COMMA ymm2_m256
                        |       VMOVSLDUP xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVSLDUP ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVSLDUP zmm1 {k1}{z} COMMA zmm2_m512;

movss                   :       MOVSS xmm1 COMMA xmm2
                        |       MOVSS xmm1 COMMA m32
                        |       MOVSS xmm2_m32 COMMA xmm1;
vmovss                  :       VMOVSS xmm1 COMMA xmm2 COMMA xmm3
                        |       VMOVSS xmm1 COMMA m32
                        |       VMOVSS m32 COMMA xmm1
                        |       VMOVSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3
                        |       VMOVSS xmm1 {k1}{z} COMMA m32
                        |       VMOVSS m32 {k1} COMMA xmm1;

movsx                   :       MOVSX r16 COMMA r_m8
                        |       MOVSX r32 COMMA r_m8
                        |       MOVSX r64 COMMA r_m8
                        |       MOVSX r32 COMMA r_m16
                        |       MOVSX r64 COMMA r_m16;
movsxd                  :       MOVSXD r16 COMMA r_m16
                        |       MOVSXD r32 COMMA r_m32
                        |       MOVSXD r64 COMMA r_m32;

movupd                  :       MOVUPD xmm1 COMMA xmm2_m128
                        |       MOVUPD xmm2_m128 COMMA xmm1;
vmovupd                 :       VMOVUPD xmm1 COMMA xmm2_m128
                        |       VMOVUPD xmm2_m128 COMMA xmm1
                        |       VMOVUPD ymm1 COMMA ymm2_m256
                        |       VMOVUPD ymm2_m256 COMMA ymm1
                        |       VMOVUPD xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVUPD xmm2_m128 {k1}{z} COMMA xmm1
                        |       VMOVUPD ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVUPD ymm2_m256 {k1}{z} COMMA ymm1
                        |       VMOVUPD zmm1 {k1}{z} COMMA zmm2_m512
                        |       VMOVUPD zmm2_m512 {k1}{z} COMMA zmm1;

movups                  :       MOVUPS xmm1 COMMA xmm2_m128
                        |       MOVUPS xmm2_m128 COMMA xmm1;
vmovups                 :       VMOVUPS xmm1 COMMA xmm2_m128
                        |       VMOVUPS xmm2_m128 COMMA xmm1
                        |       VMOVUPS ymm1 COMMA ymm2_m256
                        |       VMOVUPS ymm2_m256 COMMA ymm1
                        |       VMOVUPS xmm1 {k1}{z} COMMA xmm2_m128
                        |       VMOVUPS xmm2_m128 {k1}{z} COMMA xmm1
                        |       VMOVUPS ymm1 {k1}{z} COMMA ymm2_m256
                        |       VMOVUPS ymm2_m256 {k1}{z} COMMA ymm1
                        |       VMOVUPS zmm1 {k1}{z} COMMA zmm2_m512
                        |       VMOVUPS zmm2_m512 {k1}{z} COMMA zmm1;

movzx                   :       MOVZX r16 COMMA r_m8
                        |       MOVZX r32 COMMA r_m8
                        |       MOVZX r64 COMMA r_m8
                        |       MOVZX r32 COMMA r_m16
                        |       MOVZX r64 COMMA r_m16;

mpsadbw                 :       MPSADBW xmm1 COMMA xmm2_m128 COMMA imm;
vmpsadbw                :       VMPSADBW xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VMPSADBW ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm;

mul                     :       MUL r_m8
                        |       MUL r_m16
                        |       MUL r_m32
                        |       MUL r_m64;

mulpd                   :       MULPD xmm1 COMMA xmm2_m128;
vmulpd                  :       VMULPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VMULPD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VMULPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VMULPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VMULPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};

mulps                   :       MULPS xmm1 COMMA xmm2_m128;
vmulps                  :       VMULPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VMULPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VMULPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VMULPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VMULPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};

mulsd                   :       MULSD xmm1 COMMA xmm2_m64;
vmulsd                  :       VMULSD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VMULSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64 {er};

mulss                   :       MULSS xmm1 COMMA xmm2_m32;
vmulss                  :       VMULSS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VMULSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32 {er};

mulx                    :       MULX r32a COMMA r32b COMMA r_m32
                        |       MULX r64a COMMA r64b COMMA r_m64;

mwait			:       MWAIT                { mal.output("0x0F","0x01","0xC9");}	# M_W_A_I_T;

neg                     :       NEG r_m8
                        |       NEG r_m16
                        |       NEG r_m32
                        |       NEG r_m64;

nop                     :       NOP r_m8
                        |       NOP r_m16
                        |       NOP r_m32;

not                     :       NOT r_m8
                        |       NOT r_m16
                        |       NOT r_m32
                        |       NOT r_m64;

or                      :       OR AL COMMA imm
                        |       OR AX COMMA imm
                        |       OR EAX COMMA imm
                        |       OR RAX COMMA imm
                        |       OR r_m8 COMMA imm
                        |       OR r_m16 COMMA imm
                        |       OR r_m32 COMMA imm
                        |       OR r_m64 COMMA imm
                        |       OR r_m16 COMMA imm
                        |       OR r_m32 COMMA imm
                        |       OR r_m64 COMMA imm
                        |       OR r_m8 COMMA r8
                        |       OR r_m16 COMMA r16
                        |       OR r_m32 COMMA r32
                        |       OR r_m64 COMMA r64
                        |       OR r8 COMMA r_m8
                        |       OR r16 COMMA r_m16
                        |       OR r32 COMMA r_m32
                        |       OR r64 COMMA r_m64;
orpd                    :       ORPD xmm1 COMMA xmm2_m128;
vorpd                   :       VORPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VORPD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VORPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VORPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VORPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

orps                    :       ORPS xmm1 COMMA xmm2_m128;
vorps                   :       VORPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VORPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VORPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VORPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VORPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;

out			:       OUT imm COMMA AL       { mal.output("0xE6", CalculatorLibrary.cal($imm.text));}	# OUT_imm__AL
			|       OUT imm COMMA AX       { mal.output("0xE7", CalculatorLibrary.cal($imm.text));}	# OUT_imm__AX
			|       OUT imm COMMA EAX      { mal.output("0xE7", CalculatorLibrary.cal($imm.text));}	# OUT_imm__EAX
			|       OUT DX COMMA AL         { mal.output("0xEE");}                   # OUT_DX__AL
			|	OUT DX COMMA AX 	{ mal.output("0xEF");}           	# OUT_DX__AX
			|	OUT DX COMMA EAX	{ mal.output("0xEF");}           	# OUT_DX__EAX
                        ;

outs			:      OUTS DX COMMA m8         { mal.output("0x6E");}	# OUTS_DX__M8
			|      OUTS DX COMMA m16	{ mal.output("0x6F");}	# OUTS_DX__M16
			|      OUTS DX COMMA m32	{ mal.output("0x6F");}	# OUTS_DX__M32;
outsb			:      OUTSB                    { mal.output("0x6E");}	# O_U_T_S_B;
outsw			:      OUTSW                  	{ mal.output("0x6F");}	# O_U_T_S_W;
outsd			:      OUTSD            	{ mal.output("0x6F");}	# O_U_T_S_D;

pabsb                   :       PABSB mm1 COMMA mm2_m64
                        |       PABSB xmm1 COMMA xmm2_m128;
pabsw                   :       PABSW mm1 COMMA mm2_m64
                        |       PABSW xmm1 COMMA xmm2_m128;
pabsd                   :       PABSD mm1 COMMA mm2_m64
                        |       PABSD xmm1 COMMA xmm2_m128;
vpabsb                  :       VPABSB xmm1 COMMA xmm2_m128
                        |       VPABSB ymm1 COMMA ymm2_m256
                        |       VPABSB xmm1 {k1}{z} COMMA xmm2_m128
                        |       VPABSB ymm1 {k1}{z} COMMA ymm2_m256
                        |       VPABSB zmm1 {k1}{z} COMMA zmm2_m512;
vpabsw                  :       VPABSW xmm1 COMMA xmm2_m128
                        |       VPABSW ymm1 COMMA ymm2_m256
                        |       VPABSW xmm1 {k1}{z} COMMA xmm2_m128
                        |       VPABSW ymm1 {k1}{z} COMMA ymm2_m256
                        |       VPABSW zmm1 {k1}{z} COMMA zmm2_m512;
vpabsd                  :       VPABSD xmm1 COMMA xmm2_m128
                        |       VPABSD ymm1 COMMA ymm2_m256
                        |       VPABSD xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VPABSD ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VPABSD zmm1 {k1}{z} COMMA zmm2_m512_m32bcst;
vpabsq                  :       VPABSQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VPABSQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VPABSQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst;

packsswb                :       PACKSSWB mm1 COMMA mm2_m64
                        |       PACKSSWB xmm1 COMMA xmm2_m128;
packssdw                :       PACKSSDW mm1 COMMA mm2_m64
                        |       PACKSSDW xmm1 COMMA xmm2_m128;
vpacksswb               :       VPACKSSWB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPACKSSWB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPACKSSWB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPACKSSWB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPACKSSWB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpackssdw               :       VPACKSSDW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPACKSSDW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPACKSSDW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPACKSSDW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPACKSSDW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;

packusdw                :       PACKUSDW xmm1 COMMA xmm2_m128;
vpackusdw               :       VPACKUSDW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPACKUSDW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPACKUSDW xmm1{k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPACKUSDW ymm1{k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPACKUSDW zmm1{k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;

packuswb                :       PACKUSWB mm COMMA mm_m64
                        |       PACKUSWB xmm1 COMMA xmm2_m128;
vpackuswb               :       VPACKUSWB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPACKUSWB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPACKUSWB xmm1{k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPACKUSWB ymm1{k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPACKUSWB zmm1{k1}{z} COMMA zmm2 COMMA zmm3_m512;

paddb                   :       PADDB mm COMMA mm_m64
                        |       PADDB xmm1 COMMA xmm2_m128;
paddw                   :       PADDW mm COMMA mm_m64
                        |       PADDW xmm1 COMMA xmm2_m128;
paddd                   :       PADDD mm COMMA mm_m64
                        |       PADDD xmm1 COMMA xmm2_m128;
paddq                   :       PADDQ mm COMMA mm_m64
                        |       PADDQ xmm1 COMMA xmm2_m128;
vpaddb                  :       VPADDB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPADDB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPADDB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPADDB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPADDB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpaddw                  :       VPADDW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPADDW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPADDW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPADDW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPADDW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpaddd                  :       VPADDD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPADDD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPADDD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPADDD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPADDD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpaddq                  :       VPADDQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPADDQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPADDQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPADDQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPADDQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

paddsb                  :       PADDSB mm COMMA mm_m64
                        |       PADDSB xmm1 COMMA xmm2_m128;
paddsw                  :       PADDSW mm COMMA mm_m64
                        |       PADDSW xmm1 COMMA xmm2_m128;
vpaddsb                 :       VPADDSB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPADDSB ymm1 COMMA ymm2 COMMA ymm3_m256;
                        |       VPADDSB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPADDSB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPADDSB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpaddsw                 :       VPADDSW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPADDSW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPADDSW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPADDSW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPADDSW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

paddusb                 :       PADDUSB mm COMMA mm_m64
                        |       PADDUSB xmm1 COMMA xmm2_m128;
paddusw                 :       PADDUSW mm COMMA mm_m64
                        |       PADDUSW xmm1 COMMA xmm2_m128;
vpaddusb                :       VPADDUSB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPADDUSB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPADDUSB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPADDUSB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPADDUSB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpaddusw                :       VPADDUSW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPADDUSW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPADDUSW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPADDUSW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPADDUSW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

palignr                 :       PALIGNR mm1 COMMA mm2_m64 COMMA imm
                        |       PALIGNR xmm1 COMMA xmm2_m128 COMMA imm;
vpalignr                :       VPALIGNR xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VPALIGNR xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VPALIGNR xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VPALIGNR ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256 COMMA imm
                        |       VPALIGNR zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512 COMMA imm;

pand                    :       PAND mm COMMA mm_m64
                        |       PAND xmm1 COMMA xmm2_m128;
vpand                   :       VPAND xmm1 COMMA xmm2 COMMA xmm2_m128
                        |       VPAND ymm1 COMMA ymm2 COMMA ymm3_m256;
vpandd                  :       VPANDD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPANDD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPANDD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpandq                  :       VPANDQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPANDQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPANDQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

pandn                   :       PANDN mm COMMA mm_m64
                        |       PANDN xmm1 COMMA xmm2_m128;
vpandn                  :       VPANDN xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPANDN ymm1 COMMA ymm2 COMMA ymm3_m256;
vpandnd                 :       VPANDND xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPANDND ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPANDND zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpandnq                 :       VPANDNQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPANDNQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPANDNQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

pause                   :       PAUSE;

pavgb                   :       PAVGB mm1 COMMA mm2_m64
                        |       PAVGB xmm1 COMMA xmm2_m128;
pavgw                   :       PAVGW mm1 COMMA mm2_m64
                        |       PAVGW xmm1 COMMA xmm2_m128;
vpavgb                  :       VPAVGB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPAVGB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPAVGB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPAVGB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPAVGB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpavgw                  :       VPAVGW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPAVGW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPAVGW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPAVGW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPAVGW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

pblendvb                :       PBLENDVB xmm1 COMMA xmm2_m128 COMMA <XMM0>;
vpblendvb               :       VPBLENDVB xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA xmm4
                        |       VPBLENDVB ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA ymm4;

pblendw                 :       PBLENDW xmm1 COMMA xmm2_m128 COMMA imm;
vpblendw                :       VPBLENDW xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VPBLENDW ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm;

pclmulqdq               :       PCLMULQDQ xmm1 COMMA xmm2_m128 COMMA imm;
vpclmulqdq              :       VPCLMULQDQ xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm;

pcmpeqb                 :       PCMPEQB mm COMMA mm_m64
                        |       PCMPEQB xmm1 COMMA xmm2_m128;
pcmpeqw                 :       PCMPEQW mm COMMA mm_m64
                        |       PCMPEQW xmm1 COMMA xmm2_m128;
pcmpeqd                 :       PCMPEQD mm COMMA mm_m64
                        |       PCMPEQD xmm1 COMMA xmm2_m128;
vpcmpeqb                :       VPCMPEQB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPEQB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPCMPEQB k1 {k2} COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPEQB k1 {k2} COMMA ymm2 COMMA ymm3_m256
                        |       VPCMPEQB k1 {k2} COMMA zmm2 COMMA zmm3_m512;
vpcmpeqw                :       VPCMPEQW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPEQW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPCMPEQW k1 {k2} COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPEQW k1 {k2} COMMA ymm2 COMMA ymm3_m256
                        |       VPCMPEQW k1 {k2} COMMA zmm2 COMMA zmm3_m512;
vpcmpeqd                :       VPCMPEQD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPEQD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPCMPEQD k1 {k2} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPCMPEQD k1 {k2} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPCMPEQD k1 {k2} COMMA zmm2 COMMA zmm3_m512_m32bcst;

pcmpeqq                 :       PCMPEQQ xmm1 COMMA xmm2_m128;
vpcmpeqq                :       VPCMPEQQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPEQQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPCMPEQQ k1 {k2} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPCMPEQQ k1 {k2} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPCMPEQQ k1 {k2} COMMA zmm2 COMMA zmm3_m512_m64bcst;

pcmpestri               :       PCMPESTRI xmm1 COMMA xmm2_m128 COMMA imm;
vpcmpestri              :       VPCMPESTRI xmm1 COMMA xmm2_m128 COMMA imm;

pcmpestrm               :       PCMPESTRM xmm1 COMMA xmm2_m128 COMMA imm;
vpcmpestrm              :       VPCMPESTRM xmm1 COMMA xmm2_m128 COMMA imm;

pcmpgtb                 :       PCMPGTB mm COMMA mm_m64
                        |       PCMPGTB xmm1 COMMA xmm2_m128;
pcmpgtw                 :       PCMPGTW mm COMMA mm_m64
                        |       PCMPGTW xmm1 COMMA xmm2_m128;
pcmpgtd                 :       PCMPGTD mm COMMA mm_m64
                        |       PCMPGTD xmm1 COMMA xmm2_m128;
vpcmpgtb                :       VPCMPGTB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPGTB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPCMPGTB k1 {k2} COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPGTB k1 {k2} COMMA ymm2 COMMA ymm3_m256;
vpcmpgtw                :       VPCMPGTW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPGTW ymm1 COMMA ymm2 COMMA ymm3_m256;
vpcmpgtd                :       VPCMPGTD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPGTD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPCMPGTD k1 {k2} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPCMPGTD k1 {k2} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPCMPGTD k1 {k2} COMMA zmm2 COMMA zmm3_m512_m32bcst;
  
pcmpgtq                 :       PCMPGTQ xmm1 COMMA xmm2_m128;
vpcmpgtq                :       VPCMPGTQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPCMPGTQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPCMPGTQ k1 {k2} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPCMPGTQ k1 {k2} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPCMPGTQ k1 {k2} COMMA zmm2 COMMA zmm3_m512_m64bcst;

pcmpistri               :       PCMPISTRI xmm1 COMMA xmm2_m128 COMMA imm;
vpcmpistri              :       VPCMPISTRI xmm1 COMMA xmm2_m128 COMMA imm;

pcmpistrm               :       PCMPISTRM xmm1 COMMA xmm2_m128 COMMA imm;
vpcmpistrm              :       VPCMPISTRM xmm1 COMMA xmm2_m128 COMMA imm;

pdep                    :       PDEP r32a COMMA r32b COMMA r/m32
                        |       PDEP r64a COMMA r64b COMMA r/m64;

pext                    :       PEXT r32a COMMA r32b COMMA r/m32
                        |       PEXT r64a COMMA r64b COMMA r/m64;

pextrb                  :       PEXTRB reg_m8 COMMA xmm2 COMMA imm;
pextrd                  :       PEXTRD r_m32 COMMA xmm2 COMMA imm;
pextrq                  :       PEXTRQ r_m64 COMMA xmm2 COMMA imm;
vpextrb                 :       VPEXTRB reg_m8 COMMA xmm2 COMMA imm
                        |       VPEXTRB reg_m8 COMMA xmm2 COMMA imm;
vpextrd                 :       VPEXTRD r32_m32 COMMA xmm2 COMMA imm
                        |       VPEXTRD r32_m32 COMMA xmm2 COMMA imm;
vpextrq                 :       VPEXTRQ r64_m64 COMMA xmm2 COMMA imm
                        |       VPEXTRQ r64_m64 COMMA xmm2 COMMA imm;

pextrw                  :       PEXTRW reg COMMA mm COMMA imm
                        |       PEXTRW reg COMMA xmm COMMA imm
                        |       PEXTRW reg_m16 COMMA xmm COMMA imm;
vpextrw                 :       VPEXTRW reg COMMA xmm1 COMMA imm
                        |       VPEXTRW reg_m16 COMMA xmm2 COMMA imm
                        |       VPEXTRW reg COMMA xmm1 COMMA imm
                        |       VPEXTRW reg_m16 COMMA xmm2 COMMA imm;

phaddw                  :       PHADDW mm1 COMMA mm2_m64
                        |       PHADDW xmm1 COMMA xmm2_m128;
phaddd                  :       PHADDD mm1 COMMA mm2_m64
                        |       PHADDD xmm1 COMMA xmm2_m128;
vphaddw                 :       VPHADDW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPHADDW ymm1 COMMA ymm2 COMMA ymm3_m256;
vphaddd                 :       VPHADDD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPHADDD ymm1 COMMA ymm2 COMMA ymm3_m256;

phaddsw                 :       PHADDSW mm1 COMMA mm2_m64
                        |       PHADDSW xmm1 COMMA xmm2_m128;
vphaddsw                :       VPHADDSW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPHADDSW ymm1 COMMA ymm2 COMMA ymm3_m256;

phminposuw              :       PHMINPOSUW xmm1 COMMA xmm2_m128;
vphminposuw             :       VPHMINPOSUW xmm1 COMMA xmm2_m128;

phsubw                  :       PHSUBW mm1 COMMA mm2_m64
                        |       PHSUBW xmm1 COMMA xmm2_m128;
phsubd                  :       PHSUBD mm1 COMMA mm2_m64
                        |       PHSUBD xmm1 COMMA xmm2_m128;
vphsubw                 :       VPHSUBW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPHSUBW ymm1 COMMA ymm2 COMMA ymm3_m256;
vphsubd                 :       VPHSUBD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPHSUBD ymm1 COMMA ymm2 COMMA ymm3_m256;

phsubsw                 :       PHSUBSW mm1 COMMA mm2_m64
                        |       PHSUBSW xmm1 COMMA xmm2_m128;
vphsubsw                :       VPHSUBSW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPHSUBSW ymm1 COMMA ymm2 COMMA ymm3_m256;

pinsrb                  :       PINSRB xmm1 COMMA r32_m8 COMMA imm;
pinsrd                  :       PINSRD xmm1 COMMA r_m32 COMMA imm;
pinsdq                  :       PINSRQ xmm1 COMMA r_m64 COMMA imm;
vpinsrb                 :       VPINSRB xmm1 COMMA xmm2 COMMA r32_m8 COMMA imm;
vpinsrd                 :       VPINSRD xmm1 COMMA xmm2 COMMA r_m64 COMMA imm
                        |       VPINSRD xmm1 COMMA xmm2 COMMA r32_m32 COMMA imm;
vpinsrq                 :       VPINSRQ xmm1 COMMA xmm2 COMMA r64_m64 COMMA imm;

pinsrw                  :       PINSRW mm COMMA r32_m16 COMMA imm
                        |       PINSRW xmm COMMA r32_m16 COMMA imm;
vpinsrw                 :       VPINSRW xmm1 COMMA xmm2 COMMA r32_m16 COMMA imm;

pmaddubsw               :       PMADDUBSW mm1 COMMA mm2_m64
                        |       PMADDUBSW xmm1 COMMA xmm2_m128;
vpmaddubsw              :       VPMADDUBSW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMADDUBSW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMADDUBSW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMADDUBSW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMADDUBSW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

pmaddwd                 :       PMADDWD mm1 COMMA mm2_m64
                        |       PMADDWD xmm1 COMMA xmm2_m128;
vpmaddwd                :       VPMADDWD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMADDWD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMADDWD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMADDWD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMADDWD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

pmaxsw                  :       PMAXSW mm1 COMMA mm2_m64
                        |       PMAXSW xmm1 COMMA xmm2_m128;
pmaxsb                  :       PMAXSB xmm1 COMMA xmm2_m128;
pmaxsd                  :       PMAXSD xmm1 COMMA xmm2_m128;
vpmaxsb                 :       VPMAXSB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMAXSB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMAXSB xmm1{k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMAXSB ymm1{k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMAXSB zmm1{k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpmaxsw                 :       VPMAXSW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMAXSW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMAXSW xmm1{k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMAXSW ymm1{k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMAXSW zmm1{k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpmaxsd                 :       VPMAXSD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMAXSD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMAXSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst;

pmaxub                  :       PMAXUB mm1 COMMA mm2_m64
                        |       PMAXUB xmm1 COMMA xmm2_m128;
pmaxuw                  :       PMAXUW xmm1 COMMA xmm2_m128;
vpmaxub                 :       VPMAXUB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMAXUB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMAXUB xmm1{k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMAXUB ymm1{k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMAXUB zmm1{k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpmaxuw                 :       VPMAXUW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMAXUW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMAXUW xmm1{k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMAXUW ymm1{k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMAXUW zmm1{k1}{z} COMMA zmm2 COMMA zmm3_m512;

pmaxud                  :       PMAXUD xmm1 COMMA xmm2_m128;
vpmaxud                 :       VPMAXUD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMAXUD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMAXUD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPMAXUD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPMAXUD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpmaxuq                 :       VPMAXUQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPMAXUQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPMAXUQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

pminsw                  :       PMINSW mm1 COMMA mm2_m64
                        |       PMINSW xmm1 COMMA xmm2_m128;
pminsb                  :       PMINSB xmm1 COMMA xmm2_m128;
vpminsb                 :       VPMINSB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMINSB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMINSB xmm1{k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMINSB ymm1{k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMINSB zmm1{k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpminsw                 :       VPMINSW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMINSW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMINSW xmm1{k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMINSW ymm1{k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMINSW zmm1{k1}{z} COMMA zmm2 COMMA zmm3_m512;

pminsd                  :       PMINSD xmm1 COMMA xmm2_m128;
vpminsd                 :       VPMINSD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMINSD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMINSD xmm1{k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPMINSD ymm1{k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPMINSD zmm1{k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpminsq                 :       VPMINSQ xmm1{k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPMINSQ ymm1{k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPMINSQ zmm1{k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

pminub                  :       PMINUB mm1 COMMA mm2_m64
                        |       PMINUB xmm1 COMMA xmm2_m128;
pminuw                  :       PMINUW xmm1 COMMA xmm2_m128;
vpminub                 :       VPMINUB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMINUB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMINUB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMINUB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMINUB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpminuw                 :       VPMINUW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMINUW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMINUW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMINUW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMINUW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

pminud                  :       PMINUD xmm1 COMMA xmm2_m128;
vpminud                 :       VPMINUD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMINUD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMINUD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPMINUD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPMINUD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpminuq                 :       VPMINUQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPMINUQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPMINUQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

pmovmskb                :       PMOVMSKB reg COMMA mm
                        |       PMOVMSKB reg COMMA xmm;
vpmovmskb               :       VPMOVMSKB reg COMMA xmm1
                        |       VPMOVMSKB reg COMMA ymm1;*/
/*
pmovsxbw                :       PMOVSXBW xmm1 COMMA xmm2_m64;
pmovsxbd                :       PMOVSXBD xmm1 COMMA xmm2_m32;
pmovsxbq                :       PMOVSXBQ xmm1 COMMA xmm2_m16;
pmovsxwd                :       PMOVSXWD xmm1 COMMA xmm2_m64;
pmovsxwq                :       PMOVSXWQ xmm1 COMMA xmm2_m32;
pmovsxdq                :       PMOVSXDQ xmm1 COMMA xmm2_m64;
vpmovsxbw               :       VPMOVSXBW xmm1 COMMA xmm2_m64
                        |       VPMOVSXBW ymm1 COMMA xmm2_m128
                        |       VPMOVSXBW ymm1 {k1}{z} COMMA xmm2_m256;
vpmovsxbd               :       VPMOVSXBD xmm1 COMMA xmm2_m32
                        |       VPMOVSXBD ymm1 COMMA xmm2_m64
                        |       VPMOVSXBD xmm1 {k1}{z} COMMA xmm2_m32;
vpmovsxbq               :       VPMOVSXBQ xmm1 COMMA xmm2_m16
                        |       VPMOVSXBQ ymm1 COMMA xmm2_m32;
vpmovsxwd               :       VPMOVSXWD xmm1 COMMA xmm2_m64
                        |       VPMOVSXWD ymm1 COMMA xmm2_m128;
vpmovsxwq               :       VPMOVSXWQ xmm1 COMMA xmm2_m32
                        |       VPMOVSXWQ ymm1 COMMA xmm2_m64;
vpmovsxdq               :       VPMOVSXDQ xmm1 COMMA xmm2_m64
                        |       VPMOVSXDQ ymm1 COMMA xmm2_m128;

pmovzxbw                :       PMOVZXBW xmm1 COMMA xmm2_m64;
pmovzxbd                :       PMOVZXBD xmm1 COMMA xmm2_m32;
pmovzxbq                :       PMOVZXBQ xmm1 COMMA xmm2_m16;
pmovzxwd                :       PMOVZXWD xmm1 COMMA xmm2_m64;
pmovzxwq                :       PMOVZXWQ xmm1 COMMA xmm2_m32;
pmovzxdq                :       PMOVZXDQ xmm1 COMMA xmm2_m64;
vpmovzxbw               :       VPMOVZXBW xmm1 COMMA xmm2_m64
                        |       VPMOVZXBW ymm1 COMMA xmm2_m128
                        |       VPMOVZXBW xmm1 {k1}{z} COMMA xmm2_m64
                        |       VPMOVZXBW ymm1 {k1}{z} COMMA xmm2_m128
                        |       VPMOVZXBW zmm1 {k1}{z} COMMA xmm2_m256;
vpmovzxbd               :       VPMOVZXBD xmm1 COMMA xmm2_m32
                        |       VPMOVZXBD ymm1 COMMA xmm2_m64
                        |       VPMOVZXBD xmm1 {k1}{z} COMMA xmm2_m32
                        |       VPMOVZXBD ymm1 {k1}{z} COMMA xmm2_m64
                        |       VPMOVZXBD zmm1 {k1}{z} COMMA zmm2_m128;
vpmovzxbq               :       VPMOVZXBQ xmm1 COMMA xmm2_m16
                        |       VPMOVZXBQ ymm1 COMMA xmm2_m32
                        |       VPMOVZXBQ xmm1 {k1}{z} COMMA xmm2_m16
                        |       VPMOVZXBQ ymm1 {k1}{z} COMMA xmm2_m32
                        |       VPMOVZXBQ zmm1 {k1}{z} COMMA xmm2_m64;
vpmovzxwd               :       VPMOVZXWD xmm1 COMMA xmm2_m64
                        |       VPMOVZXWD ymm1 COMMA xmm2_m128
                        |       VPMOVZXWD xmm1 {k1}{z} COMMA xmm2_m64
                        |       VPMOVZXWD ymm1 {k1}{z} COMMA xmm2_m128
                        |       VPMOVZXWD zmm1 {k1}{z} COMMA zmm2_m256;
vpmovzxwq               :       VPMOVZXWQ xmm1 COMMA xmm2_m32
                        |       VPMOVZXWQ ymm1 COMMA xmm2_m64
                        |       VPMOVZXWQ xmm1 {k1}{z} COMMA xmm2_m32
                        |       VPMOVZXWQ ymm1 {k1}{z} COMMA xmm2_m64
                        |       VPMOVZXWQ zmm1 {k1}{z} COMMA zmm2_m128;
vpmovzxdq               :       VPMOVZXDQ xmm1 COMMA xmm2_m64
                        |       VPMOVZXDQ ymm1 COMMA xmm2_m128
                        |       VPMOVZXDQ xmm1 {k1}{z} COMMA xmm2_m64
                        |       VPMOVZXDQ ymm1 {k1}{z} COMMA xmm2_m128
                        |       VPMOVZXDQ zmm1 {k1}{z} COMMA ymm2_m256;

pmuldq                  :       PMULDQ xmm1 COMMA xmm2_m128;
vpmuldq                 :       VPMULDQ xmm1 COMMA xmm2 COMMA xmm3_m256
                        |       VPMULDQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMULDQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPMULDQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPMULDQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;
pmulhrsw                :       PMULHRSW mm1 COMMA mm2_m64
                        |       PMULHRSW xmm1 COMMA xmm2_m128;
vpmulhrsw               :       VPMULHRSW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMULHRSW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMULHRSW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMULHRSW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMULHRSW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
pmulhuw                 :       PMULHUW mm1 COMMA mm2_m64
                        |       PMULHUW xmm1 COMMA xmm2_m128;
vpmulhuw                :       VPMULHUW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMULHUW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMULHUW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMULHUW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMULHUW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
pmulhw                  :       PMULHW mm COMMA mm_m64
                        |       PMULHW xmm1 COMMA xmm2_m128;
vpmulhw                 :       VPMULHW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMULHW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMULHW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMULHW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMULHW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
pmulld                  :       PMULLD xmm1 COMMA xmm2_m128;
vpmulld                 :       VPMULLD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMULLD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMULLD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst;
                        |       VPMULLD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPMULLD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpmullq                 :       VPMULLQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPMULLQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPMULLQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;
pmullw                  :       PMULLW mm COMMA mm_m64
                        |       PMULLW xmm1 COMMA xmm2_m128;
vpmullw                 :       VPMULLW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMULLW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMULLW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPMULLW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPMULLW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
pmuludq                 :       PMULUDQ mm1 COMMA mm2_m64
                        |       PMULUDQ xmm1 COMMA xmm2_m128;
vpmuludq                :       VPMULUDQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPMULUDQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPMULUDQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPMULUDQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPMULUDQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;
pop                     :       POP r_m16
                        |       POP r_m32
                        |       POP r_m64
                        |       POP r16
                        |       POP r32
                        |       POP r64
                        |       POP DS
                        |       POP ES
                        |       POP SS
                        |       POP FS
                        |       POP GS;*/

popa			:       POPA	{ mal.output("0x61");}	# P_O_P_A;
popad			:       POPAD	{ mal.output("0x61");}	# P_O_P_A_D;

/*popcnt                :       POPCNT r16 COMMA r_m16
                        |       POPCNT r32 COMMA r_m32
                        |       POPCNT r64 COMMA r_m64;
*/
popf			:       POPF	{ mal.output("0x9D");}	# P_O_P_F;
popfd			:       POPFD	{ mal.output("0x9D");}	# P_O_P_F_D;
popfq			:       POPAD	{ mal.output("0x9D");}	# P_O_P_F_Q;
/*
por                     :       POR mm COMMA mm_m64
                        |       POR xmm1 COMMA xmm2_m128;
vpor                    :       VPOR xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPOR ymm1 COMMA ymm2 COMMA ymm3_m256;
vpord                   :       VPORD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPORD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPORD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vporq                   :       VPORQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPORQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPORQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

prefetcht0              :       PREFETCHT0 m8;
prefetcht1              :       PREFETCHT1 m8;
prefetcht2              :       PREFETCHT2 m8;
prefetchnta             :       PREFETCHNTA m8;

prefetchw               :       PREFETCHW m8;

psadbw                  :       PSADBW mm1 COMMA mm2_m64
                        |       PSADBW xmm1 COMMA xmm2_m128;
vpsadbw                 :       VPSADBW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSADBW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSADBW zmm1 COMMA zmm2 COMMA zmm3_m512;

pshufb                  :       PSHUFB mm1 COMMA mm2_m64
                        |       PSHUFB xmm1 COMMA xmm2_m128;
vpshufb                 :       VPSHUFB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSHUFB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSHUFB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSHUFB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPSHUFB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

pshufd                  :       PSHUFD xmm1 COMMA xmm2_m128 COMMA imm;
vpshufd                 :       VPSHUFD xmm1 COMMA xmm2_m128 COMMA imm
                        |       VPSHUFD ymm1 COMMA ymm2_m256 COMMA imm
                        |       VPSHUFD xmm1 {k1}{z} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VPSHUFD ymm1 {k1}{z} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VPSHUFD zmm1 {k1}{z} COMMA zmm2_m512_m32bcst COMMA imm;

pshufhw                 :       PSHUFHW xmm1 COMMA xmm2_m128 COMMA imm;
vpshufhw                :       VPSHUFHW xmm1 COMMA xmm2_m128 COMMA imm
                        |       VPSHUFHW ymm1 COMMA ymm2_m256 COMMA imm
                        |       VPSHUFHW xmm1 {k1}{z} COMMA xmm2_m128 COMMA imm
                        |       VPSHUFHW ymm1 {k1}{z} COMMA ymm2_m256 COMMA imm
                        |       VPSHUFHW zmm1 {k1}{z} COMMA zmm2_m512 COMMA imm;

pshuflw                 :       PSHUFLW xmm1 COMMA xmm2_m128 COMMA imm;
vpshuflw                :       VPSHUFLW xmm1 COMMA xmm2_m128 COMMA imm
                        |       VPSHUFLW ymm1 COMMA ymm2_m256 COMMA imm
                        |       VPSHUFLW xmm1 {k1}{z} COMMA xmm2_m128 COMMA imm
                        |       VPSHUFLW ymm1 {k1}{z} COMMA ymm2_m256 COMMA imm
                        |       VPSHUFLW zmm1 {k1}{z} COMMA zmm2_m512 COMMA imm;

pshufw                  :       PSHUFW mm1 COMMA mm2_m64 COMMA imm;

psignb                  :       PSIGNB mm1 COMMA mm2_m64
                        |       PSIGNB xmm1 COMMA xmm2_m128;
psignw                  :       PSIGNW mm1 COMMA mm2_m64
                        |       PSIGNW xmm1 COMMA xmm2_m128;
psignd                  :       PSIGND mm1 COMMA mm2_m64
                        |       PSIGND xmm1 COMMA xmm2_m128;
vpsignb                 :       VPSIGNB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSIGNB ymm1 COMMA ymm2 COMMA ymm3_m256;
vpsignw                 :       VPSIGNW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSIGNW ymm1 COMMA ymm2 COMMA ymm3_m256;
vpsignd                 :       VPSIGND xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSIGND ymm1 COMMA ymm2 COMMA ymm3_m256;

pslldq                  :       PSLLDQ xmm1 COMMA imm;
vpslldq                 :       VPSLLDQ xmm1 COMMA xmm2 COMMA imm
                        |       VPSLLDQ ymm1 COMMA ymm2 COMMA imm
                        |       VPSLLDQ xmm1 COMMA xmm2_m128 COMMA imm
                        |       VPSLLDQ ymm1 COMMA ymm2_m256 COMMA imm
                        |       VPSLLDQ zmm1 COMMA zmm2_m512 COMMA imm;

psllw                   :       PSLLW mm COMMA mm_m64
                        |       PSLLW xmm1 COMMA xmm2_m128
                        |       PSLLW mm1 COMMA imm
                        |       PSLLW mm1 COMMA imm
                        |       PSLLW xmm1 COMMA imm;
pslld                   :       PSLLD mm COMMA mm_m64
                        |       PSLLD xmm1 COMMA xmm2_m128
                        |       PSLLD mm COMMA imm
                        |       PSLLD xmm1 COMMA imm;
psllq                   :       PSLLQ mm COMMA mm_m64
                        |       PSLLQ xmm1 COMMA xmm2_m128
                        |       PSLLQ mm COMMA imm
                        |       PSLLQ xmm1 COMMA imm;
vpsllw                  :       VPSLLW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSLLW xmm1 COMMA xmm2 COMMA imm
                        |       VPSLLW ymm1 COMMA ymm2 COMMA xmm3_m128
                        |       VPSLLW ymm1 COMMA ymm2 COMMA imm
                        |       VPSLLW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSLLW ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128
                        |       VPSLLW zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128
                        |       VPSLLW xmm1 {k1}{z} COMMA xmm2_m128 COMMA imm
                        |       VPSLLW ymm1 {k1}{z} COMMA ymm2_m256 COMMA imm
                        |       VPSLLW zmm1 {k1}{z} COMMA zmm2_m512 COMMA imm;
vpslld                  :       VPSLLD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSLLD xmm1 COMMA xmm2 COMMA imm
                        |       VPSLLD ymm1 COMMA ymm2 COMMA xmm3_m128
                        |       VPSLLD ymm1 COMMA ymm2 COMMA imm
                        |       VPSLLD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSLLD ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128
                        |       VPSLLD zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128
                        |       VPSLLD xmm1 {k1}{z} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VPSLLD ymm1 {k1}{z} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VPSLLD zmm1 {k1}{z} COMMA zmm2_m512_m32bcst COMMA imm;
vpsllq                  :       VPSLLQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSLLQ xmm1 COMMA xmm2 COMMA imm
                        |       VPSLLQ ymm1 COMMA ymm2 COMMA xmm3_m128
                        |       VPSLLQ ymm1 COMMA ymm2 COMMA imm
                        |       VPSLLQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSLLQ ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128
                        |       VPSLLQ zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128
                        |       VPSLLQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst COMMA imm
                        |       VPSLLQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VPSLLQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst COMMA imm;

psraw                   :       PSRAW mm COMMA mm_m64
                        |       PSRAW xmm1 COMMA xmm2_m128
                        |       PSRAW mm COMMA imm
                        |       PSRAW xmm1 COMMA imm;
psrad                   :       PSRAD mm COMMA mm_m64
                        |       PSRAD xmm1 COMMA xmm2_m128
                        |       PSRAD mm COMMA imm
                        |       PSRAD xmm1 COMMA imm;
vpsraw                  :       VPSRAW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSRAW xmm1 COMMA xmm2 COMMA imm
                        |       VPSRAW ymm1 COMMA ymm2 COMMA xmm3_m128
                        |       VPSRAW ymm1 COMMA ymm2 COMMA imm
                        |       VPSRAW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSRAW ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128
                        |       VPSRAW zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128
                        |       VPSRAW xmm1 {k1}{z} COMMA xmm2_m128 COMMA imm
                        |       VPSRAW ymm1 {k1}{z} COMMA ymm2_m256 COMMA imm
                        |       VPSRAW zmm1 {k1}{z} COMMA zmm2_m512 COMMA imm;
vpsrad                  :       VPSRAD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSRAD xmm1 COMMA xmm2 COMMA imm
                        |       VPSRAD ymm1 COMMA ymm2 COMMA xmm3_m128
                        |       VPSRAD ymm1 COMMA ymm2 COMMA imm
                        |       VPSRAD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSRAD ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128
                        |       VPSRAD zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128
                        |       VPSRAD xmm1 {k1}{z} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VPSRAD ymm1 {k1}{z} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VPSRAD zmm1 {k1}{z} COMMA zmm2_m512_m32bcst COMMA imm;
vpsraq                  :       VPSRAQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSRAQ ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128
                        |       VPSRAQ zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128
                        |       VPSRAQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst COMMA imm
                        |       VPSRAQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VPSRAQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst COMMA imm;
psrldq                  :       PSRLDQ xmm1 COMMA imm;
vpsrldq                 :       VPSRLDQ xmm1 COMMA xmm2 COMMA imm
                        |       VPSRLDQ ymm1 COMMA ymm2 COMMA imm
                        |       VPSRLDQ xmm1 COMMA xmm2_m128 COMMA imm
                        |       VPSRLDQ ymm1 COMMA ymm2_m256 COMMA imm
                        |       VPSRLDQ zmm1 COMMA zmm2_m512 COMMA imm;
psrlw                   :       PSRLW mm COMMA mm_m64
                        |       PSRLW xmm1 COMMA xmm2_m128
                        |       PSRLW mm COMMA imm
                        |       PSRLW xmm1 COMMA imm;
psrld                   :       PSRLD mm COMMA mm_m64
                        |       PSRLD xmm1 COMMA xmm2_m128
                        |       PSRLD mm COMMA imm
                        |       PSRLD xmm1 COMMA imm;
psrlq                   :       PSRLQ mm COMMA mm_m64
                        |       PSRLQ xmm1 COMMA xmm2_m128
                        |       PSRLQ mm COMMA imm
                        |       PSRLQ xmm1 COMMA imm;
vpsrlw                  :       VPSRLW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSRLW xmm1 COMMA xmm2 COMMA imm
                        |       VPSRLW ymm1 COMMA ymm2 COMMA xmm3_m128
                        |       VPSRLW ymm1 COMMA ymm2 COMMA imm
                        |       VPSRLW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSRLW ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128
                        |       VPSRLW zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128
                        |       VPSRLW xmm1 {k1}{z} COMMA xmm2_m128 COMMA imm
                        |       VPSRLW ymm1 {k1}{z} COMMA ymm2_m256 COMMA imm
                        |       VPSRLW zmm1 {k1}{z} COMMA zmm2_m512 COMMA imm;
vpsrld                  :       VPSRLD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSRLD xmm1 COMMA xmm2 COMMA imm
                        |       VPSRLD ymm1 COMMA ymm2 COMMA xmm3_m128
                        |       VPSRLD ymm1 COMMA ymm2 COMMA imm
                        |       VPSRLD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSRLD ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128
                        |       VPSRLD zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128
                        |       VPSRLD xmm1 {k1}{z} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VPSRLD ymm1 {k1}{z} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VPSRLD zmm1 {k1}{z} COMMA zmm2_m512_m32bcst COMMA imm;
vpsrlq                  :       VPSRLQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSRLQ xmm1 COMMA xmm2 COMMA imm
                        |       VPSRLQ ymm1 COMMA ymm2 COMMA xmm3_m128
                        |       VPSRLQ ymm1 COMMA ymm2 COMMA imm
                        |       VPSRLQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSRLQ ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128
                        |       VPSRLQ zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128
                        |       VPSRLQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst COMMA imm
                        |       VPSRLQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VPSRLQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst COMMA imm;
psubb                   :       PSUBB mm COMMA mm_m64
                        |       PSUBB xmm1 COMMA xmm2_m128;
psubw                   :       PSUBW mm COMMA mm_m64
                        |       PSUBW xmm1 COMMA xmm2_m128;
psubd                   :       PSUBD mm COMMA mm_m64
                        |       PSUBD xmm1 COMMA xmm2_m128;
vpsubb                  :       VPSUBB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSUBB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSUBB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSUBB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPSUBB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpsubw                  :       VPSUBW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSUBW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSUBW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSUBW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPSUBW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpsubd                  :       VPSUBD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSUBD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSUBD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPSUBD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPSUBD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;

psubq                   :       PSUBQ mm1 COMMA mm2_m64
                        |       PSUBQ xmm1 COMMA xmm2_m128;
vpsubq                  :       VPSUBQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSUBQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSUBQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPSUBQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPSUBQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

psubsb                  :       PSUBSB mm COMMA mm_m64
                        |       PSUBSB xmm1 COMMA xmm2_m128;
psubsw                  :       PSUBSW mm COMMA mm_m64
                        |       PSUBSW xmm1 COMMA xmm2_m128;
vpsubsb                 :       VPSUBSB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSUBSB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSUBSB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSUBSB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPSUBSB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpsubsw                 :       VPSUBSW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSUBSW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSUBSW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSUBSW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPSUBSW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
psubusb                 :       PSUBUSB mm COMMA mm_m64
                        |       PSUBUSB xmm1 COMMA xmm2_m128
                        |       PSUBUSB xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       PSUBUSB ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       PSUBUSB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       PSUBUSB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       PSUBUSB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
psubusw                 :       PSUBUSW mm COMMA mm_m64
                        |       PSUBUSW xmm1 COMMA xmm2_m128
                        |       PSUBUSW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       PSUBUSW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       PSUBUSW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       PSUBUSW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       PSUBUSW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
ptest                   :       PTEST xmm1 COMMA xmm2_m128;
vptest                  :       VPTEST xmm1 COMMA xmm2_m128
                        |       VPTEST ymm1 COMMA ymm2_m256;
ptwrite                 :       PTWRITE r64_m64
                        |       PTWRITE r32_m32;
punpckhbw               :       PUNPCKHBW mm COMMA mm_m64
                        |       PUNPCKHBW xmm1 COMMA xmm2_m128;
punpckhbd               :       PUNPCKHBD mm COMMA mm_m64
                        |       PUNPCKHBD xmm1 COMMA xmm2_m128;
punpckhbq               :       PUNPCKHBQ mm COMMA mm_m64
                        |       PUNPCKHBQ xmm1 COMMA xmm2_m128;
punpckhqdq              :       PUNPCKHQDQ xmm1 COMMA xmm2_m128
vpunpckhbw              :       VPUNPCKHBW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKHBW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKHBW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKHBW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKHBW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpunpckhwd              :       VPUNPCKHWD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKHWD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKHWD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKHWD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKHWD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpunpckhdq              :       VPUNPCKHDQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKHDQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKHDQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPUNPCKHDQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPUNPCKHDQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpunpckhqdq             :       VPUNPCKHQDQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKHQDQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKHQDQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPUNPCKHQDQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPUNPCKHQDQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

punpcklbw               :       PUNPCKLBW mm COMMA mm_m32
                        |       PUNPCKLBW xmm1 COMMA xmm2_m128;
punpcklwd               :       PUNPCKLWD mm COMMA mm_m32
                        |       PUNPCKLWD xmm1 COMMA xmm2_m128;
punpckldq               :       PUNPCKLDQ mm COMMA mm_m32
                        |       PUNPCKLDQ xmm1 COMMA xmm2_m128;
punpcklqdq              :       PUNPCKLQDQ xmm1 COMMA xmm2_m128;
vpunpcklbw              :       VPUNPCKLBW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKLBW ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKLBW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKLBW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKLBW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpunpcklwd              :       VPUNPCKLWD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKLWD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKLWD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKLWD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKLWD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpunpckldq              :       VPUNPCKLDQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKLDQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKLDQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPUNPCKLDQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPUNPCKLDQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpunpcklqdq             :       VPUNPCKLQDQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPUNPCKLQDQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPUNPCKLQDQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPUNPCKLQDQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPUNPCKLQDQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPUNPCKLQDQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

push                    :       PUSH r_m16
                        |       PUSH r_m32
                        |       PUSH r_m64
                        |       PUSH r16
                        |       PUSH r32
                        |       PUSH r64
                        |       PUSH imm
                        |       PUSH imm
                        |       PUSH imm
                        |       PUSH CS
                        |       PUSH SS
                        |       PUSH DS
                        |       PUSH ES
                        |       PUSH FS
                        |       PUSH GS;

pusha                   :       PUSHA;
pushad                  :       PUSHAD;
*/
pushf			:       PUSHF	{ mal.output("0x9C");}	# P_U_S_H_F;
pushfd			:       PUSHFD	{ mal.output("0x9C");}	# P_U_S_H_F_D;
pushfq			:       PUSHFQ	{ mal.output("0x9C");}	# P_U_S_H_F_Q;
/*
pxor                    :       PXOR mm COMMA mm_m64
                        |       PXOR xmm1 COMMA xmm2_m128;
vpxor                   :       VPXOR xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPXOR ymm1 COMMA ymm2 COMMA ymm3_m256;
vpxord                  :       VPXORD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPXORD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPXORD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpxorq                  :       VPXORQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPXORQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPXORQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

rcl                     :       RCL r_m8 COMMA '1'
                        |       RCL r_m8 COMMA CL
                        |       RCL r_m8 COMMA imm
                        |       RCL r_m16 COMMA '1'
                        |       RCL r_m16 COMMA CL
                        |       RCL r_m16 COMMA imm
                        |       RCL r_m32 COMMA '1'
                        |       RCL r_m64 COMMA '1'
                        |       RCL r_m32 COMMA CL
                        |       RCL r_m64 COMMA CL
                        |       RCL r_m32 COMMA imm
                        |       RCL r_m64 COMMA imm;
rcr                     :       RCR r_m8 COMMA '1'
                        |       RCR r_m8 COMMA CL
                        |       RCR r_m8 COMMA imm
                        |       RCR r_m16 COMMA '1'
                        |       RCR r_m16 COMMA CL
                        |       RCR r_m16 COMMA imm
                        |       RCR r_m32 COMMA '1'
                        |       RCR r_m64 COMMA '1'
                        |       RCR r_m32 COMMA CL
                        |       RCR r_m64 COMMA CL
                        |       RCR r_m32 COMMA imm
                        |       RCR r_m64 COMMA imm
rol                     :       ROL r_m8 COMMA '1'
                        |       ROL r_m8 COMMA CL
                        |       ROL r_m8 COMMA imm;
rcpps                   :       RCPPS xmm1 COMMA xmm2_m128;
vrcpps                  :       VRCPPS xmm1 COMMA xmm2_m128
                        |       VRCPPS ymm1 COMMA ymm2_m256;

rcpss                   :       RCPSS xmm1 COMMA xmm2_m32;
vrcpss                  :       VRCPSS xmm1 COMMA xmm2 COMMA xmm3_m32;

rdfsbase                :       RDFSBASE r32
                        |       RDFSBASE r64;
rdgsbase                :       RDGSBASE r32
                        |       RDGSBASE r64;

rdmsr			:       RDMSR	{ mal.output("0x0F","0x32");}	# R_D_M_S_R;

rdpid                   :       RDPID r32
                        |       RDPID r64;

rdpkru                  :       RDPKRU;

rdpmc			:       RDPMC	{ mal.output("0x0F","0x33");}	# R_D_P_M_C;

rdrand                  :       RDRAND r16
                        |       RDRAND r32
                        |       RDRAND r64;

rdseed                  :       RDSEED r16
                        |       RDSEED r32
                        |       RDSEED r64;

rdtsc			:       RDTSC	{ mal.output("0x0F","0x31");}	# R_D_T_S_C;

rdtscp			:       RDTSCP	{ mal.output("0x0F","0x01","0xF9");}	# R_D_T_S_C_P;

rep ins                 :       REP INS m8 COMMA DX
                        |       REP INS m16 COMMA DX
                        |       REP INS m32 COMMA DX
                        |       REP INS r_m32 COMMA DX;
rep movs                :       REP MOVS m8 COMMA m8
                        |       REP MOVS m16 COMMA m16
                        |       REP MOVS m32 COMMA m32
                        |       REP MOVS m64 COMMA m64;
rep outs                :       REP OUTS DX COMMA r_m8
                        |       REP OUTS DX COMMA r_m16
                        |       REP OUTS DX COMMA r_m32;
rep lods                :       REP LODS AL
                        |       REP LODS AX
                        |       REP LODS EAX
                        |       REP LODS RAX;
rep stos                :       REP STOS m8
                        |       REP STOS m16
                        |       REP STOS m32
                        |       REP STOS m64;
repe cmps               :       REPE CMPS m8 COMMA m8
                        |       REPE CMPS m16 COMMA m16
                        |       REPE CMPS m32 COMMA m32
                        |       REPE CMPS m64 COMMA m64;
repe scas               :       REPE SCAS m8
                        |       REPE SCAS m16
                        |       REPE SCAS m32
                        |       REPE SCAS m64;
repne cmps              :       REPNE CMPS m8 COMMA m8
                        |       REPNE CMPS m16 COMMA m16
                        |       REPNE CMPS m32 COMMA m32
                        |       REPNE CMPS m64 COMMA m64;
repne scas              :       REPNE SCAS m8
                        |       REPNE SCAS m16
                        |       REPNE SCAS m32
                        |       REPNE SCAS m64;
*/
ret			:       RET	{ mal.output("0xC3");}	# R_E_T//why two
			|       RET	{ mal.output("0xCB");}	# R_E_T
			|       RET imm	{ mal.output("0xC2",CalculatorLibrary.cal($imm.text));}	# RET_imm
			|       RET imm	{ mal.output("0xC8",CalculatorLibrary.cal($imm.text));}	# RET_imm;
/*
rorx                    :       RORX r32 COMMA r_m32 COMMA imm
                        |       RORX r64 COMMA r_m64 COMMA imm;

roundpd                 :       ROUNDPD xmm1 COMMA xmm2_m128 COMMA imm;
vroundpd                :       VROUNDPD xmm1 COMMA xmm2_m128 COMMA imm
                        |       VROUNDPD ymm1 COMMA ymm2_m256 COMMA imm;

roundps                 :       ROUNDPS xmm1 COMMA xmm2_m128 COMMA imm;
vroundps                :       VROUNDPS xmm1 COMMA xmm2_m128 COMMA imm
                        |       VROUNDPS ymm1 COMMA ymm2_m256 COMMA imm;

roundsd                 :       ROUNDSD xmm1 COMMA xmm2_m64 COMMA imm;
vroundsd                :       VROUNDSD xmm1 COMMA xmm2 COMMA xmm3_m64 COMMA imm;

roundss                 :       ROUNDSS xmm1 COMMA xmm2_m32 COMMA imm;
vroundss                :       VROUNDSS xmm1 COMMA xmm2 COMMA xmm3_m32 COMMA imm;
*/
rsm			:       RSM	{ mal.output("0x0F","0xAA");}	# R_S_M;
/*
rsqrtps                 :       RSQRTPS xmm1 COMMA xmm2_m128;
vrsqrtps                :       VRSQRTPS xmm1 COMMA xmm2_m128
                        |       VRSQRTPS ymm1 COMMA ymm2_m256;

vsqrtss                 :       VSQRTSS xmm1 COMMA xmm2_m32;
vrsqrtss                :       VRSQRTSS xmm1 COMMA xmm2 COMMA xmm3_m32;
*/
sahf			:       SAHF	{ mal.output("0x9E");}	# S_A_H_F;
/*
sal                     :       SAL r_m8 COMMA '1'
                        |       SAL r_m8 COMMA CL
                        |       SAL r_m8 COMMA imm
                        |       SAL r_m16 COMMA '1'
                        |       SAL r_m16 COMMA CL
                        |       SAL r_m16 COMMA imm
                        |       SAL r_m32 COMMA '1'
                        |       SAL r_m64 COMMA '1'
                        |       SAL r_m32 COMMA CL
                        |       SAL r_m64 COMMA CL
                        |       SAL r_m32 COMMA imm
                        |       SAL r_m64 COMMA imm;
sar                     :       SAR r_m8 COMMA '1'
                        |       SAR r_m8 COMMA CL
                        |       SAR r_m8 COMMA imm
                        |       SAR r_m16 COMMA '1'
                        |       SAR r_m16 COMMA CL
                        |       SAR r_m16 COMMA imm
                        |       SAR r_m32 COMMA '1'
                        |       SAR r_m64 COMMA '1'
                        |       SAR r_m32 COMMA CL
                        |       SAR r_m64 COMMA CL
                        |       SAR r_m32 COMMA imm
                        |       SAR r_m64 COMMA imm;
shl                     :       SHL r_m8 COMMA '1'
                        |       SHL r_m8 COMMA CL
                        |       SHL r_m8 COMMA imm
                        |       SHL r_m16 COMMA '1'
                        |       SHL r_m16 COMMA CL
                        |       SHL r_m16 COMMA imm
                        |       SHL r_m32 COMMA '1'
                        |       SHL r_m64 COMMA '1'
                        |       SHL r_m32 COMMA CL
                        |       SHL r_m64 COMMA CL
                        |       SHL r_m32 COMMA imm
                        |       SHL r_m64 COOMA imm;
shr                     :       SHR r_m8 COMMA '1'
                        |       SHR r_m8 COMMA CL
                        |       SHR r_m8 COMMA imm
                        |       SHR r_m16 COMMA '1'
                        |       SHR r_m16 COMMA CL
                        |       SHR r_m16 COMMA imm
                        |       SHR r_m32 COMMA '1'
                        |       SHR r_m64 COMMA '1'
                        |       SHR r_m32 COMMA CL
                        |       SHR r_m64 COMMA CL
                        |       SHR r_m32 COMMA imm
                        |       SHR r_m64 COMMA imm;

sarx                    :       SARX r32a COMMA r_m32 COMMA r32b
                        |       SARX r64a COMMA r_m64 COMMA r64b;
shlx                    :       SHLX r32a COMMA r_m32 COMMA r32b
                        |       SHLX r64a COMMA r_m64 COMMA r64b;
shrx                    :       SHRX r32a COMMA r_m32 COMMA r32b
                        |       SHRX r64a COMMA r_m64 COMMA r64b;

sbb                     :       SBB AL COMMA imm
                        |       SBB AX COMMA imm
                        |       SBB EAX COMMA imm
                        |       SBB RAX COMMA imm
                        |       SBB r_m8 COMMA imm
                        |       SBB r_m16 COMMA imm
                        |       SBB r_m32 COMMA imm
                        |       SBB r_m64 COMMA imm
                        |       SBB r_m16 COMMA imm
                        |       SBB r_m32 COMMA imm
                        |       SBB r_m64 COMMA imm
                        |       SBB r_m8 COMMA r8
                        |       SBB r_m16 COMMA r16
                        |       SBB r_m32 COMMA r32
                        |       SBB r_m64 COMMA r64
                        |       SBB r8 COMMA r_m8
                        |       SBB r16 COMMA r_m16
                        |       SBB r32 COMMA r_m32        
                        |       SBB r64 COMMA r_m64;
scas                    :       SCAS m8
                        |       SCAS m16
                        |       SCAS m32
                        |       SCAS m64;
scasb                   :       SCASB;
scasw                   :       SCASW;
scasd                   :       SCASD;
scasq                   :       SCASQ;

seta                    :       SETA r_m8;
setae                   :       SETAE r_m8;
setb                    :       SETB r_m8;
setbe                   :       SETBE r_m8;
setc                    :       SETC r_m8;
sete                    :       SETE r_m8;
setg                    :       SETG r_m8;
setge                   :       SETGE r_m8;
setl                    :       SETL r_m8;
setle                   :       SETLE r_m8;
setna                   :       SETNA r_m8;
setnae                  :       SETNAE r_m8;
setnb                   :       SETNB r_m8;
setnbe                  :       SETNBE r_m8;
setnc                   :       SETNC r_m8;
setne                   :       SETNE r_m8;
setng                   :       SETNG r_m8;
setnge                  :       SETNGE r_m8;
setnl                   :       SETNL r_m8;
setnle                  :       SETNLE r_m8;
setno                   :       SETNO r_m8;
setnp                   :       SETNP r_m8;
setns                   :       SETNS r_m8;
setnz                   :       SETNZ r_m8;
seto                    :       SETO r_m8;
setp                    :       SETP r_m8;
setpe                   :       SETPE r_m8;
setpo                   :       SETPO r_m8;
sets                    :       SETS r_m8;
setz                    :       SETZ r_m8;

sfence                  :       SFENCE;

sgdt                    :       SGDT m;

sha1rnds4               :       SHA1RNDS4 xmm1 COMMA xmm2_m128 COMMA imm;

sha1nexte               :       SHA1NEXTE xmm1 COMMA xmm2_m128;

sha1msg1                :       SHA1MSG1 xmm1 COMMA xmm2_m128;

sha1msg2                :       SHA1MSG2 xmm1 COMMA xmm2_m128;

sha256rnds2             :       SHA256RNDS2 xmm1 COMMA xmm2_m128 COMMA <XMM0>;

sha256msg1              :       SHA256MSG1 xmm1 COMMA xmm2_m128;

sha256msg2              :       SHA256MSG2 xmm1 COMMA xmm2_m128;

shld                    :       SHLD r_m16 COMMA r16 COMMA imm
                        |       SHLD r_m16 COMMA r16 COMMA CL
                        |       SHLD r_m32 COMMA r32 COMMA imm
                        |       SHLD r_m64 COMMA r64 COMMA imm
                        |       SHLD r_m32 COMMA r32 COMMA CL
                        |       SHLD r_m64 COMMA r64 COMMA CL;

shrd                    :       SHRD r_m16 COMMA r16 COMMA imm
                        |       SHRD r_m16 COMMA r16 COMMA CL
                        |       SHRD r_m32 COMMA r32 COMMA imm
                        |       SHRD r_m64 COMMA r64 COMMA imm
                        |       SHRD r_m32 COMMA r32 COMMA CL
                        |       SHRD r_m64 COMMA r64 COMMA CL;

shufpd                  :       SHUFPD xmm1 COMMA xmm2_m128 COMMA imm
vshufpd                 :       VSHUFPD xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VSHUFPD ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm
                        |       VSHUFPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst COMMA imm
                        |       VSHUFPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VSHUFPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst COMMA imm;

shufps                  :       SHUFPS xmm1 COMMA xmm3_m128 COMMA imm
vshufps                 :       VSHUFPS xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VSHUFPS ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm
                        |       VSHUFPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst COMMA imm
                        |       VSHUFPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst COMMA imm
                        |       VSHUFPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst COMMA imm;

sidt                    :       SIDT m;

sldt                    :       SLDT r_m16
                        |       SLDT r64_m16;

smsw                    :       SMSW r_m16
                        |       SMSW r32_m16
                        |       SMSW r64_m16;

sqrtpd                  :       SQRTPD xmm1 COMMA xmm2_m128;
vsqrtpd                 :       VSQRTPD xmm1 COMMA xmm2_m128
                        |       VSQRTPD ymm1 COMMA ymm2_m256
                        |       VSQRTPD xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VSQRTPD ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VSQRTPD zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{er};

sqrtps                  :       SQRTPS xmm1 COMMA xmm2_m128;
vsqrtps                 :       VSQRTPS xmm1 COMMA xmm2_m128
                        |       VSQRTPS ymm1 COMMA ymm2_m256
                        |       VSQRTPS xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VSQRTPS ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VSQRTPS zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{er};

sqrtsd                  :       SQRTSD xmm1 COMMA xmm2_m64;
vsqrtsd                 :       VSQRTSD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VSQRTSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};

sqrtss                  :       SQRTSS xmm1 COMMA xmm2_m32;
vsqrtss                 :       VSQRTSS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VSQRTSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};

stac                    :       STAC;
*/
stc			:       STC	{ mal.output("0xF9");}	# S_T_C;

std			:       STD	{ mal.output("0xFD");}	# S_T_D;

sti			:       STI	{ mal.output("0xFB");}	# S_T_I;
/*
stmxcsr                 :       STMXCSR m32;
vstmxcsr                :       VSTMXCSR m32;

stos                    :       STOS m8
                        |       STOS m16
                        |       STOS m32
                        |       STOS m64;
stosb                   :       STOSB;
stosw                   :       STOSW;
stosd                   :       STOSD;
stosq                   :       STOSQ;

str                     :       STR r_m16;

sub                     :       SUB AL COMMA imm
                        |       SUB AX COMMA imm
                        |       SUB EAX COMMA imm
                        |       SUB RAX COMMA imm
                        |       SUB r_m8 COMMA imm
                        |       SUB r_m16 COMMA imm
                        |       SUB r_m32 COMMA imm
                        |       SUB r_m64 COMMA imm
                        |       SUB r_m16 COMMA imm
                        |       SUB r_m32 COMMA imm
                        |       SUB r_m64 COMMA imm
                        |       SUB r_m8 COMMA r8
                        |       SUB r_m16 COMMA r16
                        |       SUB r_m32 COMMA r32
                        |       SUB r_m64 COMMA r64
                        |       SUB r8 COMMA r_m8
                        |       SUB r16 COMMA r_m16
                        |       SUB r32 COMMA r_m32
                        |       SUB r64 COMMA r_m64;
subpd                   :       SUBPD xmm1 COMMA xmm2_m128;
vsubpd                  :       VSUBPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VSUBPD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VSUBPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VSUBPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VSUBPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};

subps                   :       SUBPS xmm1 COMMA xmm2_m128;
vsubps                  :       VSUBPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VSUBPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VSUBPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VSUBPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VSUBPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};

subsd                   :       SUBSD xmm1 COMMA xmm2_m64;
vsubsd                  :       VSUBSD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VSUBSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};

subss                   :       SUBSS xmm1 COMMA xmm2_m32;
vsubss                  :       VSUBSS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VSUBSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};
*/
swapgs			:       SWAPGS	{ mal.output("0x0F","0x01","0xF8");}	# S_W_A_P_G_S;

syscall			:       SYSCALL	{ mal.output("0x0F","0x05");}	# S_Y_S_C_A_L_L;

sysenter		:       SYSENTER{ mal.output("0x0F","0x34");}	# S_Y_S_E_N_T_E_R;
/*
sysexit                 :       SYSEXIT #S_Y_S_E_X_I_T;

sysret                  :       SYSRET;

test                    :       TEST AL COMMA imm
                        |       TEST AX COMMA imm
                        |       TEST EAX COMMA imm
                        |       TEST RAX COMMA imm
                        |       TEST r_m8 COMMA imm
                        |       TEST r_m16 COMMA imm
                        |       TEST r_m32 COMMA imm
                        |       TEST r_m64 COMMA imm
                        |       TEST r_m8 COMMA r8
                        |       TEST r_m16 COMMA r16
                        |       TEST r_m32 COMMA r32
                        |       TEST r_m64 COMMA r64

tpause                  :       TPAUSE r32 COMMA <edx> COMMA <eax>;

tzcnt                   :       TZCNT r16 COMMA r_m16
                        |       TZCNT r32 COMMA r_m32
                        |       TZCNT r64 COMMA r_m64;

ucomisd                 :       UCOMISD xmm1 COMMA xmm2_m64;
vucomisd                :       VUCOMISD xmm1 COMMA xmm2_m64
                        |       VUCOMISD xmm1 COMMA xmm2_m64{sae};

ucomiss                 :       UCOMISS xmm1 COMMA xmm2_m32;
vucomiss                :       VUCOMISS xmm1 COMMA xmm2_m32
                        |       VUCOMISS xmm1 COMMA xmm2_m32{sae};

ud0                     :       UD0 r32 COMMA r_m32;
ud1                     :       UD1 r32 COMMA r_m32;
ud2                     :       UD2;

umonitor                :       UMONITOR r16_r32_r64;
umwait                  :       UMWAIT r32 COMMA <edx> COMMA <eax>;

unpckhpd                :       UNPCKHPD xmm1 COMMA xmm2_m128;
vunpckhpd               :       VUNPCKHPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VUNPCKHPD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VUNPCKHPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VUNPCKHPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VUNPCKHPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

unpckhps                :       UNPCKHPS xmm1 COMMA xmm2_m128;
vunpckhps               :       VUNPCKHPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VUNPCKHPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VUNPCKHPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VUNPCKHPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VUNPCKHPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;

unpcklpd                :       UNPCKLPD xmm1 COMMA xmm2_m128;
vunpcklpd               :       VUNPCKLPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VUNPCKLPD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VUNPCKLPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VUNPCKLPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VUNPCKLPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

unpcklps                :       UNPCKLPS xmm1 COMMA xmm2_m128;
vunpcklps               :       VUNPCKLPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VUNPCKLPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VUNPCKLPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VUNPCKLPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VUNPCKLPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;

valignd                 :       VALIGND xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst COMMA imm
                        |       VALIGND ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst COMMA imm
                        |       VALIGND zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst COMMA imm
valignq                 :       VALIGNQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst COMMA imm
                        |       VALIGNQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VALIGNQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst COMMA imm;

vblendmpd               :       VBLENDMPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VBLENDMPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VBLENDMPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;
vblendmps               :       VBLENDMPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VBLENDMPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VBLENDMPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;

vbroadcastss            :       VBROADCASTSS xmm1 COMMA m32
                        |       VBROADCASTSS ymm1 COMMA m32
                        |       VBROADCASTSS xmm1 COMMA xmm2
                        |       VBROADCASTSS ymm1 COMMA xmm2
                        |       VBROADCASTSS xmm1 {k1}{z} COMMA xmm2_m32
                        |       VBROADCASTSS ymm1 {k1}{z} COMMA xmm2_m32
                        |       VBROADCASTSS zmm1 {k1}{z} COMMA xmm2_m32;
vbroadcastsd            :       VBROADCASTSD ymm1 COMMA m64
                        |       VBROADCASTSD ymm1 COMMA xmm2
                        |       VBROADCASTSD ymm1 {k1}{z} COMMA xmm2_m64
                        |       VBROADCASTSD zmm1 {k1}{z} COMMA xmm2_m64;
vbroadcastf128          :       VBROADCASTF128 ymm1 COMMA m128;
vbroadcastf32x2         :       VBROADCASTF32X2 ymm1 {k1}{z} COMMA xmm2_m64
                        |       VBROADCASTF32X2 zmm1 {k1}{z} COMMA xmm2_m64;
vbroadcastf32x4         :       VBROADCASTF32X4 ymm1 {k1}{z} COMMA m128
                        |       VBROADCASTF32X4 zmm1 {k1}{z} COMMA m128;
vbroadcastf64x2         :       VBROADCASTF64X2 ymm1 {k1}{z} COMMA m128
                        |       VBROADCASTF64X2 zmm1 {k1}{z} COMMA m128;
vbroadcastf32x8         :       VBROADCASTF32X8 zmm1 {k1}{z} COMMA m256;
vbroadcastf64x4         :       VBROADCASTF64X4 zmm1 {k1}{z} COMMA m256;

vcompresspd             :       VCOMPRESSPD xmm1_m128 {k1}{z} COMMA xmm2
                        |       VCOMPRESSPD ymm1_m256 {k1}{z} COMMA ymm2
                        |       VCOMPRESSPD zmm1_m512 {k1}{z} COMMA zmm2;

vcompressps             :       VCOMPRESSPS xmm1_m128 {k1}{z} COMMA xmm2
                        |       VCOMPRESSPS ymm1_m256 {k1}{z} COMMA ymm2
                        |       VCOMPRESSPS zmm1_m512 {k1}{z} COMMA zmm2;

vcvtpd2qq               :       VCVTPD2QQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTPD2QQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTPD2QQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{er};

vcvtpd2udq              :       VCVTPD2UDQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTPD2UDQ xmm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTPD2UDQ ymm1 {k1}{z} COMMA zmm2_m512_m64bcst{er};

vcvtpd2uqq              :       VCVTPD2UQQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTPD2UQQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTPD2UQQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{er};

vcvtph2ps               :       VCVTPH2PS xmm1 COMMA xmm2_m64
                        |       VCVTPH2PS ymm1 COMMA xmm2_m128
                        |       VCVTPH2PS xmm1 {k1}{z} COMMA xmm2_m64
                        |       VCVTPH2PS ymm1 {k1}{z} COMMA xmm2_m128
                        |       VCVTPH2PS zmm1 {k1}{z} COMMA ymm2_m256{sae};

vcvtps2ph               :       VCVTPS2PH xmm1_m64 COMMA xmm2 COMMA imm
                        |       VCVTPS2PH xmm1_m128 COMMA ymm2 COMMA imm
                        |       VCVTPS2PH xmm1_m64 {k1}{z} COMMA xmm2 COMMA imm
                        |       VCVTPS2PH xmm1_m128 {k1}{z} COMMA ymm2 COMMA imm
                        |       VCVTPS2PH ymm1_m256 {k1}{z} COMMA zmm2{sae} COMMA imm;

vcvtps2udq              :       VCVTPS2UDQ xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTPS2UDQ ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VCVTPS2UDQ zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{er};

vcvtps2qq               :       VCVTPS2QQ xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTPS2QQ ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VCVTPS2QQ zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{er};

vcvtps2uqq              :       VCVTPS2UQQ xmm1 {k1}{z} COMMA xmm2_m64_m32bcst
                        |       VCVTPS2UQQ ymm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTPS2UQQ zmm1 {k1}{z} COMMA ymm2_m256_m32bcst{er};

vcvtqq2pd               :       VCVTQQ2PD xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTQQ2PD ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTQQ2PD zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{er};

vcvtqq2ps               :       VCVTQQ2PS xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTQQ2PS xmm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTQQ2PS ymm1 {k1}{z} COMMA zmm2_m512_m64bcst{er};

vcvtsd2usi              :       VCVTSD2USI r32 COMMA xmm1_m64{er}
                        |       VCVTSD2USI r64 COMMA xmm1_m64{er};

vcvtss2usi              :       VCVTSS2USI r32 COMMA xmm1_m32{er}
                        |       VCVTSS2USI r64 COMMA xmm1_m32{er};

vcvttpd2qq              :       VCVTTPD2QQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTTPD2QQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTTPD2QQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{sae};

vcvttpd2udq             :       VCVTTPD2UDQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTTPD2UDQ xmm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTTPD2UDQ ymm1 {k1}{z} COMMA zmm2_m512_m64bcst{sae};

vcvttpd2uqq             :       VCVTTPD2UQQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTTPD2UQQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTTPD2UQQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{sae};

vcvttps2udq             :       VCVTTPS2UDQ xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTTPS2UDQ ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VCVTTPS2UDQ zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{sae};

vcvttps2qq              :       VCVTTPS2QQ xmm1 {k1}{z} COMMA xmm2_m64_m32bcst
                        |       VCVTTPS2QQ ymm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTTPS2QQ zmm1 {k1}{z} COMMA ymm2_m256_m32bcst{sae};

vcvttps2uqq             :       VCVTTPS2UQQ xmm1 {k1}{z} COMMA xmm2_m64_m32bcst
                        |       VCVTTPS2UQQ ymm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTTPS2UQQ zmm1 {k1}{z} COMMA ymm2_m256_m32bcst{sae};

vcvttsd2usi             :       VCVTTSD2USI r32 COMMA xmm1_m64{er}
                        |       VCVTTSD2USI r64 COMMA xmm1_m64{er};

vcvttss2usi             :       VCVTTSS2USI r32 COMMA xmm1_m32{er}
                        |       VCVTTSS2USI r64 COMMA xmm1_m32{er};

vcvtudq2pd              :       VCVTUDQ2PD xmm1 {k1}{z} COMMA xmm2_m64_m32bcst
                        |       VCVTUDQ2PD ymm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTUDQ2PD zmm1 {k1}{z} COMMA ymm2_m256_m32bcst;

vcvtudq2ps              :       VCVTUDQ2PS xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VCVTUDQ2PS ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VCVTUDQ2PS zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{er};

vcvtuqq2pd              :       VCVTUQQ2PD xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTUQQ2PD ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTUQQ2PD zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{er};

vcvtuqq2ps              :       VCVTUQQ2PS xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VCVTUQQ2PS xmm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VCVTUQQ2PS ymm1 {k1}{z} COMMA zmm2_m512_m64bcst{er};

vcvtusi2sd              :       VCVTUSI2SD xmm1 COMMA xmm2 COMMA r_m32
                        |       VCVTUSI2SD xmm1 COMMA xmm2 COMMA r_m64{er};

vcvtusi2ss              :       VCVTUSI2SS xmm1 COMMA xmm2 COMMA r_m32{er}
                        |       VCVTUSI2SS xmm1 COMMA xmm2 COMMA r_m64{er};

vdbpsadbw               :       VDBPSADBW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VDBPSADBW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256 COMMA imm
                        |       VDBPSADBW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512 COMMA imm

vexpandpd               :       VEXPANDPD xmm1 {k1}{z} COMMA xmm2_m128
                        |       VEXPANDPD ymm1 {k1}{z} COMMA ymm2_m256
                        |       VEXPANDPD zmm1 {k1}{z} COMMA zmm2_m512;

vexpandps               :       VEXPANDPS xmm1 {k1}{z} COMMA xmm2_m128
                        |       VEXPANDPS ymm1 {k1}{z} COMMA ymm2_m256
                        |       VEXPANDPS zmm1 {k1}{z} COMMA zmm2_m512;


verr                    :       VERR r_m16;
verw                    :       VERW r_m16;

vextractf128            :       VEXTRACTF128 xmm1_m128 COMMA ymm2 COMMA imm
                        |       VEXTRACTF32X4 xmm1_m128{k1}{z} COMMA ymm2 COMMA imm
                        |       VEXTRACTF32X4 xmm1_m128{k1}{z} COMMA zmm2 COMMA imm
                        |       VEXTRACTF64X2 xmm1_m128{k1}{z} COMMA ymm2 COMMA imm
                        |       VEXTRACTF64X2 xmm1_m128{k1}{z} COMMA zmm2 COMMA imm
                        |       VEXTRACTF32X8 ymm1_m256{k1}{z} COMMA zmm2 COMMA imm
                        |       VEXTRACTF64X4 ymm1_m256{k1}{z} COMMA zmm2 COMMA imm;

vextracti128            :       VEXTRACTI128 xmm1_m128 COMMA ymm2 COMMA imm
                        |       VEXTRACTI32X4 xmm1_m128{k1}{z} COMMA ymm2 COMMA imm
                        |       VEXTRACTI32X4 xmm1_m128{k1}{z} COMMA zmm2 COMMA imm
                        |       VEXTRACTI64X2 xmm1_m128{k1}{z} COMMA ymm2 COMMA imm
                        |       VEXTRACTI64X2 xmm1_m128{k1}{z} COMMA zmm2 COMMA imm
                        |       VEXTRACTI32X8 ymm1_m256{k1}{z} COMMA zmm2 COMMA imm
                        |       VEXTRACTI64X4 ymm1_m256{k1}{z} COMMA zmm2 COMMA imm;

vfixupimmpd             :       VFIXUPimmPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst COMMA imm
                        |       VFIXUPimmPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VFIXUPimmPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{sae} COMMA imm;

vfixupimmps             :       VFIXUPimmPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst COMMA imm
                        |       VFIXUPimmPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst COMMA imm
                        |       VFIXUPimmPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{sae} COMMA imm;

vfixupimmsd             :       VFIXUPimmSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{sae} COMMA imm;

vfixupimmss             :       VFIXUPimmSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{sae} COMMA imm;

vfmadd132pd             :       VFMADD132PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADD132PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADD132PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMADD132PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMADD132PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfmadd213pd             :       VFMADD213PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADD213PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADD213PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMADD213PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMADD213PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfmadd231pd             :       VFMADD231PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADD231PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADD231PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMADD231PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMADD231PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};

vfmadd132ps             :       VFMADD132PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADD132PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADD132PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMADD132PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMADD132PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfmadd213ps             :       VFMADD213PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADD213PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADD213PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMADD213PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMADD213PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfmadd231ps             :       VFMADD231PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADD231PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADD231PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMADD231PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMADD231PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};

vfmadd132sd             :       VFMADD132SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFMADD132SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};
vfmadd213sd             :       VFMADD213SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFMADD213SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};
vfmadd231sd             :       VFMADD231SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFMADD231SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};

vfmadd132ss             :       VFMADD132SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFMADD132SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};
vfmadd213ss             :       VFMADD213SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFMADD213SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};
vfmadd231ss             :       VFMADD231SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFMADD231SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};

vfmaddsub132pd          :       VFMADDSUB132PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADDSUB132PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADDSUB132PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMADDSUB132PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMADDSUB132PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfmaddsub213pd          :       VFMADDSUB213PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADDSUB213PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADDSUB213PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMADDSUB213PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMADDSUB213PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfmaddsub231pd          :       VFMADDSUB231PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADDSUB231PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADDSUB231PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMADDSUB231PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMADDSUB231PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};

vfmaddsub132ps          :       VFMADDSUB132PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADDSUB132PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADDSUB132PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMADDSUB132PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMADDSUB132PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfmaddsub213ps          :       VFMADDSUB213PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADDSUB213PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADDSUB213PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMADDSUB213PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMADDSUB213PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfmaddsub231ps          :       VFMADDSUB231PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMADDSUB231PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMADDSUB231PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMADDSUB231PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMADDSUB231PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};

vfmsubadd132pd          :       VFMSUBADD132PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUBADD132PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUBADD132PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMSUBADD132PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMSUBADD132PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfmsubadd213pd          :       VFMSUBADD213PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUBADD213PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUBADD213PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMSUBADD213PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMSUBADD213PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfmsubadd231pd          :       VFMSUBADD231PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUBADD231PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUBADD231PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMSUBADD231PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMSUBADD231PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};

vfmsubadd132ps          :       VFMSUBADD132PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUBADD132PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUBADD132PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMSUBADD132PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMSUBADD132PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfmsubadd213ps          :       VFMSUBADD213PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUBADD213PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUBADD213PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMSUBADD213PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMSUBADD213PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfmsubadd231ps          :       VFMSUBADD231PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUBADD231PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUBADD231PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMSUBADD231PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMSUBADD231PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};

vfmsub132pd             :       VFMSUB132PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUB132PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUB132PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMSUB132PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMSUB132PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfmsub213pd             :       VFMSUB213PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUB213PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUB213PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMSUB213PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMSUB213PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfmsub231pd             :       VFMSUB231PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUB231PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUB231PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFMSUB231PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFMSUB231PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};

vfmsub132ps             :       VFMSUB132PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUB132PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUB132PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMSUB132PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMSUB132PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfmsub213ps             :       VFMSUB213PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUB213PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUB213PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMSUB213PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMSUB213PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfmsub231ps             :       VFMSUB231PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFMSUB231PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFMSUB231PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFMSUB231PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFMSUB231PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};

vfmsub132sd             :       VFMSUB132SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFMSUB132SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};
vfmsub213sd             :       VFMSUB213SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFMSUB213SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};
vfmsub231sd             :       VFMSUB231SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFMSUB231SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};

vfmsub132ss             :       VFMSUB132SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFMSUB132SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};
vfmsub213ss             :       VFMSUB213SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFMSUB213SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};
vfmsub231ss             :       VFMSUB231SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFMSUB231SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};

vfnmadd132pd            :       VFNMADD132PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMADD132PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMADD132PD xmm0 {k1}{z} COMMA xmm1 COMMA xmm2_m128_m64bcst
                        |       VFNMADD132PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFNMADD132PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfnmadd213pd            :       VFNMADD213PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMADD213PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMADD213PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFNMADD213PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFNMADD213PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfnmadd231pd            :       VFNMADD231PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMADD231PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMADD231PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFNMADD231PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFNMADD231PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};


vfnmadd132ps            :       VFNMADD132PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMADD132PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMADD132PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFNMADD132PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFNMADD132PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfnmadd213ps            :       VFNMADD213PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMADD213PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMADD213PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFNMADD213PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFNMADD213PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfnmadd231ps            :       VFNMADD231PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMADD231PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMADD231PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFNMADD231PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFNMADD231PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};

vfnmadd132sd            :       VFNMADD132SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFNMADD132SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};
vfnmadd213sd            :       VFNMADD213SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFNMADD213SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};
vfnmadd231sd            :       VFNMADD231SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFNMADD231SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};


vfnmadd132ss            :       VFNMADD132SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFNMADD132SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};
vfnmadd213ss            :       VFNMADD213SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFNMADD213SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};
vfnmadd231ss            :       VFNMADD231SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFNMADD231SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};

vfnmsub132pd            :       VFNMSUB132PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMSUB132PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMSUB132PD xmm0 {k1}{z} COMMA xmm1 COMMA xmm2_m128_m64bcst
                        |       VFNMSUB132PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFNMSUB132PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfnmsub213pd            :       VFNMSUB213PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMSUB213PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMSUB213PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFNMSUB213PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFNMSUB213PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};
vfnmsub231pd            :       VFNMSUB231PD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMSUB231PD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMSUB231PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VFNMSUB231PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VFNMSUB231PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};


vfnmsub132ps            :       VFNMSUB132PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMSUB132PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMSUB132PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFNMSUB132PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFNMSUB132PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfnmsub213ps            :       VFNMSUB213PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMSUB213PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMSUB213PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFNMSUB213PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFNMSUB213PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};
vfnmsub231ps            :       VFNMSUB231PS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VFNMSUB231PS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VFNMSUB231PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VFNMSUB231PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VFNMSUB231PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};

vfnmsub132sd            :       VFNMSUB132SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFNMSUB132SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};
vfnmsub213sd            :       VFNMSUB213SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFNMSUB213SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};
vfnmsub231sd            :       VFNMSUB231SD xmm1 COMMA xmm2 COMMA xmm3_m64
                        |       VFNMSUB231SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};


vfnmsub132ss            :       VFNMSUB132SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFNMSUB132SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};
vfnmsub213ss            :       VFNMSUB213SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFNMSUB213SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};
vfnmsub231ss            :       VFNMSUB231SS xmm1 COMMA xmm2 COMMA xmm3_m32
                        |       VFNMSUB231SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};

vfpclasspd              :       VFPCLASSPD k2 {k1} COMMA xmm2_m128_m64bcst COMMA imm
                        |       VFPCLASSPD k2 {k1} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VFPCLASSPD k2 {k1} COMMA zmm2_m512_m64bcst COMMA imm;

vfpclassps              :       VFPCLASSPS k2 {k1} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VFPCLASSPS k2 {k1} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VFPCLASSPS k2 {k1} COMMA zmm2_m512_m32bcst COMMA imm;

vfpclasssd              :       VFPCLASSSD k2 {k1} COMMA xmm2_m64 COMMA imm;

vfpclassss              :       VFPCLASSSS k2 {k1} COMMA xmm2_m32 COMMA imm;

vgatherdpd              :       VGATHERDPD xmm1 COMMA vm32x COMMA xmm2
                        |       VGATHERDPD ymm1 COMMA vm32x COMMA ymm2
                        |       VGATHERDPD xmm1 {k1} COMMA vm32x
                        |       VGATHERDPD ymm1 {k1} COMMA vm32x
                        |       VGATHERDPD zmm1 {k1} COMMA vm32y;
vgatherqpd              :       VGATHERQPD xmm1 COMMA vm64x COMMA xmm2
                        |       VGATHERQPD ymm1 COMMA vm64y COMMA ymm2
                        |       VGATHERQPD xmm1 {k1} COMMA vm64x
                        |       VGATHERQPD ymm1 {k1} COMMA vm64y
                        |       VGATHERQPD zmm1 {k1} COMMA vm64z;

vgatherdps              :       VGATHERDPS xmm1 COMMA vm32x COMMA xmm2
                        |       VGATHERDPS ymm1 COMMA vm32y COMMA ymm2
                        |       VGATHERDPS xmm1 {k1} COMMA vm32x
                        |       VGATHERDPS ymm1 {k1} COMMA vm32y
                        |       VGATHERDPS zmm1 {k1} COMMA vm32z;
vgatherqps              :       VGATHERQPS xmm1 COMMA vm64x COMMA xmm2
                        |       VGATHERQPS xmm1 COMMA vm64y COMMA xmm2
                        |       VGATHERQPS xmm1 {k1} COMMA vm64x
                        |       VGATHERQPS xmm1 {k1} COMMA vm64y
                        |       VGATHERQPS ymm1 {k1} COMMA vm64z;

vgetexppd               :       VGETEXPPD xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VGETEXPPD ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VGETEXPPD zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{sae};

vgetexpps               :       VGETEXPPS xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VGETEXPPS ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VGETEXPPS zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{sae};

vgetexpsd               :       VGETEXPSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{sae};

vgetexpss               :       VGETEXPSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{sae};

vgetmantpd              :       VGETMANTPD xmm1 {k1}{z} COMMA xmm2_m128_m64bcst COMMA imm
                        |       VGETMANTPD ymm1 {k1}{z} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VGETMANTPD zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{sae} COMMA imm;

vgetmantps              :       VGETMANTPS xmm1 {k1}{z} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VGETMANTPS ymm1 {k1}{z} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VGETMANTPS zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{sae} COMMA imm;

vgetmantsd              :       VGETMANTSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{sae} COMMA imm;

vgetmantss              :       VGETMANTSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{sae} COMMA imm;

vinsertf128             :       VINSERTF128 ymm1 COMMA ymm2 COMMA xmm3_m128 COMMA imm;
vinsertf32x4            :       VINSERTF32X4 ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128 COMMA imm
                        |       VINSERTF32X4 zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128 COMMA imm;
vinsertf64x2            :       VINSERTF64X2 ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128 COMMA imm
                        |       VINSERTF64X2 zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128 COMMA imm;
vinsertf32x8            :       VINSERTF32X8 zmm1 {k1}{z} COMMA zmm2 COMMA ymm3_m256 COMMA imm;
vinsertf64x4            :       VINSERTF64X4 zmm1 {k1}{z} COMMA zmm2 COMMA ymm3_m256 COMMA imm;

vinserti128             :       VINSERTI128 ymm1 COMMA ymm2 COMMA xmm3_m128 COMMA imm;
vinserti32x4            :       VINSERTI32X4 ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128 COMMA imm
                        |       VINSERTI32X4 zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128 COMMA imm;
vinserti64x2            :       VINSERTI64X2 ymm1 {k1}{z} COMMA ymm2 COMMA xmm3_m128 COMMA imm
                        |       VINSERTI64X2 zmm1 {k1}{z} COMMA zmm2 COMMA xmm3_m128 COMMA imm;
vinserti32x8            :       VINSERTI32X8 zmm1 {k1}{z} COMMA zmm2 COMMA ymm3_m256 COMMA imm;
vinserti64x4            :       VINSERTI64X4 zmm1 {k1}{z} COMMA zmm2 COMMA ymm3_m256 COMMA imm;

vmaskmovps              :       VMASKMOVPS xmm1 COMMA xmm2 COMMA m128
                        |       VMASKMOVPS ymm1 COMMA ymm2 COMMA m256
                        |       VMASKMOVPS m128 COMMA xmm1 COMMA xmm2
                        |       VMASKMOVPS m256 COMMA ymm1 COMMA ymm2;
vmaskmovpd              :       VMASKMOVPD xmm1 COMMA xmm2 COMMA m128
                        |       VMASKMOVPD ymm1 COMMA ymm2 COMMA m256
                        |       VMASKMOVPD m128 COMMA xmm1 COMMA xmm2
                        |       VMASKMOVPD m256 COMMA ymm1 COMMA ymm2;

vpblendd                :       VPBLENDD xmm1 COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VPBLENDD ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm;

vpblendmb               :       VPBLENDMB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPBLENDMB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPBLENDMB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpblendmw               :       VPBLENDMW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPBLENDMW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPBLENDMW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

vpblendmd               :       VPBLENDMD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPBLENDMD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPBLENDMD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpblendmq               :       VPBLENDMQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPBLENDMQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPBLENDMQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vpbroadcastb            :       VPBROADCASTB xmm1 {k1}{z} COMMA reg
                        |       VPBROADCASTB ymm1 {k1}{z} COMMA reg
                        |       VPBROADCASTB zmm1 {k1}{z} COMMA reg
                        |       VPBROADCASTB xmm1 COMMA xmm2_m8
                        |       VPBROADCASTB ymm1 COMMA ymm2_m8
                        |       VPBROADCASTB xmm1 {k1}{z} COMMA xmm2_m8
                        |       VPBROADCASTB ymm1 {k1}{z} COMMA xmm2_m8
                        |       VPBROADCASTB zmm1 {k1}{z} COMMA xmm2_m8;
vpbroadcastw            :       VPBROADCASTW xmm1 {k1}{z} COMMA reg
                        |       VPBROADCASTW ymm1 {k1}{z} COMMA reg
                        |       VPBROADCASTW zmm1 {k1}{z} COMMA reg
                        |       VPBROADCASTW xmm1 COMMA xmm2_m16
                        |       VPBROADCASTW ymm1 COMMA xmm2_m16
                        |       VPBROADCASTW xmm1 {k1}{z} COMMA xmm2_m16
                        |       VPBROADCASTW ymm1 {k1}{z} COMMA xmm2_m16
                        |       VPBROADCASTW zmm1 {k1}{z} COMMA xmm2_m16;
vpbroadcastd            :       VPBROADCASTD xmm1 {k1}{z} COMMA r32
                        |       VPBROADCASTD ymm1 {k1}{z} COMMA r32
                        |       VPBROADCASTD zmm1 {k1}{z} COMMA r32
                        |       VPBROADCASTD xmm1 COMMA xmm2_m32
                        |       VPBROADCASTD ymm1 COMMA xmm2_m32
                        |       VPBROADCASTD xmm1 {k1}{z} COMMA xmm2_m32
                        |       VPBROADCASTD ymm1 {k1}{z} COMMA xmm2_m32
                        |       VPBROADCASTD zmm1 {k1}{z} COMMA xmm2_m32;
vpbroadcastq            :       VPBROADCASTQ xmm1 {k1}{z} COMMA r64
                        |       VPBROADCASTQ ymm1 {k1}{z} COMMA r64
                        |       VPBROADCASTQ zmm1 {k1}{z} COMMA r64
                        |       VPBROADCASTQ xmm1 COMMA xmm2_m64
                        |       VPBROADCASTQ ymm1 COMMA xmm2_m64
                        |       VPBROADCASTQ xmm1 {k1}{z} COMMA xmm2_m64
                        |       VPBROADCASTQ ymm1 {k1}{z} COMMA xmm2_m64
                        |       VPBROADCASTQ zmm1 {k1}{z} COMMA xmm2_m64;
vbroadcasti32x2         :       VBROADCASTI32x2 xmm1 {k1}{z} COMMA xmm2_m64
                        |       VBROADCASTI32x2 ymm1 {k1}{z} COMMA xmm2_m64
                        |       VBROADCASTI32x2 zmm1 {k1}{z} COMMA xmm2_m64;
vbroadcasti128          :       VBROADCASTI128 ymm1 COMMA m128;
vbroadcasti32x4         :       VBROADCASTI32X4 ymm1 {k1}{z} COMMA m128
                        |       VBROADCASTI32X4 zmm1 {k1}{z} COMMA m128;
vbroadcasti64x2         :       VBROADCASTI64X2 ymm1 {k1}{z} COMMA m128
                        |       VBROADCASTI64X2 zmm1 {k1}{z} COMMA m128;
vbroadcasti32x8         :       VBROADCASTI32X8 zmm1 {k1}{z} COMMA m256;
vbroadcasti64X4         :       VBROADCASTI64X4 zmm1 {k1}{z} COMMA m256;

vpbroadcastmb2q         :       VPBROADCASTMB2Q xmm1 COMMA k1
                        |       VPBROADCASTMB2Q ymm1 COMMA k1
                        |       VPBROADCASTMB2Q zmm1 COMMA k1;
vpbroadcastmw2d         :       VPBROADCASTMW2D xmm1 COMMA k1
                        |       VPBROADCASTMW2D ymm1 COMMA k1
                        |       VPBROADCASTMW2D zmm1 COMMA k1;

vpcmpb                  :       VPCMPB k1 {k2} COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VPCMPB k1 {k2} COMMA ymm2 COMMA ymm3_m256 COMMA imm
                        |       VPCMPB k1 {k2} COMMA zmm2 COMMA zmm3_m512 COMMA imm;
vpcmpub                 :       VPCMPUB k1 {k2} COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VPCMPUB k1 {k2} COMMA ymm2 COMMA ymm3_m256 COMMA imm
                        |       VPCMPUB k1 {k2} COMMA zmm2 COMMA zmm3_m512 COMMA imm;

vpcmpd                  :       VPCMPD k1 {k2} COMMA xmm2 COMMA xmm3_m128_m32bcst COMMA imm
                        |       VPCMPD k1 {k2} COMMA ymm2 COMMA ymm3_m256_m32bcst COMMA imm
                        |       VPCMPD k1 {k2} COMMA zmm2 COMMA zmm3_m512_m32bcst COMMA imm;
vpcmpud                 :       VPCMPUD k1 {k2} COMMA xmm2 COMMA xmm3_m128_m32bcst COMMA imm
                        |       VPCMPUD k1 {k2} COMMA ymm2 COMMA ymm3_m256_m32bcst COMMA imm
                        |       VPCMPUD k1 {k2} COMMA zmm2 COMMA zmm3_m512_m32bcst COMMA imm;

vpcmpq                  :       VPCMPQ k1 {k2} COMMA xmm2 COMMA xmm3_m128_m64bcst COMMA imm
                        |       VPCMPQ k1 {k2} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VPCMPQ k1 {k2} COMMA zmm2 COMMA zmm3_m512_m64bcst COMMA imm;
vpcmpuq                 :       VPCMPUQ k1 {k2} COMMA xmm2 COMMA xmm3_m128_m64bcst COMMA imm
                        |       VPCMPUQ k1 {k2} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VPCMPUQ k1 {k2} COMMA zmm2 COMMA zmm3_m512_m64bcst COMMA imm;

vpcmpw                  :       VPCMPW k1 {k2} COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VPCMPW k1 {k2} COMMA ymm2 COMMA ymm3_m256 COMMA imm
                        |       VPCMPW k1 {k2} COMMA zmm2 COMMA zmm3_m512 COMMA imm;
vpcmpuw                 :       VPCMPUW k1 {k2} COMMA xmm2 COMMA xmm3_m128 COMMA imm
                        |       VPCMPUW k1 {k2} COMMA ymm2 COMMA ymm3_m256 COMMA imm
                        |       VPCMPUW k1 {k2} COMMA zmm2 COMMA zmm3_m512 COMMA imm;

vpcompressd             :       VPCOMPRESSD xmm1_m128{k1}{z} COMMA xmm2
                        |       VPCOMPRESSD ymm1_m256{k1}{z} COMMA ymm2
                        |       VPCOMPRESSD zmm1_m512{k1}{z} COMMA zmm2;

vpcompressq             :       VPCOMPRESSQ xmm1_m128{k1}{z} COMMA xmm2
                        |       VPCOMPRESSQ ymm1_m256{k1}{z} COMMA ymm2
                        |       VPCOMPRESSQ zmm1_m512{k1}{z} COMMA zmm2;

vpconflictd             :       VPCONFLICTD xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VPCONFLICTD ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VPCONFLICTD zmm1 {k1}{z} COMMA zmm2_m512_m32bcst;
vpconflictq             :       VPCONFLICTQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VPCONFLICTQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VPCONFLICTQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst;

vperm2f128              :       VPERM2F128 ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm;

vperm2i128              :       VPERM2I128 ymm1 COMMA ymm2 COMMA ymm3_m256 COMMA imm;

vpermb                  :       VPERMB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPERMB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPERMB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

vpermd                  :       VPERMD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPERMD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPERMD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpermw                  :       VPERMW xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPERMW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPERMW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

vpermi2b                :       VPERMI2B xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPERMI2B ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPERMI2B zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

vpermi2w                :       VPERMI2W xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPERMI2W ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPERMI2W zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpermi2d                :       VPERMI2D xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPERMI2D ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPERMI2D zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpermi2q                :       VPERMI2Q xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPERMI2Q ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPERMI2Q zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;
vpermi2ps               :       VPERMI2PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPERMI2PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPERMI2PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpermi2pd               :       VPERMI2PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPERMI2PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPERMI2PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vpermilpd               :       VPERMILPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPERMILPD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPERMILPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPERMILPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPERMILPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst
                        |       VPERMILPD xmm1 COMMA xmm2_m128 COMMA imm
                        |       VPERMILPD ymm1 COMMA ymm2_m256 COMMA imm
                        |       VPERMILPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst COMMA imm
                        |       VPERMILPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VPERMILPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst COMMA imm;

vpermilps               :       VPERMILPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPERMILPS xmm1 COMMA xmm2_m128 COMMA imm
                        |       VPERMILPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPERMILPS ymm1 COMMA ymm2_m256 COMMA imm
                        |       VPERMILPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPERMILPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPERMILPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst
                        |       VPERMILPS xmm1 {k1}{z} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VPERMILPS ymm1 {k1}{z} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VPERMILPS zmm1 {k1}{z} COMMA zmm2_m512_m32bcst COMMA imm;

vpermpd                 :       VPERMPD ymm1 COMMA ymm2_m256 COMMA imm
                        |       VPERMPD ymm1 {k1}{z} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VPERMPD zmm1 {k1}{z} COMMA zmm2_m512_m64bcst COMMA imm
                        |       VPERMPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPERMPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vpermps                 :       VPERMPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPERMPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPERMPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;

vpermq                  :       VPERMQ ymm1 COMMA ymm2_m256 COMMA imm
                        |       VPERMQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VPERMQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst COMMA imm
                        |       VPERMQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPERMQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vpermt2b                :       VPERMT2B xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPERMT2B ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPERMT2B zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

vpermt2w                :       VPERMT2W xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPERMT2W ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPERMT2W zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpermt2d                :       VPERMT2D xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPERMT2D ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPERMT2D zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpermt2q                :       VPERMT2Q xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPERMT2Q ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPERMT2Q zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;
vpermt2ps               :       VPERMT2PS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPERMT2PS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPERMT2PS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpermt2pd               :       VPERMT2PD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPERMT2PD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPERMT2PD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vpexpandd               :       VPEXPANDD xmm1 {k1}{z} COMMA xmm2_m128
                        |       VPEXPANDD ymm1 {k1}{z} COMMA ymm2_m256
                        |       VPEXPANDD zmm1 {k1}{z} COMMA zmm2_m512;

vpexpandq               :       VPEXPANDQ xmm1 {k1}{z} COMMA xmm2_m128
                        |       VPEXPANDQ ymm1 {k1}{z} COMMA ymm2_m256
                        |       VPEXPANDQ zmm1 {k1}{z} COMMA zmm2_m512;

vpgatherdd              :       VPGATHERDD xmm1 COMMA vm32x COMMA xmm2
                        |       VPGATHERDD ymm1 COMMA vm32y COMMA ymm2
                        |       VPGATHERDD xmm1 {k1} COMMA vm32x
                        |       VPGATHERDD ymm1 {k1} COMMA vm32y
                        |       VPGATHERDD zmm1 {k1} COMMA vm32z
vpgatherqd              :       VPGATHERQD xmm1 COMMA vm64x COMMA xmm2
                        |       VPGATHERQD xmm1 COMMA vm64y COMMA xmm2
                        |       VPGATHERQD xmm1 {k1} COMMA vm64x
                        |       VPGATHERQD xmm1 {k1} COMMA vm64y
                        |       VPGATHERQD ymm1 {k1} COMMA vm64z;
vpgatherdq              :       VPGATHERDQ xmm1 {k1} COMMA vm32x
                        |       VPGATHERDQ ymm1 {k1} COMMA vm32x
                        |       VPGATHERDQ zmm1 {k1} COMMA vm32y
                        |       VPGATHERDQ xmm1 COMMA vm32x COMMA xmm2
                        |       VPGATHERDQ ymm1 COMMA vm32x COMMA ymm2;
vpgatherqq              :       VPGATHERQQ xmm1 COMMA vm64x COMMA xmm2
                        |       VPGATHERQQ ymm1 COMMA vm64y COMMA ymm2
                        |       VPGATHERQQ xmm1 {k1} COMMA vm64x
                        |       VPGATHERQQ xmm1 {k1} COMMA vm64y
                        |       VPGATHERQQ ymm1 {k1} COMMA vm64z;

vplzcntd                :       VPLZCNTD xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VPLZCNTD ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VPLZCNTD zmm1 {k1}{z} COMMA zmm2_m512_m32bcst;
vplzcntq                :       VPLZCNTQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VPLZCNTQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VPLZCNTQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst;

vpmadd52huq             :       VPMADD52HUQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPMADD52HUQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPMADD52HUQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vpmadd52luq             :       VPMADD52LUQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPMADD52LUQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPMADD52LUQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vpmaskmovd              :       VPMASKMOVD xmm1 COMMA xmm2 COMMA m128
                        |       VPMASKMOVD ymm1 COMMA ymm2 COMMA m256
                        |       VPMASKMOVD m128 COMMA xmm1 COMMA xmm2
                        |       VPMASKMOVD m256 COMMA ymm1 COMMA ymm2;
vpmaskmovq              :       VPMASKMOVQ xmm1 COMMA xmm2 COMMA m128
                        |       VPMASKMOVQ ymm1 COMMA ymm2 COMMA m256
                        |       VPMASKMOVQ m128 COMMA xmm1 COMMA xmm2
                        |       VPMASKMOVQ m256 COMMA ymm1 COMMA ymm2;

vpmovb2m                :       VPMOVB2M k1 COMMA xmm1
                        |       VPMOVB2M k1 COMMA ymm1
                        |       VPMOVB2M k1 COMMA zmm1
                        |       VPMOVW2M k1 COMMA xmm1
                        |       VPMOVW2M k1 COMMA ymm1
                        |       VPMOVW2M k1 COMMA zmm1
                        |       VPMOVD2M k1 COMMA xmm1
                        |       VPMOVD2M k1 COMMA ymm1
                        |       VPMOVD2M k1 COMMA zmm1
                        |       VPMOVQ2M k1 COMMA xmm1
                        |       VPMOVQ2M k1 COMMA ymm1
                        |       VPMOVQ2M k1 COMMA zmm1;

vpmovdb                 :       VPMOVDB xmm1_m32 {k1}{z} COMMA xmm2
                        |       VPMOVDB xmm1_m64 {k1}{z} COMMA ymm2
                        |       VPMOVDB xmm1_m128 {k1}{z} COMMA zmm2;
vpmovsdb                :       VPMOVSDB xmm1_m32 {k1}{z} COMMA xmm2
                        |       VPMOVSDB xmm1_m64 {k1}{z} COMMA ymm2
                        |       VPMOVSDB xmm1_m128 {k1}{z} COMMA zmm2;
vpmovusdb               :       VPMOVUSDB xmm1_m32 {k1}{z} COMMA xmm2
                        |       VPMOVUSDB xmm1_m64 {k1}{z} COMMA ymm2
                        |       VPMOVUSDB xmm1_m128 {k1}{z} COMMA zmm2;

vpmovdw                 :       VPMOVDW xmm1_m64 {k1}{z} COMMA xmm2
                        |       VPMOVDW xmm1_m128 {k1}{z} COMMA ymm2
                        |       VPMOVDW xmm1_m256 {k1}{z} COMMA zmm2;
vpmovsdw                :       VPMOVSDW xmm1_m64 {k1}{z} COMMA xmm2
                        |       VPMOVSDW xmm1_m128 {k1}{z} COMMA ymm2
                        |       VPMOVSDW ymm1_m256 {k1}{z} COMMA zmm2;
vpmovusdw               :       VPMOVUSDW xmm1_m64 {k1}{z} COMMA xmm2
                        |       VPMOVUSDW xmm1_m128 {k1}{z} COMMA ymm2
                        |       VPMOVUSDW ymm1_m256 {k1}{z} COMMA zmm2;

vpmovm2b                :       VPMOVM2B xmm1 COMMA k1
                        |       VPMOVM2B ymm1 COMMA k1
                        |       VPMOVM2B zmm1 COMMA k1;
vpmovm2w                :       VPMOVM2W xmm1 COMMA k1
                        |       VPMOVM2W ymm1 COMMA k1
                        |       VPMOVM2W zmm1 COMMA k1;
vpmovm2d                :       VPMOVM2D xmm1 COMMA k1
                        |       VPMOVM2D ymm1 COMMA k1
                        |       VPMOVM2D zmm1 COMMA k1;
vpmovm2q                :       VPMOVM2Q xmm1 COMMA k1
                        |       VPMOVM2Q ymm1 COMMA k1
                        |       VPMOVM2Q zmm1 COMMA k1;

vpmovqb                 :       VPMOVQB xmm1_m16 {k1}{z} COMMA xmm2
                        |       VPMOVQB xmm1_m32 {k1}{z} COMMA ymm2
                        |       VPMOVQB xmm1_m64 {k1}{z} COMMA zmm2;
vpmovsqb                :       VPMOVSQB xmm1_m16 {k1}{z} COMMA xmm2
                        |       VPMOVSQB xmm1_m32 {k1}{z} COMMA ymm2
                        |       VPMOVSQB xmm1_m64 {k1}{z} COMMA zmm2;
vpmovusqb               :       VPMOVUSQB xmm1_m16 {k1}{z} COMMA xmm2
                        |       VPMOVUSQB xmm1_m32 {k1}{z} COMMA ymm2
                        |       VPMOVUSQB xmm1_m64 {k1}{z} COMMA zmm2;

vpmovqd                 :       VPMOVQD xmm1_m128 {k1}{z} COMMA xmm2
                        |       VPMOVQD xmm1_m128 {k1}{z} COMMA ymm2
                        |       VPMOVQD ymm1_m256 {k1}{z} COMMA zmm2;
vpmovsqd                :       VPMOVSQD xmm1_m64 {k1}{z} COMMA xmm2
                        |       VPMOVSQD xmm1_m128 {k1}{z} COMMA ymm2
                        |       VPMOVSQD ymm1_m256 {k1}{z} COMMA zmm2;
vpmovusqd               :       VPMOVUSQD xmm1_m64 {k1}{z} COMMA xmm2
                        |       VPMOVUSQD xmm1_m128 {k1}{z} COMMA ymm2
                        |       VPMOVUSQD ymm1_m256 {k1}{z} COMMA zmm2;

vpmovqw                 :       VPMOVQW xmm1_m32 {k1}{z} COMMA xmm2
                        |       VPMOVQW xmm1_m64 {k1}{z} COMMA ymm2
                        |       VPMOVQW xmm1_m128 {k1}{z} COMMA zmm2;
vpmovsqw                :       VPMOVSQW xmm1_m32 {k1}{z} COMMA xmm2
                        |       VPMOVSQW xmm1_m64 {k1}{z} COMMA ymm2
                        |       VPMOVSQW xmm1_m128 {k1}{z} COMMA zmm2;
vpmovusqw               :       VPMOVUSQW xmm1_m32 {k1}{z} COMMA xmm2
                        |       VPMOVUSQW xmm1_m64 {k1}{z} COMMA ymm2
                        |       VPMOVUSQW xmm1_m128 {k1}{z} COMMA zmm2;

vpmovwb                 :       VPMOVWB xmm1_m64 {k1}{z} COMMA xmm2
                        |       VPMOVWB xmm1_m128 {k1}{z} COMMA ymm2
                        |       VPMOVWB ymm1_m256 {k1}{z} COMMA zmm2;
vpmovswb                :       VPMOVSWB xmm1_m64 {k1}{z} COMMA xmm2
                        |       VPMOVSWB xmm1_m128 {k1}{z} COMMA ymm2
                        |       VPMOVSWB ymm1_m256 {k1}{z} COMMA zmm2;
vpmovuswb               :       VPMOVUSWB xmm1_m64 {k1}{z} COMMA xmm2
                        |       VPMOVUSWB xmm1_m128 {k1}{z} COMMA ymm2
                        |       VPMOVUSWB ymm1_m256 {k1}{z} COMMA zmm2;

vpmultishiftqb          :       VPMULTISHIFTQB xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPMULTISHIFTQB ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPMULTISHIFTQB zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vprolvd                 :       VPROLVD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPROLVD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPROLVD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vprold                  :       VPROLD xmm1 {k1}{z} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VPROLD ymm1 {k1}{z} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VPROLD zmm1 {k1}{z} COMMA zmm2_m512_m32bcst COMMA imm;
vprolvq                 :       VPROLVQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPROLVQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPROLVQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;
vprolq                  :       VPROLQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst COMMA imm
                        |       VPROLQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VPROLQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst COMMA imm;

vprorvd                 :       VPRORVD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPRORVD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPRORVD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vprord                  :       VPRORD xmm1 {k1}{z} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VPRORD ymm1 {k1}{z} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VPRORD zmm1 {k1}{z} COMMA zmm2_m512_m32bcst COMMA imm;
vprorvq                 :       VPRORVQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPRORVQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPRORVQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;
vprorq                  :       VPRORQ xmm1 {k1}{z} COMMA xmm2_m128_m64bcst COMMA imm
                        |       VPRORQ ymm1 {k1}{z} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VPRORQ zmm1 {k1}{z} COMMA zmm2_m512_m64bcst COMMA imm;

vpscatterdd             :       VPSCATTERDD vm32x {k1} COMMA xmm1
                        |       VPSCATTERDD vm32y {k1} COMMA ymm1
                        |       VPSCATTERDD vm32z {k1} COMMA zmm1;
vpscatterdq             :       VPSCATTERDQ vm32x {k1} COMMA xmm1
                        |       VPSCATTERDQ vm32x {k1} COMMA ymm1
                        |       VPSCATTERDQ vm32y {k1} COMMA zmm1;
vpscatterqd             :       VPSCATTERQD vm64x {k1} COMMA xmm1
                        |       VPSCATTERQD vm64y {k1} COMMA xmm1
                        |       VPSCATTERQD vm64z {k1} COMMA ymm1;
vpscatterqq             :       VPSCATTERQQ vm64x {k1} COMMA xmm1
                        |       VPSCATTERQQ vm64y {k1} COMMA ymm1
                        |       VPSCATTERQQ vm64z {k1} COMMA zmm1;

vpsllvd                 :       VPSLLVD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSLLVD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSLLVD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPSLLVD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPSLLVD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpsllvq                 :       VPSLLVQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSLLVQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSLLVQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPSLLVQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPSLLVQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;
vpsllvw                 :       VPSLLVW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSLLVW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPSLLVW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

vpsravd                 :       VPSRAVD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSRAVD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSRAVD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPSRAVD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPSRAVD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpsravw                 :       VPSRAVW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSRAVW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPSRAVW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;
vpsravq                 :       VPSRAVQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPSRAVQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPSRAVQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vpsrlvd                 :       VPSRLVD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSRLVD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSRLVD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPSRLVD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPSRLVD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vpsrlvq                 :       VPSRLVQ xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VPSRLVQ ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VPSRLVQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPSRLVQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPSRLVQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;
vpsrlvw                 :       VPSRLVW xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128
                        |       VPSRLVW ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256
                        |       VPSRLVW zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512;

vpternlogd              :       VPTERNLOGD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst COMMA imm
                        |       VPTERNLOGD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst COMMA imm
                        |       VPTERNLOGD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst COMMA imm;
vpternlogq              :       VPTERNLOGQ xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst COMMA imm
                        |       VPTERNLOGQ ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VPTERNLOGQ zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst COMMA imm;

vptestmb                :       VPTESTMB k2 {k1} COMMA xmm2 COMMA xmm3_m128
                        |       VPTESTMB k2 {k1} COMMA ymm2 COMMA ymm3_m256
                        |       VPTESTMB k2 {k1} COMMA zmm2 COMMA zmm3_m512;
vptestmw                :       VPTESTMW k2 {k1} COMMA xmm2 COMMA xmm3_m128
                        |       VPTESTMW k2 {k1} COMMA ymm2 COMMA ymm3_m256
                        |       VPTESTMW k2 {k1} COMMA zmm2 COMMA zmm3_m512;
vptestmd                :       VPTESTMD k2 {k1} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPTESTMD k2 {k1} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPTESTMD k2 {k1} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vptestmq                :       VPTESTMQ k2 {k1} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPTESTMQ k2 {k1} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPTESTMQ k2 {k1} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vptestnmb               :       VPTESTNMB k2 {k1} COMMA xmm2 COMMA xmm3_m128
                        |       VPTESTNMB k2 {k1} COMMA ymm2 COMMA ymm3_m256
                        |       VPTESTNMB k2 {k1} COMMA zmm2 COMMA zmm3_m512;
vptestnmw               :       VPTESTNMW k2 {k1} COMMA xmm2 COMMA xmm3_m128
                        |       VPTESTNMW k2 {k1} COMMA ymm2 COMMA ymm3_m256
                        |       VPTESTNMW k2 {k1} COMMA zmm2 COMMA zmm3_m512;
vptestnmd               :       VPTESTNMD k2 {k1} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VPTESTNMD k2 {k1} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VPTESTNMD k2 {k1} COMMA zmm2 COMMA zmm3_m512_m32bcst;
vptestnmq               :       VPTESTNMQ k2 {k1} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VPTESTNMQ k2 {k1} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VPTESTNMQ k2 {k1} COMMA zmm2 COMMA zmm3_m512_m64bcst;

vrangepd                :       VRANGEPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst COMMA imm
                        |       VRANGEPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VRANGEPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{sae} COMMA imm;

vrangeps                :       VRANGEPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst COMMA imm
                        |       VRANGEPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst COMMA imm
                        |       VRANGEPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{sae} COMMA imm;

vrangesd                :       VRANGESD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{sae} COMMA imm;

vrangess                :       VRANGESS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{sae} COMMA imm;

vrcp14pd                :       VRCP14PD xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VRCP14PD ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VRCP14PD zmm1 {k1}{z} COMMA zmm2_m512_m64bcst;

vrcp14sd                :       VRCP14SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64;

vrcp14ps                :       VRCP14PS xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VRCP14PS ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VRCP14PS zmm1 {k1}{z} COMMA zmm2_m512_m32bcst;

vrcp14ss                :       VRCP14SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32;

vreducepd               :       VREDUCEPD xmm1 {k1}{z} COMMA xmm2_m128_m64bcst COMMA imm
                        |       VREDUCEPD ymm1 {k1}{z} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VREDUCEPD zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{sae} COMMA imm;

vreducesd               :       VREDUCESD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{sae} COMMA imm_r;

vreduceps               :       VREDUCEPS xmm1 {k1}{z} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VREDUCEPS ymm1 {k1}{z} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VREDUCEPS zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{sae} COMMA imm;

vreducess               :       VREDUCESS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{sae} COMMA imm;

vrndscalepd             :       VRNDSCALEPD xmm1 {k1}{z} COMMA xmm2_m128_m64bcst COMMA imm
                        |       VRNDSCALEPD ymm1 {k1}{z} COMMA ymm2_m256_m64bcst COMMA imm
                        |       VRNDSCALEPD zmm1 {k1}{z} COMMA zmm2_m512_m64bcst{sae} COMMA imm;

vrndscalesd             :       VRNDSCALESD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{sae} COMMA imm;

vrndscaleps             :       VRNDSCALEPS xmm1 {k1}{z} COMMA xmm2_m128_m32bcst COMMA imm
                        |       VRNDSCALEPS ymm1 {k1}{z} COMMA ymm2_m256_m32bcst COMMA imm
                        |       VRNDSCALEPS zmm1 {k1}{z} COMMA zmm2_m512_m32bcst{sae} COMMA imm;

vrndscaless             :       VRNDSCALESS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{sae} COMMA imm;

vrsqrt14pd              :       VRSQRT14PD xmm1 {k1}{z} COMMA xmm2_m128_m64bcst
                        |       VRSQRT14PD ymm1 {k1}{z} COMMA ymm2_m256_m64bcst
                        |       VRSQRT14PD zmm1 {k1}{z} COMMA zmm2_m512_m64bcst;

vrsqrt14sd              :       VRSQRT14SD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64;

vrsqrt14ps              :       VRSQRT14PS xmm1 {k1}{z} COMMA xmm2_m128_m32bcst
                        |       VRSQRT14PS ymm1 {k1}{z} COMMA ymm2_m256_m32bcst
                        |       VRSQRT14PS zmm1 {k1}{z} COMMA zmm2_m512_m32bcst;

vrsqrt14ss              :       VRSQRT14SS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32;

vscalefpd               :       VSCALEFPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VSCALEFPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VSCALEFPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst{er};

vscalefsd               :       VSCALEFSD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m64{er};

vscalefps               :       VSCALEFPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VSCALEFPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VSCALEFPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst{er};

vscalefss               :       VSCALEFSS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m32{er};

vscatterdps             :       VSCATTERDPS vm32x {k1} COMMA xmm1
                        |       VSCATTERDPS vm32y {k1} COMMA ymm1
                        |       VSCATTERDPS vm32z {k1} COMMA zmm1;
vscatterdpd             :       VSCATTERDPD vm32x {k1} COMMA xmm1
                        |       VSCATTERDPD vm32x {k1} COMMA ymm1
                        |       VSCATTERDPD vm32y {k1} COMMA zmm1;
vscatterqps             :       VSCATTERQPS vm64x {k1} COMMA xmm1
                        |       VSCATTERQPS vm64y {k1} COMMA xmm1
                        |       VSCATTERQPS vm64z {k1} COMMA ymm1;
vscatterqpd             :       VSCATTERQPD vm64x {k1} COMMA xmm1
                        |       VSCATTERQPD vm64y {k1} COMMA ymm1
                        |       VSCATTERQPD vm64z {k1} COMMA zmm1;

vshuff32x4              :       VSHUFF32X4 ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst COMMA imm
                        |       VSHUFF32X4 zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst COMMA imm;
vshuff64x2              :       VSHUFF64X2 ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VSHUFF64X2 zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst COMMA imm;
vshufi32x4              :       VSHUFI32X4 ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst COMMA imm
                        |       VSHUFI32X4 zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst COMMA imm;
vshufi64x2              :       VSHUFI64X2 ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst COMMA imm
                        |       VSHUFI64X2 zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst COMMA imm;

vtestps                 :       VTESTPS xmm1 COMMA xmm2_m128
                        |       VTESTPS ymm1 COMMA ymm2_m256;
vtestpd                 :       VTESTPD xmm1 COMMA xmm2_m128
                        |       VTESTPD ymm1 COMMA ymm2_m256;

vzeroall                :       VZEROALL;

vzeroupper              :       VZEROUPPER;
*/
//wait			:       WAIT	{ mal.output("0x9B");}	# W_A_I_T;

fwait			:       FWAIT	{ mal.output("0x9B");}	# F_W_A_I_T;

wbinvd			:       WBINVD	{ mal.output("0x0F","0x09");}	# W_B_I_N_V_D;

/*
wrfsbase                :       WRFSBASE r32
                        |       WRFSBASE r64;
wrgsbase                :       WRGSBASE r32
                        |       WRGSBASE r64;
*/
wrmsr			:       WRMSR	{ mal.output("0x0F","0x30");}	# W_R_M_S_R;

/*
wrpkru                  :       WRPKRU;

xacquire                :       XACQUIRE;
xrelease                :       XRELEASE;

xabort                  :       XABORT imm;

xadd                    :       XADD r_m8 COMMA r8
                        |       XADD r_m8 COMMA r8
                        |       XADD r_m16 COMMA r16
                        |       XADD r_m32 COMMA r32
                        |       XADD r_m64 COMMA r64;

xbegin                  :       XBEGIN rel16
                        |       XBEGIN rel32;

xchg                    :       XCHG AX COMMA r16
                        |       XCHG r16 COMMA AX
                        |       XCHG EAX COMMA r32
                        |       XCHG RAX COMMA r64
                        |       XCHG r32 COMMA EAX
                        |       XCHG r64 COMMA RAX
                        |       XCHG r_m8 COMMA r8
                        |       XCHG r8 COMMA r_m8
                        |       XCHG r_m16 COMMA r16
                        |       XCHG r16 COMMA r_m16
                        |       XCHG r_m32 COMMA r32
                        |       XCHG r_m64 COMMA r64
                        |       XCHG r32 COMMA r_m32
                        |       XCHG r64 COMMA r_m64;

xend                    :       XEND;

xgetbv                  :       XGETBV;

xlat                    :       XLAT m8;
xlatb                   :       XLATB;

xor                     :       XOR AL COMMA imm
                        |       XOR AX COMMA imm
                        |       XOR EAX COMMA imm
                        |       XOR RAX COMMA imm
                        |       XOR r_m8 COMMA imm
                        |       XOR r_m16 COMMA imm
                        |       XOR r_m32 COMMA imm
                        |       XOR r_m64 COMMA imm
                        |       XOR r_m16 COMMA imm
                        |       XOR r_m32 COMMA imm
                        |       XOR r_m64 COMMA imm
                        |       XOR r_m8 COMMA r8
                        |       XOR r_m16 COMMA r16
                        |       XOR r_m32 COMMA r32
                        |       XOR r_m64 COMMA r64
                        |       XOR r8 COMMA r_m8
                        |       XOR r16 COMMA r_m16
                        |       XOR r32 COMMA r_m32
                        |       XOR r64 COMMA r_m64;

xorpd                   :       XORPD xmm1 COMMA xmm2_m128;
vxorpd                  :       VXORPD xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VXORPD ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VXORPD xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m64bcst
                        |       VXORPD ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m64bcst
                        |       VXORPD zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m64bcst;

xorps                   :       XORPS xmm1 COMMA xmm2_m128;
vxorps                  :       VXORPS xmm1 COMMA xmm2 COMMA xmm3_m128
                        |       VXORPS ymm1 COMMA ymm2 COMMA ymm3_m256
                        |       VXORPS xmm1 {k1}{z} COMMA xmm2 COMMA xmm3_m128_m32bcst
                        |       VXORPS ymm1 {k1}{z} COMMA ymm2 COMMA ymm3_m256_m32bcst
                        |       VXORPS zmm1 {k1}{z} COMMA zmm2 COMMA zmm3_m512_m32bcst;

xrstor                  :       XRSTOR mem;
xrstor64                :       XRSTOR64 mem;

xrstors                 :       XRSTORS mem;
xrstors64               :       XRSTORS64 mem;

xsave                   :       XSAVE mem;
xsave64                 :       XSAVE64 mem;

xsavec                  :       XSAVEC mem;
xsavec64                :       XSAVEC64 mem;

xsaveopt                :       XSAVEOPT mem;
xsaveopt64              :       XSAVEOPT64 mem;

xsaves                  :       XSAVES mem;
xsaves64                :       XSAVES64 mem;

xsetbv                  :       XSETBV;

xtest                   :       XTEST;
*/


data		:	DB_SYMBOL STRING COMMA imm
			;


bits16		:	{ mal.bits =16;mal.encoder.initBit(16);} BIT16 OPEN_BIG_BRACKET (label? instructions comment?)* CLOSE_BIG_BRACKET
			;


bits32		:	{ mal.bits = 32; mal.encoder.initBit(32);} BIT32 OPEN_BIG_BRACKET (label? instructions comment?)* CLOSE_BIG_BRACKET
			;


bits64		:	{ mal.bits = 64;mal.encoder.initBit(64);} BIT64 OPEN_BIG_BRACKET (label? instructions comment?)* CLOSE_BIG_BRACKET
			;


